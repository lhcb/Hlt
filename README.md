If you have a crucial bugfix or feature addition for the current production version, make a merge request with the 2018-patches branch. 

If you want to add a new line, make a merge request with the 2018-patches branch. 

If you have developments which target the Upgrade, go to https://gitlab.cern.ch/lhcb/Moore and follow the instructions there. We have reorganised the project structure so that the upgrade Moore does not depend on Hlt anymore.
