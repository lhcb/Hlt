from GaudiKernel.SystemOfUnits import GeV, mm, MeV, picosecond

class Hcetacmumu_pp_2018 :
    """
    Threshold settings for Hlt2 Hcetacmumu lines: 
    
    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS
    
    @author R. Cardinale < roberta.cardinale@cern.ch > 
    @date 2017-02-28
    """
    
    __all__ = ( 'ActiveHlt2Lines' )
    
    
    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            'Hlt2HcetacPPmumuTurbo',
            'Hlt2HcetacPhiPhimumuTurbo'
            ]
        
        return lines


    def Thresholds(self) :
        
        d = {}
        
        from Hlt2Lines.Hcetacmumu.Lines import HcetacmumuLines

        d.update (
            {
            HcetacmumuLines : {
            
            'MuonForHc' : { 
            #'M_MIN'          :  200.0 * MeV
            #, 'M_MAX'          :  600.0 * MeV
            #, 'VCHI2PDOF_MAX'  :  25.0
            #,
            'TRACK_TRCHI2DOF_MAX' : 5
            , 'PT_MIN' : 0*MeV
            },  
            'Etac2PP' : {
            'DauCuts'                :  """
            (TRCHI2DOF < 3.0) & (PROBNNp > 0.6) & (PT > 0.75*GeV) & (TRGHOSTPROB<0.2)
            
            """,
            'ComCuts': """
            in_range(2.7*GeV, AM, 3.3*GeV)
            & (APT > 0.8*GeV)
            """,
            'MomCuts': """
            (VFASPF(VCHI2PDOF) < 9)
            & (in_range(2.8*GeV, MM, 3.2*GeV))
            & (PT > 1.0*GeV)
            """  
            },
            'Etac2PhiPhi' : {
            'DauCuts':  """
            (INTREE( ('K+'==ID) & (PT > 0.65*GeV)
            & (P > 1.*GeV) & (TRCHI2DOF < 3.) & (PIDK > 0.) & (TRGHOSTPROB<0.5)))
            & (INTREE( ('K-'==ID) & (PT > 0.65*GeV)
            & (P > 1.*GeV) & (TRCHI2DOF < 3.) & (PIDK > 0.) & (TRGHOSTPROB<0.5)))
            & (PT > 0.8*GeV)
            & (VFASPF(VCHI2PDOF) < 9)
            & (ADMASS('phi(1020)') < 30.*MeV)
            """,
            'ComCuts': """
            in_range(2.7*GeV, AM, 3.3*GeV)
            & (APT > 0.0*GeV)
            """,
            'MomCuts': """
            (VFASPF(VCHI2PDOF) < 9)
            & (in_range(2.8*GeV, MM, 3.2*GeV))
            & (PT > 0.0*GeV)
            """
            
            },
            
            'HcetacPPmumu' : {
            'HcDauCuts'                :  """
            (INTREE( ('mu+'==ABSID) & (TRCHI2DOF < 5.0) & (TRGHOSTPROB<0.5) & ISMUON & (PIDmu>0.0)))
            
            """,
            'HcComCuts': """
            in_range(3.2*GeV, AM, 3.8*GeV)
            """,
            'HcMomCuts': """
            (VFASPF(VCHI2PDOF) < 9)
            & (in_range(3.3*GeV, MM, 3.7*GeV))
            """
            
            },
            'HcetacPhiPhimumu' : {
            'HcPhiPhiDauCuts'                :  """
            (INTREE( ('mu+'==ABSID) & (TRCHI2DOF < 5.0) & (TRGHOSTPROB<0.5)))
            
            """,
            'HcPhiPhiComCuts': """
            in_range(3.2*GeV, AM, 3.8*GeV)
            """,
            'HcPhiPhiMomCuts': """
            (VFASPF(VCHI2PDOF) < 9)
            & (in_range(3.3*GeV, MM, 3.7*GeV))
            """
            
            }
            
            }
            }
            )
        
            
        
        return d
