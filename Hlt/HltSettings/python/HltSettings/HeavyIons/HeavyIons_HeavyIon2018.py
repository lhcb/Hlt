class HeavyIons_HeavyIon2018(object):
    """
    Threshold settings for Hlt2 HeavyIons lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author Y. Zhang
    @date 2018-07-11
    """
    def ActiveHlt2Lines(self):
        lines = [
            'Hlt2BBPassThrough',
            'Hlt2SMOGPassThrough',
            'Hlt2BBHighMult',
            'Hlt2BEHighMult',
            'Hlt2SingleTrack',
            #new lines to provide interfaces for prescales
            'Hlt2MBMicroBiasVelo',
            'Hlt2MBMicroBiasLowMultVelo',
            'Hlt2BEMicroBiasLowMultVelo',
        ]
        return lines

    def Thresholds(self):
        from Hlt2Lines.HeavyIons.Lines import HeavyIonsLines
        thresholds = {
            HeavyIonsLines: {
                'Prescale':{
                    "Hlt2BBPassThrough": 1.,
                    "Hlt2MBMicroBiasVelo": 1.,
                    "Hlt2MBMicroBiasLowMultVelo": 1.,
                    "Hlt2SMOGPassThrough": 1.,
                    "Hlt2BBHighMult": 1.,
                    "Hlt2BEHighMult": 1.,
                    "Hlt2SingleTrack": 1.,
                    "Hlt2BEMicroBiasLowMultVelo": 1.,
                    },
                'BBPassThrough': {
                    'HLT1': " | ".join(
                        [ "HLT_PASS_RE('^Hlt1BB(?!VeryHigh)(?!MicroBias).*Decision$')", #Hlt1BBMicroBiasLowMultVelo, -SoftCEP, -Velo
                          "HLT_PASS('Hlt1L0PhotonLowMultDecision')",
                          "HLT_PASS_RE('^Hlt1(?!SMOG).*Muon.*Decision$')"
                            ]
                        ),
                    'VoidFilter': '',
                    'ODIN': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing)',
                },
                "MBMicroBiasVelo":{
                    "HLT1" : "HLT_PASS('Hlt1BBMicroBiasVeloDecision')",
                    'VoidFilter': '',
                    },
                "MBMicroBiasLowMultVelo":{
                    "HLT1" : "HLT_PASS('Hlt1BBMicroBiasLowMultVeloDecision')",
                    'VoidFilter': '',
                    },
                "BEMicroBiasLowMultVelo":{
                    "HLT1" : "HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision')",
                    'VoidFilter': '',
                    },
                'SMOGPassThrough': {
                    'HLT1': " | ".join(
                        [ "HLT_PASS_RE('^Hlt1(BE|EB)(?!VeryHigh)(?!MicroBiasLowMult).*Decision$')",
                          "HLT_PASS_RE('^Hlt1SMOG.*Decision$')"
                            ]
                        ),
                    'VoidFilter': '',
                     'ODIN':    '(ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2)',
                },
                'BBHighMult': {
                    'HLT1': "HLT_PASS('Hlt1BBVeryHighMultDecision')",
                    'VoidFilter': '',
                },
                'BEHighMult': {
                    'HLT1': "HLT_PASS('Hlt1BEVeryHighMultDecision')",
                    'VoidFilter': '',
                },
                'SingleTrack' : {
                    "HLT1" :  "HLT_PASS('Hlt1BBMicroBiasSoftCEPDecision')",
                    'L0Req':   "(L0_DATA('Spd(Mult)') < 50)"
                    },
            },
        }
        return thresholds
