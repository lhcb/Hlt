from GaudiKernel.SystemOfUnits import GeV, MeV, mm

class LowMult_HeavyIon2018 :
    """
    Threshold settings for Hlt2 Low Multiplicity (LowMult) lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS
    WARNING :: Added new dependence to include Herschel at L0

    @author yanxi.zhang@cern.ch
    @date 2018-09-12 21:20
    """

    __all__ = ( 'ActiveHlt2Lines' )

    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            'Hlt2NoBiasNonBeamBeam',
        ]
        return lines

    def Thresholds(self) :
        from Hlt2Lines.LowMult.Lines import LowMultLines
        d = {
            LowMultLines: {
                'Prescale': {'Hlt2NoBiasNonBeamBeam': 1.0},  # Aim for 80 Hz
                'HLT': {
                    "TechnicalNoBias" : "HLT_PASS_RE('Hlt1NoBiasEmptyEmptyDecision')",
                    },
                'Technical_L0': {"NoBias": ""},
                'Technical_ODIN': {"ALL": ""},
            }
        }
        return d
