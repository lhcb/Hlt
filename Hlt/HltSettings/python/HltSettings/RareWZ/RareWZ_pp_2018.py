# =============================================================================
# @file   RareWZ_pp_2018.py
# @author Hang Yin, Qundong Han (hang.yin@cern.ch, qundong.han@cern.ch)
# @date   01.14.2018
# =============================================================================
"""Threshold settings for Hlt2 Rare WZ lines for 2018."""

from GaudiKernel.SystemOfUnits import mm, MeV, GeV, picosecond, mrad

class RareWZ_pp_2018(object):
    __all__ = ('ActiveHlt2Lines', 'Thresholds')

    def ActiveHlt2Lines(self):
        """Returns a list of active lines."""

        lines = ['Hlt2RareWZKaonGammaTurbo',
                 'Hlt2RareWZPionGammaTurbo',
                 'Hlt2RareWZDsGammaTurbo',
                 'Hlt2RareWZDpGammaTurbo',
                 'Hlt2RareWZD0GammaTurbo',
                 'Hlt2RareWZGammaGammaTurbo',
                 'Hlt2RareWZPi0GammaTurbo',
                 'Hlt2RareWZPi0Pi0Turbo',
                 'Hlt2RareWZDiMuonGammaTurbo',
                 'Hlt2RareWZKstGammaTurbo',
                 'Hlt2RareWZKs0GammaTurbo',
                 'Hlt2RareWZRho0GammaTurbo',
                 'Hlt2RareWZPhiGammaTurbo',
                 'Hlt2RareWZJPsiDiJetsTurbo',
             ]
        return lines

    def Thresholds(self):
        """Returns the line thresholds."""
        d={}
        from Hlt2Lines.RareWZ.Lines import RareWZLines

	d.update( { RareWZLines : {
            'Common': {
                'TisTosSpec'                           : "Hlt1.*Track.*Decision%TOS",
                'Trk_ALL_TRCHI2DOF_MAX'                : 3.0,
                'Trk_ALL_TRGHOSTPROB_MAX'              : 0.4,
                'Trk_ALL_P_MIN'                        : 1000 * MeV,
                'VCHI2PDOF_MAX'                        : 10.0,
            },
            'WZKaonGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZPionGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV, 
            },
            'WZGammaGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV, 
            },
            'WZPi0Gamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV, 
            },
            'WZPi0Pi0' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZD0Gamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV, 
            },
            'WZDpGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZDsGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV, 
            },
            'WZRho0Gamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZDiMuonGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZKstGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZKs0Gamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZRho0Gamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZPhiGamma' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZJpsiDiJets' :{
                'AM_MIN'                               : 30000 * MeV,
                'AM_MAX'                               : 130000 * MeV,
            },
            'WZD02HH': {
                'Trk_ALL_P_MIN'                        : 1000 * MeV,
                'Trk_ALL_PT_MIN'                       : 500.0 * MeV,
                'Pair_AMINDOCA_MAX'                    : 0.1 * mm,
                'AM_MIN'                               : 1715.0 * MeV,
                'AM_MAX'                               : 2015.0 * MeV,
                'acosBPVDIRA_MAX'                      : 100.0 * mrad,
                'BPVVDCHI2_MIN'                        : 25.0,
                'PT_MIN'                               : 15000.0 * MeV,
                'VCHI2PDOF_MAX'                        : 10.0,
                'TisTosSpec'                           : [ ]
            },
            'WZDpm2HHH' : {
                'Trk_ALL_PT_MIN'                       : 250.0 * MeV,
                'Trk_2OF3_PT_MIN'                      : 400.0 * MeV,
                'Trk_1OF3_PT_MIN'                      : 1000.0 * MeV,
                'ASUMPT_MIN'                           : 3000 * MeV,
                'acosBPVDIRA_MAX'                      : 100.0 * mrad,
                'BPVLTIME_MIN'                         : 0.4 * picosecond,
                'BPVVDCHI2_MIN'                        : 150.0,
                'VCHI2PDOF_MAX'                        : 6,
                'AM_MAX'                               : 1959 * MeV,
                'AM_MIN'                               : 1779 * MeV,
                'PT_MIN'                               : 15000 * MeV,
                'TisTosSpec'                           : [ ]
            },
            'WZDs2HHH': {
                'Trk_ALL_PT_MIN'                       : 250.0 * MeV,
                'Trk_2OF3_PT_MIN'                      : 400.0 * MeV,
                'Trk_1OF3_PT_MIN'                      : 1000.0 * MeV,
                'ASUMPT_MIN'                           : 3000 * MeV,
                'acosBPVDIRA_MAX'                      : 100.0 * mrad,
                'BPVLTIME_MIN'                         : 0.2 * picosecond,
                'BPVVDCHI2_MIN'                        : 100.0,
                'VCHI2PDOF_MAX'                        : 6,
                'AM_MAX'                               : 2059 * MeV,
                'AM_MIN'                               : 1879 * MeV,
                'PT_MIN'                               : 15000 * MeV,
                'TisTosSpec'                           : [ ]
            },
            'RareWZSharedKs0': {
                'AM_MIN'                               : 792 * MeV,
                'AM_MAX'                               : 992 * MeV,
                'ASUMPT_MIN'                           : 1.0 * GeV,
                'ADOCA_MAX'                            : 0.08 * mm,
                'VCHI2_MAX'                            : 10.0,
                'BPVVD_MIN'                            : -999.,
                'BPVCORRM_MAX'                         : 3.5 * GeV,
                'BPVVDCHI2_MIN'                        : 25.0,
                'PT_MIN'                               : 15000. * MeV,
            },
            'RareWZSharedRho0': {
                'AM_MIN'                               : 670 * MeV,
                'AM_MAX'                               : 870 * MeV,
                'ASUMPT_MIN'                           : 1.0 * GeV,
                'ADOCA_MAX'                            : 0.08 * mm,
                'VCHI2_MAX'                            : 10.0,
                'BPVVD_MIN'                            : -999.,
                'BPVCORRM_MAX'                         : 3.5 * GeV,
                'BPVVDCHI2_MIN'                        : 25.0,
                'PT_MIN'                               : 15000. * MeV,
            },
            'RareWZSharedKst' : {
                'AM_MIN'                               : 792 * MeV,
                'AM_MAX'                               : 992 * MeV,
                'ASUMPT_MIN'                           : 1. * GeV,
                'PT_MIN'                               : 15000 * MeV,
                'VCHI2PDOF_MAX'                        : 10.0,
                'BPVCORRM_MAX'                         : 3500 * MeV,
                'acosBPVDIRA_MAX'                      : 100 * mrad,
                'BPVVDCHI2_MIN'                        : 20.0,
                'BPVLTIME_MIN'                         : 0.3 * picosecond,
            },
            'RareWZSharedPhi': {
                'AM_MIN'                               : 920 * MeV,
                'AM_MAX'                               : 1120 * MeV,
                'ASUMPT_MIN'                           : 1.0 * GeV,
                'ADOCA_MAX'                            : 0.08 * mm,
                'VCHI2_MAX'                            : 10.0,
                'BPVVD_MIN'                            : -999.,
                'BPVCORRM_MAX'                         : 3.5 * GeV,
                'BPVVDCHI2_MIN'                        : 25.0,
                'PT_MIN'                               : 15000. * MeV,
            },
            'RareWZSharedDiMuon' : {
                'MUONPT_MIN'                           : 700 * MeV,
                'VertexChi2'                           : 9.,
                'TRCHI2DOFTIGHT'                       : 3.,
                'AM_MIN1'                              : 3030 * MeV,
                'AM_MAX1'                              : 3150 * MeV,
                'AM_MIN2'                              : 9000 * MeV,
                'AM_MAX2'                              : 10800 * MeV,
                'PT_MIN'                               : 15000 * MeV,
            },
 
            'RareWZDiJets': {
                'Reco'                                 : 'TURBO',
                'JetPtMin'                             : 5. * GeV,
                'JetInfo'                              : False,
                'JetEcPath'                            : ''
            },
        },
                    # Now build the final dictionary
        }   )
        return d
        #return {RareWZLines: thresholds}
