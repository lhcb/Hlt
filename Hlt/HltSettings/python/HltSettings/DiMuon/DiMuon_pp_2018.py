from GaudiKernel.SystemOfUnits import GeV, mm, MeV

class DiMuon_pp_2018(object) :
    """
    Threshold settings for Hlt2 DiMuon lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author F. Dettori
    @date 2015-07-019
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            # control rate via: 1) mass: 120 --> 70,
            # DO NOT INCREASE PT!
            'Hlt2DiMuonJPsi',
            'Hlt2DiMuonJPsiHighPT',
            'Hlt2DiMuonPsi2S',
            'Hlt2DiMuonPsi2SHighPT',

            # Do not change 
            'Hlt2DiMuonB',
            'Hlt2DiMuonBUB',
            'Hlt2DiMuonBPrompt',

            'Hlt2DiMuonZ',

            # Control rate via IP cut
            'Hlt2DiMuonSoft',

            # control rate via: DLS cut, dimuon PT
            'Hlt2DiMuonDetached',
            'Hlt2DiMuonDetachedJPsi',
            'Hlt2DiMuonDetachedHeavy',
            'Hlt2DiMuonDetachedPsi2S',

            # Turbo lines 
            'Hlt2DiMuonJPsiTurbo',
            'Hlt2DiMuonPsi2STurbo',
            #'Hlt2DiMuonPsi2SLowPTTurbo',
            'Hlt2DiMuonUpsilonTurbo',
            'Hlt2DiMuonChicXTurbo',
            'Hlt2DiMuonB2KSMuMuLLTurbo',
            'Hlt2DiMuonB2KSMuMuDDTurbo'
            ]

        return lines


    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        # keep pass through thresholds
        d = { }

        from Hlt2Lines.DiMuon.Lines     import DiMuonLines            
        d.update({DiMuonLines :
                  {'Common' :              {'TrChi2'      :   10,
                                            'TrChi2Tight' :    5},
                   'DiMuon' :              {'MinMass'     :     0 * MeV,
                                            'Pt'          :   600 * MeV,
                                            'MuPt'        :   300 * MeV,
                                            'VertexChi2'  :     9 },

                   'BaseDiMuon' :  {  'MuPt'       :     0 * MeV,
                                      'VertexChi2' :    25,
                                      'TRACK_TRGHOSTPROB_MAX': 0.4,
                                      'PIDCut'     : "(MINTREE('mu-' == ABSID, PROBNNmu)>0.1)"},
                
                   
                   'JPsi' :          {'MassWindow' :   120 * MeV,
                                      'Pt'         :     0 * MeV},

                   # do not set any pt cut here!
                   'JPsiNoPT' :      {'MassWindow' :    80 * MeV,
                                      'Pt'         :     0 * MeV},
                   
                   'JPsiHighPT' :    {'Pt'         :  2000 * MeV,
                                      'MassWindow' :   120 * MeV},
                   
                   'Psi2S' :         {'MassWindow' :   120 * MeV,
                                      'Pt'         :  0 * MeV},

                   'Psi2SLowPT' :    {'MassWindow' :   120 * MeV,
                                      'PtMax'      :  2500 * MeV},

                   'Psi2SHighPT' :   {'MassWindow' :   120 * MeV,
                                      'Pt'         :  2000 * MeV},

                   'B' :             {'MinMass'    :   4700 * MeV,
                                      'MaxMass'    :   6500 * MeV,
                                      'doca'       :   0.9 * mm,
                                      'IPChi2Min'  :   2,
                                      'VertexChi2' :    9},

                   'Z' :             {'MinMass'     : 40000 * MeV,
                                      'Pt'          :     0 * MeV},
                   
                   'Soft' :          {'IP'         :   0.3 * mm ,
                                      'IPChi2Min'  :   9,
                                      'IPChi2Max'  :   1000000000000,
                                      'TTHits'     :      -1,
                                      'TRACK_TRGHOSTPROB_MAX': 0.4,
                                      'MaxMass'    :   1000 * MeV,
                                      'VertexChi2' :    25,
                                      'MuProbNNmu' :    0.05,
                                      'Rho'        :     3,
                                      'SVZ'        :   650,
                                      'doca'       :   0.3,
                                      'MinVDZ'     :     0,
                                      'MinBPVDira' :     0,
                                      'MaxIpDistRatio':  1./60,
                                      'cosAngle'   : 0.999998
                                    },                    

                   'DetachedCommon' :  {'Pt'         :     0 * MeV,
                                        'MuPt'       :     0 * MeV,
                                        'VertexChi2' :    25,
                                        'TRACK_TRGHOSTPROB_MAX': 0.4,
                                        'DLS'        :     3},
            
                   'Detached' :            {'IPChi2'      :     25,
                                            'DLS'         :     9},
                   'DetachedHeavy' :       {'MinMass'     :  2950 * MeV,
                                            'Pt'          :     0 * MeV,
                                            'MuPt'        :   300 * MeV,
                                            'VertexChi2'  :    25,
                                            'IPChi2'      :     0,
                                            'DLS'         :     5},       

                   'DetachedJPsi' :        {'MassWindow'  :     120},
                   'DetachedPsi2S' :       {'MassWindow'  :     120},

                   # Turbo lines
                   'JPsiTurbo' :          {'MassWindow' :   120 * MeV,
                                           'Pt'         :     0 * MeV},

                   'Psi2STurbo' :         {'MassWindow' :   120 * MeV,
                                           'Pt'         :     0 * MeV},
                   'Psi2SLowPTTurbo' :    {'MassWindow' :   120 * MeV,
                                           'PtMax'      :  2500 * MeV},

                   'UpsilonTurbo' :       {'MinMass'    :   7900 * MeV,
                                           'VertexChi2' :     25,
                                           'PIDCut'     : "MINTREE('mu-' == ABSID, PROBNNmu) > 0.2" },
                   
                   'ChicXTurbo':          {'MassWindow_comb' :  100 * MeV,
                                           'VertexChi2'      :  25},
                   
                   'ChicXExtraSelK' :        {'VertexChi2'  :  25},
                   'ChicXExtraSelP' :        {'VertexChi2'  :  25},
                   'ChicXExtraSelPi' :       {'VertexChi2'  :  25},
                   'ChicXExtraSelMu' :       {'VertexChi2'  :  25},
                   'ChicXExtraSelKsDD' :       {'VertexChi2'  :  25},
                   'ChicXExtraSelKsLL' :       {'VertexChi2'  :  25},
                   'ChicXExtraSelElectron' : {'VertexChi2'  :  25},
                   'ChicXExtraSelGamma' :    {'VertexChi2'  :  25},
                   'ChicXExtraSelLambdaDD' :   {'VertexChi2'  :  25},
                   'ChicXExtraSelLambdaLL' :   {'VertexChi2'  :  25},
                   
                   # Prescales
                   'Prescale'   : {'Hlt2DiMuon'        :  0,   
                                   'Hlt2DiMuonJPsi'    :  0.2,
                                   'Hlt2DiMuonPsi2S'   :  0.1,
                                   'Hlt2DiMuonJPsiTurbo' : 0.01,
                                   'Hlt2DiMuonPsi2STurbo' : 0.01
                                   }
                   }
                  })
        return d
    
    

