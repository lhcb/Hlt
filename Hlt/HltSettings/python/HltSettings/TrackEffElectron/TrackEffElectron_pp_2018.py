from GaudiKernel.SystemOfUnits import GeV, mm, MeV

class TrackEffElectron_pp_2018 :
    """
    Threshold settings for Hlt2 Hadronic TrackEff lines: 25ns data taking, 2017

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author V. V. Gligorov
    @date 2017-03-29
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            'Hlt2TrackEffElectronDetachedEKTurboCalib',
            'Hlt2TrackEffElectronDetachedEPiTurboCalib',
            'Hlt2TrackEffElectronDetachedMuKTurboCalib',
            'Hlt2TrackEffElectronDetachedMuPiTurboCalib',
            'Hlt2TrackEffElectronDetachedEEKTurboCalib',
            'Hlt2TrackEffElectronDetachedMuMuKTurboCalib',
            'Hlt2TrackEffElectronDetachedEEPhiTurboCalib',
            'Hlt2TrackEffElectronDetachedMuMuPhiTurboCalib',
            'Hlt2TrackEffElectronDetachedEEKstarTurboCalib',
            'Hlt2TrackEffElectronDetachedMuMuKstarTurboCalib',
            'Hlt2TrackEffElectronDetachedProtonProtonPhiTurboCalib'
        ]


        return lines

    def Thresholds(self) :

        d = {}

        from Hlt2Lines.TrackEffElectron.Lines     import TrackEffElectronLines
        d.update ({TrackEffElectronLines : {'Prescale'             : {'Hlt2TrackEffElectronDetachedMuKTurboCalib'  : 0.02,
                                                                      'Hlt2TrackEffElectronDetachedMuPiTurboCalib' : 0.02,
                                                                      'Hlt2TrackEffElectronDetachedEKTurboCalib'   : 0.02,
                                                                      'Hlt2TrackEffElectronDetachedEPiTurboCalib'  : 0.1,
                                                                      'Hlt2TrackEffElectronDetachedProtonProtonPhiTurboCalib': 0.1
                                                                     },
                                            'TrackGEC'             : {'NTRACK_MAX'           : 120}, 
                                            'SharedChild'          : {'TrChi2Mu'   :   5,  
                                                                      'TrChi2Ele'  :   5,  
                                                                      'TrChi2Kaon' :   5,  
                                                                      'TrChi2Pion' :   5,  
                                                                      'IPMu'       :   0.0 * mm, 
                                                                      'IPEle'      :   0.0 * mm, 
                                                                      'IPKaon'     :   0.0 * mm, 
                                                                      'IPPion'     :   0.0 * mm, 
                                                                      'IPChi2Mu'   :   16, 
                                                                      'IPChi2Ele'  :   16, 
                                                                      'IPChi2Kaon' :   16,  
                                                                      'IPChi2Pion' :   36,  
                                                                      'EtaMinMu'   :   1.8,
                                                                      'EtaMinEle'  :   1.8,
                                                                      'EtaMaxMu'   :   4.5,
                                                                      'EtaMaxEle'  :   4.5,
                                                                      'ProbNNe'    :   0.2,
                                                                      'ProbNNmu'   :   0.5,
                                                                      'ProbNNk'    :   0.2,
                                                                      'ProbNNpi'   :   0.8,
                                                                      'PtMu'       :   1800 * MeV,
                                                                      'PtEle'      :   2400 * MeV,
                                                                      'PtKaon'     :   500 * MeV,
                                          							  'PtProton'   :   500 * MeV,
                                                                      'PtPion'     :   1000 * MeV },
                                                            
                            
                                             'DetachedPhi_ForEleTrackEff': {'MINFDZ': 2.5*mm,
                                                                            'MAXVCHI2P': 12.0,
                                                                            'MAXTRCHI2': 3.0,
                                                                            'MINIPCHI2': 6,
                                                                            'KMINPIDK': -1,
                                                                            'MINPT': 250*MeV},
                                             'DetachedKstars_ForEleTrackEff': {'MINFDZ': 2.5*mm,
                                                                            'MAXVCHI2P': 12.0,
                                                                            'MAXTRCHI2': 3.0,
                                                                            'MINIPCHI2': 6,
                                                                            'MINPT': 250*MeV},
                                             'DetachedProbeElectrons': {'IPprobe': 0.04*mm,
                                                                            'MAXTRCHI2probe': 5},
                                             'DetachedProbeMuons': {'IPprobe': 0.08*mm,
                                                                            'MAXTRCHI2probe': 5},
                                             'DetachedProbeProtons': {'IPprobe': 0.05*mm,
                                                                            'MAXTRCHI2probe': 5},
                                             'DetachedProbeKaons': {'IPprobe': 0.03*mm,
                                                                            'MAXTRCHI2probe': 6},
                                             'DetachedEPhi'           : {'AMMAX'         :   6000*MeV,
                                                                         'AMMIN'         :   1500*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedEKstar'           : {'AMMAX'         :   6000*MeV,
                                                                         'AMMIN'         :   1500*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuPhi'           : {'AMMAX'         :   6000*MeV,
                                                                         'AMMIN'         :   1500*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuKstar'           : {'AMMAX'         :   6000*MeV,
                                                                         'AMMIN'         :   1500*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedpPhi'           : {'AMMAX'         :   6000*MeV,
                                                                         'AMMIN'         :   1500*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedpKstar'        : {'AMMAX'         :   6000*MeV,
                                                                         'AMMIN'         :   1500*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedEK'           : {'AM'         :   6000*MeV,
                                                                       'VCHI2'      :   10,
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedEPi'          : {'AM'         :   2250*MeV,
                                                                       'VCHI2'      :   10, 
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuK'          : {'AM'         :   5500*MeV,
                                                                       'VCHI2'      :   10, 
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuPi'         : {'AM'         :   2100*MeV,
                                                                       'VCHI2'      :   10, 
                                                                       'VDCHI2'     :   100,
                                                                       'DIRA'       :   0.995,
                                                                       'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuMuK'        : {'AMTAP'      :   5800*MeV,
                                                                       'VCHI2TAP'   :   20,
                                                                       'MINCONSTRAINTDIRA'    :   0.998,
                                                                       'MINFDZ'     :   2.8*mm,
                                                                       'MLOW'       :   5000.*MeV,
                                                                       'MHIGH'      :   5800.*MeV,
                                                                       'CORRMMIN'   :   2000 * MeV,
                                                                       'CORRMMAX'   :   6750 * MeV,
                                                                       'PMINMUON'   :   2000 * MeV,
                                                                       'PMAXMUON'   :   100000*MeV,
                                                                       'PVMMIN'     :   4400*MeV,
                                                                       'PVMMAX'     :   6200*MeV,
                                                                       'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                      },
                                             'DetachedEEK'          : {'AMTAP'      :   6000*MeV,
                                                                       'VCHI2TAP'   :   20,
                                                                       'MLOW'       :   4500.*MeV,
                                                                       'MHIGH'      :   6000.*MeV,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedEEPhi'          : {
                                                                         'AMTAP'      :   6000*MeV,
                                                                       'VCHI2TAP'   :   20,
                                                                       'MLOW'       :   4000.*MeV,
                                                                       'MHIGH'      :   6000.*MeV,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedEEKstar'          : {
                                                                       'AMTAP'      :   6000*MeV,
                                                                       'VCHI2TAP'   :   20,
                                                                       'MLOW'       :   4000.*MeV,
                                                                       'MHIGH'      :   6000.*MeV,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuMuPhi'          : {
                                                                       'AMTAP'      :   5800*MeV,
                                                                       'VCHI2TAP'   :   20,
                                                                       'MINFDZ'     :   2.5 * mm,
                                                                       'MLOW'       :   5000.*MeV,
                                                                       'MHIGH'      :   5800.*MeV,
                                                                       'MINCONSTRAINTDIRA'    :   0.999,
                                                                       'CORRMMIN'   :   2200 * MeV,
                                                                       'CORRMMAX'   :   6750 * MeV,
                                                                       'PMINMUON'   :   2000 * MeV,
                                                                       'PMAXMUON'   :   100000*MeV,
                                                                       'PVMMIN'     :   4300*MeV,
                                                                       'PVMMAX'     :   6300*MeV,
                                                                       'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                      },
                                             'DetachedMuMuKstar'          : {
                                                                       'AMTAP'      :   5800*MeV,
                                                                       'VCHI2TAP'   :   20,
                                                                       'MINFDZ'     :   3.0*mm,
                                                                       'MINCONSTRAINTDIRA'    :   0.999,
                                                                       'MLOW'       :   4500.*MeV,
                                                                       'MHIGH'      :   6000.*MeV,
                                                                       'CORRMMIN'   :   2000 * MeV,
                                                                       'CORRMMAX'   :   6750 * MeV,
                                                                       'PMINMUON'   :   2000 * MeV,
                                                                       'PMAXMUON'   :   100000*MeV,
                                                                       'PVMMIN'     :   4400*MeV,
                                                                       'PVMMAX'     :   6200*MeV,
                                                                       'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                      },
                                             'DetachedProtonProtonPhi'  : {
                                                                       'AMTAP'      :   6000*MeV,
                                                                       'VCHI2TAP'   :   15,
                                                                       'MLOW'       :   4500.*MeV,
                                                                       'MHIGH'      :   6000.*MeV,
                                                                       'PMIN'       :   3000*MeV,
                                                                       'PMAX'       :   65000*MeV,
                                                                       'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                      },
                                             'L0Req'                : {'DetachedMuMuK': "L0_CHANNEL('Muon')",
                                                                       'DetachedMuMuPhi'  : "L0_CHANNEL('Muon')",
                                                                       'DetachedMuMuKstar'  : "L0_CHANNEL('Muon')",
                                                                       'DetachedEEK'  : "L0_CHANNEL('Electron')",
                                                                       'DetachedEEPhi'  : "L0_CHANNEL('Electron')|L0_CHANNEL('Muon')",
                                                                       'DetachedEEKstar'  : "L0_CHANNEL('Electron')|L0_CHANNEL('Muon')",
                                                                       'DetachedProtonProtonPhi'  : "L0_CHANNEL('Hadron')|L0_CHANNEL('Muon')",
                                                                       'DetachedEK'   : "L0_CHANNEL('Electron')",
                                                                       'DetachedEPi'  : "L0_CHANNEL('Electron')",
                                                                       'DetachedMuK'  : "L0_CHANNEL('Muon')",
                                                                       'DetachedMuPi' : "L0_CHANNEL('Muon')" },
                                             'Hlt1Req'              : {'DetachedMuMuK': "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                                                       'DetachedMuMuPhi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedMuMuKstar'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedEEK'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedEEPhi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedEEKstar'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedProtonProtonPhi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedEK'   : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedEPi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                       'DetachedMuK'  : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                                                       'DetachedMuPi' : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')" }
                           
                 }
                 })
        return d

