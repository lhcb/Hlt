from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds


def __get_conf__(folder, suffix):
    import importlib
    conf = folder + suffix
    module = importlib.import_module("HltSettings.{0}.{1}".format(folder, conf))
    return getattr(module, conf)()


class HeavyIon_2018( object ):
    """Settings for Heavy ion 2018 data taking, Pb-Pb and Pb-Neon SMOG physics (LBHLT-404).
    PbPb/PbNe: 500/200 bunch crossings, possible reduction of beta* (3m to 1.5 m)

    PbPb: Based on XenonXenon_2017

    @author P. Robbe, R. Matev, S. Stahl, Y. Zhang
    @date 2018-07-11
    """

    def __init__(self):
        # No TURBO => no aferburner
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

        ####################################################################
        #Special settings for beam gas reconstruction, heavy ion GECs
        ####################################################################
        # Extend velo tracking search range in z direction to [-2000,2000]
        from Configurables import HltRecoConf
        HltRecoConf().BeamGasMode = True
        HltRecoConf().VeloTrackingZMin = -2000. * mm
        HltRecoConf().VeloTrackingZMax =  2000. * mm
        # Loosen the radial cut for standard Hlt PVs
        HltRecoConf().PVOptions = {
            "UseBeamSpotRCut": True,
            "BeamSpotRCut": 4 * mm,
            "TrackErrorScaleFactor": 1.,
            "ResolverBound": 5 * mm,
            "MinTracks": 4.,
            "trackMaxChi2": 12.,
            "BeamSpotRHighMultiplicityCut": 4 * mm,
            "BeamSpotRMultiplicityTreshold": 10,
        }
        ####################################################################
        #Special settings for tracking and GEC
        ####################################################################
        HltRecoConf().HeavyIonGECVeloLow  = 6000   #tight
        HltRecoConf().HeavyIonGECVeloHigh = 10000  #loose
        HltRecoConf().Forward_HPT_MinPt = 400.*MeV
        HltRecoConf().MaxTrGHOSTPROB    = 0.8
        HltRecoConf().MaxTrCHI2PDOF     = 4.0

        #HltRecoConf().ApplyGHOSTPROBCutInHLT1 = False #default True
        #HltRecoConf().MaxTrGHOSTPROB = 0.8? #default 0.4

        ####################################################################


        ####################################################################
        #Special settings concerning very busy events
        ####################################################################
        # Tighter GEC for the Hlt1TrackMonitor
        from Configurables import HltMonitoringConf
        HltMonitoringConf().Hlt1TrackMonitorGEC = 'HeavyIonsTight'

        from HltLine.HltDecodeRaw import DecodeVELO
        DecodeVELO.members()[0].MaxVeloClusters = 99999
        from DAQSys.Decoders import DecoderDB
        DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Properties["MaxVeloClusters"] = 99999

        HltRecoConf().Forward_MaxOTHits = 99999

        from HltConf.Hlt2 import Hlt2Conf
        Hlt2Conf().DefaultHlt1Filter = "assert False"
        # Hlt2Conf().DefaultHlt1Filter = "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB)"\
        #         "(?!BBHighMult)(?!BEHighMult)(?!BBVeryHighMult)(?!BEVeryHighMult).*Decision')"
        #Hlt2Conf().DefaultHlt1Filter = "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB).*Decision')"

        ####################################################################

        ####################################################################
        # Extend physics reach at low p/pt
        ####################################################################
        #how can we select low pt electrons for lumi (typical p/pt: >0.1/2 GeV?)
        from Hlt1Lines.Hlt1SharedParticles import Hlt1SharedParticles
        Hlt1SharedParticles(**{
            'PreFitPT'        : 380,
            'PreFitP'         : 2850,
            'PT'              : 400,     # MeV
            'P'               : 3000,
            'TrackChi2DOF'    : 4.,
            'TrGP'            :
            999.,  # ghost prob
            })
        ####################################################################

        ####################################################################
        # Set the lines for the data quality
        ####################################################################
        from HltConf.HltOutput import HltOutputConf
        HltOutputConf().Hlt2LinesForDQ = ['Hlt2BBPassThrough','Hlt2SMOGPassThrough','Hlt2MBMicroBiasVelo',
                'Hlt2MBMicroBiasLowMultVelo', 'Hlt2SingleTrack','Hlt2BELongTrack', 'Hlt2BBSMOGD02KPi', 'Hlt2BBLongTrack']

        # Non-default monitoring bits
        HltOutputConf().OverrideBits = {
            # RB 37 Beam-Beam collisions for Velo alignment
            37: "HLT_PASS_RE('Hlt1BBMicroBiasVeloDecision')",
            # RB 46 HLT1 physics for monitoring and alignment
            46: "HLT_PASS_RE('Hlt1(?!Lumi)(?!Tell1)(?!MB)(?!Velo)(?!BeamGas).*Decision')",
            58: "HLT_PASS('Hlt1BBMicroBiasVeloDecision')",
        }

    def verifyType(self,ref) :
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if ( self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self)  or self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self) ) :
            raise RuntimeError( 'Must update HltType when modifying ActiveHlt.Lines()' )

    def L0TCK(self) :
        return '0x1821'

    def HltType(self) :
        self.verifyType( HeavyIon_2018 )
        return          'HeavyIon_2018'

    def SubDirs(self):
        # use LowMult only for Hlt2NoBiasNonBeamBeam
        # for physics we now use LongTrack
        return {'HeavyIon2018': ['HeavyIons', 'Technical', 'LowMult']}

    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        from Hlt1Lines.Hlt1MuonLines            import Hlt1MuonLinesConf  #for muons
        from Hlt1Lines.Hlt1IFTLines             import Hlt1IFTLinesConf   #MB and MB high multiplicity
        from Hlt1Lines.Hlt1MBLines              import Hlt1MBLinesConf    #Nobias
        from Hlt1Lines.Hlt1LowMultLines         import Hlt1LowMultLinesConf
        from Hlt1Lines.Hlt1L0Lines              import Hlt1L0LinesConf  #
        from Hlt1Lines.Hlt1SMOGLines            import Hlt1SMOGLinesConf

        from Hlt1Lines.Hlt1LumiLines            import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1CommissioningLines   import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines         import Hlt1BeamGasLinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines   import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf

        thresholds = {
                Hlt1IFTLinesConf: {
                    'ODIN': {
                        'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                        'BENoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                        'EBNoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                        'BEMicroBiasVelo': '',
                        'EBMicroBiasVelo': '',
                        'BBMicroBiasVelo': '',
                        'BBHighMult': '',
                        'BEHighMult': '',
                        'BBVeryHighMult': '',
                        'BEVeryHighMult': '',
                        'BEMicroBiasLowMultVeloNoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                        'BBHasTrack':  '',
                        'BEHasTrack':  '',
                        },
                    'L0':{  #TODO, separate soft CEP with the rest
                        'BBMicroBiasLowMultVelo': " | ".join( ["L0_CHANNEL('%sLowMult')"%(chn,) for chn in ["Hadron", "Muon", "Electron", "DiEM", "SPD"]]),  #avoid including SoftCEP
                        'BBMicroBiasVelo': " | ".join( ["L0_CHANNEL('%s')"%(chn,) for chn in ["Hadron", "HadronHighMult", "Muon", "SPD"]]), #avoid including SoftCEP
                        'BBMicroBiasSoftCEP'    : " | ".join( ["L0_CHANNEL('%sLowMult')"%(chn,) for chn in ["Hadron", "Muon", "Electron", "DiEM", "SPD"]]) + " | L0_CHANNEL('SoftCEP')", #CEP
                        #'BBMicroBiasSoftCEP'    : "L0_CHANNEL('SoftCEP')", #CEP
                        'BEMicroBiasVelo': " | ".join( ["L0_CHANNEL('%s')"%(chn,) for chn in ["HadronBE", "HadronHighMultBE", "MuonBE", "SPDBE"]]), #avoid including ElectronBE
                        'EBMicroBiasVelo': " | ".join( ["L0_CHANNEL('%s')"%(chn,) for chn in ["HadronBE", "HadronHighMultBE", "MuonBE", "SPDBE"]]), #avoid including ElectronBE
                        'BEMicroBiasLowMultVelo': "L0_CHANNEL('ElectronBE')"

                        },
                    'MinVeloTracks': {
                        'BBMicroBiasVelo': 1,
                        'BBMicroBiasLowMultVelo': 1,
                        'BEMicroBiasVelo': 1,
                        'EBMicroBiasVelo': 1,
                        'BEMicroBiasLowMultVelo': 1,
                        'BBMicroBiasSoftCEP': 1,
                        },
                    'MaxVeloTracks': {
                        'BEMicroBiasLowMultVelo': 15,
                        },
                    'GEC': {
                        'BEMicroBiasVelo': 'HeavyIonsTight',              # N1<X := 6K
                        'EBMicroBiasVelo': 'HeavyIonsTight',              # ..
                        'BEMicroBiasLowMultVelo': 'HeavyIonsTight',       # ..
                        'BEMicroBiasLowMultVeloNoBias': 'HeavyIonsTight', # ..
                        'BBMicroBiasVelo': 'HeavyIonsTight',              # ..
                        'BBMicroBiasLowMultVelo': 'HeavyIonsTight',       # ..
                        'BBMicroBiasSoftCEP': 'HeavyIonsTight',       # ..
                        'BBHighMult': 'HeavyIonsPass',                    # N1<X<N2 := 10K
                        'BBVeryHighMult': 'HeavyIonsLoose',               # N2<X
                        'BEHighMult': 'HeavyIonsPass',                    # N1<X<N2
                        'BEVeryHighMult': 'HeavyIonsLoose',               # N2<X
                        'BBHasTrack': 'HeavyIonsTight',                   # N1<X
                        'BEHasTrack': 'HeavyIonsTight'                   # ..
                        },
                     'BEMicroBiasLowMultVelo': {    #TODO why range so tight (at least one track in range). Also requires nTrackBack<3
                         'MinEta': 1.0,   #3.5 -> 1.5 to allow physics tracks to be selected
                         'MaxEta': 7.0
                     },
                     'BEMicroBiasLowMultVeloNoBias': { 
                         'MinEta': 3.5,
                         'MaxEta': 7.0
                     },
                     'SingleTrack':{
                          "ParticlePT": 400,       #B(E|B)HasTrack, min PT. ..HltHPTtracking -> pion->..
                          "SingleTrackTrGP": 0.8,   #B(E|B)HasTrack, max ghost probability
                          "TrChi2": 4,   #B(E|B)HasTrack, max track chi2/ndof
                     },
                    'Prescale': {
                        'Hlt1BENoBias': 0.016,  # ~50 Hz from 3.2 kHz
                        'Hlt1EBNoBias': 0.016,  # ~50 Hz from 3.2 kHz
                        'Hlt1BBNoBias': 0.05,   # ~100 Hz from 2 kHz
                        'Hlt1BBMicroBiasVelo': 1.,   #propagated to Hlt2, with prescales easily applied
                        'Hlt1BEMicroBiasVelo': 1.,   #
                        'Hlt1EBMicroBiasVelo': 0.1,   #
                        'Hlt1BBHighMult': 1,
                        'Hlt1BEHighMult': 1,
                        'Hlt1BBHasTrack': 1,
                        'Hlt1BEHasTrack': 1,
                        'Hlt1BBVeryHighMult': 0.001,
                        'Hlt1BEVeryHighMult': 0.001,
                        'Hlt1BBMicroBiasLowMultVelo':1, #propagated to Hlt2, with prescales easily applied
                        'Hlt1BBMicroBiasSoftCEP':1, #CEP
                        'Hlt1BEMicroBiasLowMultVelo':1, #propagated to Hlt2, with prescales easily applied
                        }
                    },
                Hlt1L0LinesConf: {
                    'L0Channels': ['PhotonLowMult'],   #TODO
                    'Prescale': {
                        'Hlt1L0PhotonLowMult': 1,
                        #'Hlt1L0SoftCEP': 1, # HLT2 for SoftCEP (phi -> KK), not needed as will put in MBSoftCEP
                        },
                    },
                Hlt1LumiLinesConf : {
                    'RecoGEC': 'HeavyIonsTight',
                    # 'L0Channel': ['B1gas', 'B2gas'],
                    },
                Hlt1BeamGasLinesConf: {
                    # Global behaviour settings
                    'TrackingConf'          : 'FastVelo',
                    'FitTracks'             : True,
                    'PVFitter'              : 'LSAdaptPV3DFitter',
                    'PVSeeding'             : 'PVSeed3DTool',
                    'SplitVertices'         : True,
                    'CreateGlobalSelection' : False,
                    'Turbo'                 : False,
                    'UseGEC'                : 'HeavyIonsTight',

                    # Minimum number of tracks for the produced vertices (#tr/vtx > X)
                    'VertexMinNTracks'          : 9,  # strictly greater than
                    'FullZVertexMinNTracks'     : 9,  # strictly greater than
                    'Beam1VtxMaxBwdTracks'      : -1,  # less or equal than, negative to switch off
                    'Beam2VtxMaxFwdTracks'      : -1,  # less or equal than, negative to switch off

                    # z-ranges for Vertexing
                    'Beam1VtxRangeLow'        : -2000.,
                    'Beam1VtxRangeUp'         :  2000.,
                    'Beam2VtxRangeLow'        : -2000.,
                    'Beam2VtxRangeUp'         :  2000.,
                    # Luminous region exclusion range
                    'BGVtxExclRangeMin'       :  -250.,
                    'BGVtxExclRangeMax'       :   250.,

                    'L0Filter' : {
                        'BeamGasNoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'BeamGasNoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'BeamGasBeam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'BeamGasBeam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'BeamGasCrossingForcedReco': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'BeamGasCrossingForcedRecoFullZ': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        },

                    'Prescale' : {
                        'Hlt1BeamGasNoBeamBeam1'             : 1.,
                        'Hlt1BeamGasNoBeamBeam2'             : 1.,
                        'Hlt1BeamGasBeam1'                   : 0.04,
                        'Hlt1BeamGasBeam2'                   : 0.04,
                        'Hlt1BeamGasCrossingForcedReco'      : 0.04,
                        'Hlt1BeamGasCrossingForcedRecoFullZ' : 0.01,
                        },
                    'Postscale' : {
                        'Hlt1BeamGasNoBeamBeam1'             : 1.,
                        'Hlt1BeamGasNoBeamBeam2'             : 1.,
                        'Hlt1BeamGasBeam1'                   : 1.,
                        'Hlt1BeamGasBeam2'                   : 1.,
                        'Hlt1BeamGasCrossingForcedReco'      : 1.,
                        'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                        },
                    },

                 Hlt1MuonLinesConf :     {
                        'SingleMuonHighPT_P'       : 3000,
                        'SingleMuonHighPT_PT'      : 600,
                        'SingleMuonHighPT_TrChi2'  : 4.,
                        'SingleMuonHighPT_TrGP'    : 999.,
                        'SingleMuonHighPT_GEC'     : 'HeavyIonsTight',

                        'DiMuonHighMass_VxDOCA'    :  1,
                        'DiMuonHighMass_VxChi2'    :   25,
                        'DiMuonHighMass_P'         : 3000,
                        'DiMuonHighMass_PT'        :  400,
                        'DiMuonHighMass_TrChi2'    :    4,
                        'DiMuonHighMass_M'         :    0,  # 2700->0 MeV,  #...
                        'DiMuonHighMass_GEC'       : 'HeavyIonsTight',

                        'SMOGDiMuonHighMass_VxDOCA'    :  1,
                        'SMOGDiMuonHighMass_VxChi2'    :   25,
                        'SMOGDiMuonHighMass_P'         : 3000,
                        'SMOGDiMuonHighMass_PT'        :  400,
                        'SMOGDiMuonHighMass_TrChi2'    :    4,
                        'SMOGDiMuonHighMass_M'         : 0.,  # 2700->2500 MeV,  #...
                        'SMOGDiMuonHighMass_GEC'       : 'HeavyIonsTight',

                        'CalibMuonAlignJpsi_ParticlePT'             : 800,     # MeV
                        'CalibMuonAlignJpsi_ParticleP'              : 6000,    # MeV
                        'CalibMuonAlignJpsi_TrackCHI2DOF'           : 2,       # dimensionless
                        'CalibMuonAlignJpsi_CombMaxDaughtPT'        : 800,     # MeV
                        'CalibMuonAlignJpsi_CombAPT'                : 1500,    # MeV
                        'CalibMuonAlignJpsi_CombDOCA'               : 0.2,     # mm
                        'CalibMuonAlignJpsi_CombVCHI2DOF'           : 10,     # dimensionless
                        'CalibMuonAlignJpsi_CombVCHI2DOFLoose'      : 10,      # dimensionless
                        'CalibMuonAlignJpsi_CombDIRA'               : 0.9,     # dimensionless
                        'CalibMuonAlignJpsi_CombTAU'                : 0.,     # ps
                        'CalibMuonAlignJpsi_JpsiMassWinLoose'       : 150,     # MeV
                        'CalibMuonAlignJpsi_JpsiMassWin'            : 100,     # MeV
                        'CalibMuonAlignJpsi_GEC'                    : 'HeavyIonsTight',

                        'L0Channels'               : {
                            'SingleMuonHighPT' : ( 'Muon','MuonLowMult'),
                            'DiMuonHighMass'   : ( 'Muon','MuonLowMult'),
                            'SMOGDiMuonHighMass'   : ( 'MuonBE',),
                            'CalibMuonAlignJpsi'    : ( 'Muon','MuonLowMult'),
                            },
                        'Prescale'                 : {
                            'Hlt1SingleMuonHighPT': 1.0,
                            'Hlt1DiMuonHighMass' : 1.0,
                            'Hlt1SMOGDiMuonHighMass' : 1.0,
                            'Hlt1CalibMuonAlignJpsi' : 1.0,
                            },
                        'Priorities'               : {
                            'SingleMuonHighPT' : 8,
                            'DiMuonHighMass'   : 6,
                            }
                        },
                Hlt1SMOGLinesConf: {
                     'ParticlePT':          400,  # MeV  # 600 -> 400 #...
                     'ParticleP':           3000,  # MeV 4000->3000
                     'TrackCHI2DOF':        4,
                     'CombMaxDaughtPT':     400,  # MeV  900 -> 400 #...
                     'CombDOCA':            1.0,  # mm
                     'CombVCHI2DOF':        25,
                     'MassWinLoose':        200,  # MeV
                     'MassWin':             150,  # MeV
                     'GenericMassMinLoose': 0,  # MeV
                     'GenericMassMin':      0,  # MeV
                     'GenericMaxDaughtPT':  800,  # MeV
                     'SingleTrackPT':       800,  # MeV
                     'GEC':                 'HeavyIonsTight',
                     'L0':    'L0_ALL',
                     'L0_BB': 'L0_ALL',
                     'ODIN':    '(ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2)',
                     'ODIN_BB': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing)',
                     'Prescale': {
                        'Hlt1SMOGKPi': 0.001,
                        'Hlt1SMOGKPiPi': 0.001,
                        'Hlt1SMOGKKPi': 0.001,
                        'Hlt1SMOGpKPi': 0.001,
                        'Hlt1SMOGGeneric': 0.001,
                        'Hlt1SMOGSingleTrack': 0.001,
                        'Hlt1BBSMOGKPi': 0.001,
                        'Hlt1BBSMOGKPiPi': 0.001,
                        'Hlt1BBSMOGKKPi': 0.001,
                        'Hlt1BBSMOGpKPi': 0.001,
                        'Hlt1BBSMOGGeneric': 0.001,
                        'Hlt1BBSMOGSingleTrack': 0.001,
                     }
                 },
                Hlt1CalibTrackingLinesConf :  { 'ParticlePT'            : 600     # MeV
                        ,'ParticleP'             : 4000    # MeV
                        ,'ParticlePT_LTUNB'      : 600     # MeV
                        ,'ParticleP_LTUNB'       : 4000    # MeV
                        ,'L0_DHH_LTUNB'          : ['HadronLowMult',"MuonLowMult",'Hadron','Muon']
                        ,'L0_DHH_LTUNB_Lines'    : ['CalibTrackingPiPi','CalibTrackingKPi','CalibTrackingKK']
                        ,'TrackCHI2DOF'          : 2       # dimensionless
                        ,'CombMaxDaughtPT'       : 900     # MeV 900
                        ,'CombAPT'               : 1800    # MeV 1200
                        ,'CombDOCA'              : 0.1     # mm
                        ,'CombVCHI2DOF'          : 10      # dimensionless
                        ,'CombVCHI2DOFLoose'     : 15      # dimensionless
                        ,'CombDIRA'              : 0.99    # dimensionless
                        ,'CombTAU'               : 0.25    # ps
                        ,'D0MassWinLoose'        : 100     # MeV
                        ,'D0MassWin'             : 60      # MeV
                        ,'B0MassWinLoose'        : 200     # MeV
                        ,'B0MassWin'             : 150     # MeV
                        ,'D0DetachedDaughtsIPCHI2': 9      # dimensionless
                        ,'D0DetachedIPCHI2'       : 9      # dimensionless
                        ,'BsPhiGammaMassMinLoose': 3350    # MeV
                        ,'BsPhiGammaMassMaxLoose': 6900    # MeV
                        ,'BsPhiGammaMassMin'     : 3850    # MeV
                        ,'BsPhiGammaMassMax'     : 6400    # MeV
                        ,'PhiMassWinLoose'       : 50      # MeV
                        ,'PhiMassWin'            : 30      # MeV
                        ,'PhiMassWinTight'       : 20      # MeV
                        ,'PhiPT'                 : 1800    # MeV
                        ,'PhiPTLoose'            : 800     # MeV
                        ,'PhiSumPT'              : 3000    # MeV
                        ,'PhiIPCHI2'             : 16      # dimensionless
                        ,'B0SUMPT'               : 4000    # MeV
                        ,'B0PT'                  : 1000    # MeV
                        ,'GAMMA_PT_MIN'          : 2000    # MeV
                        ,'Velo_Qcut'             : 999     # OFF
                        ,'TrNTHits'              : 0       # OFF
                        ,'ValidateTT'            : False
                        ,'GEC'            : "HeavyIonsTight"
                        ,'Prescale'  : { 'Hlt1CalibTrackingKPiDetached': 1.0 }
                        },
                Hlt1CommissioningLinesConf: {
                        'Prescale': {
                            'Hlt1VeloClosingMicroBias': 1,
                            'Hlt1VeloClosingPV': 1,
                            },
                        'Postscale': {
                            'Hlt1VeloClosingMicroBias': 'RATE(500)',
                            'Hlt1VeloClosingPV': 'RATE(500)',
                            },
                        'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                        'ODIN': {
                            'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                            },
                        'L0': {
                            'VeloClosingPV': "L0_DECISION(LHCb.L0DUDecision.Any)",
                            },
                        'VeloClosingPV': {
                            'ZMin': -150 * mm,
                            'ZMax': 150 * mm,
                            'MinBackwardTracks': 1,
                            'MinForwardTracks': 1,
                            },
                        'GEC': 'HeavyIonsTight',
                        },
                Hlt1MBLinesConf: {
                    'Prescale': {'Hlt1MBNoBias': 1},
                },
                Hlt1LowMultLinesConf: {
                    'Prescale': {'Hlt1NoBiasEmptyEmpty': 0.05},  # 1.6 kHz -> 80 Hz
                },
                Hlt1CalibRICHMirrorLinesConf : {
                        'Prescale' : {
                            'Hlt1CalibHighPTLowMultTrks'     : 0.0001,
                            'Hlt1CalibRICHMirrorRICH1'       : 1.0,
                            'Hlt1CalibRICHMirrorRICH2'       : 1.0
                            }
                        , 'DoTiming' : False
                        , 'R2L_PreFitPT'    : 475. * MeV
                        , 'R2L_PreFitP'     : 38000. * MeV 
                        , 'R2L_PT'       : 500. * MeV
                        , 'R2L_P'        : 40000. * MeV
                        , 'R2L_MinETA'   : 2.65
                        , 'R2L_MaxETA'   : 2.80
                        , 'R2L_Phis'     : [ ( -2.69, -2.29 ), ( -0.85, -0.45 ), ( 0.45, 0.85 ), ( 2.29, 2.69 ) ]
                        , 'R2L_TrChi2'   : 2.
                        , 'R2L_MinTr'    : 0.5
                        , 'R2L_GEC'      : 'HeavyIonsTight'
                        , 'R1L_PreFitPT' : 475. * MeV
                        , 'R1L_PreFitP'  : 19000. * MeV 
                        , 'R1L_PT'       : 500. * MeV
                        , 'R1L_P'        : 20000. * MeV
                        , 'R1L_MinETA'   : 1.6
                        , 'R1L_MaxETA'   : 2.04
                        , 'R1L_Phis'   : [ ( -2.65, -2.30 ), ( -0.80, -0.50 ), ( 0.50, 0.80 ), ( 2.30, 2.65 ) ]
                        , 'R1L_TrChi2'   : 2.
                        , 'R1L_MinTr'    : 0.5
                        , 'R1L_GEC'      : 'HeavyIonsTight'
                        , 'LM_PreFitPT' : 475. * MeV
                        , 'LM_PreFitP'  : 950. * MeV 
                        , 'LM_PT'    : 500. * MeV
                        , 'LM_P'     : 1000. * MeV
                        , 'LM_TrChi2': 2.
                        , 'LM_MinTr' : 1
                        , 'LM_MaxTr' : 40
                        , 'LM_GEC'   : 'HeavyIonsTight'
                        }
                } # Thresholds of HLT1

        from Hlt2Lines.SMOG.Lines import SMOGLines
        thresholds.update({
            SMOGLines: {
                'Prescale': {
                    "Hlt1BBLongTrack" : 1.,
                    "Hlt1BELongTrack" : 1.,
                    "Hlt1BBSMOGD02KPi": 1.
                    },
                'Postscale': {},
                'Hlt1Req':{
                    'BBLongTrack' : "HLT_PASS('Hlt1BBMicroBiasSoftCEPDecision')",
                    'BBSMOGD02KPi': "HLT_PASS('Hlt1BBMicroBiasSoftCEPDecision')", #modified to select two-track event (w/o PV requirements)
                    'BELongTrack' : "HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision')",
                    },
                'Common':{
                    'Trk_ALL_TRCHI2DOF_MAX'    :  10.0,                                   
                    'Trk_ALL_P_MIN'            :  0 * MeV,                            
                    'VCHI2PDOF_MAX'            :  50.0,                                  
                    },
                'D02HH': {
                    'TisTosSpec'               : [],        #<- "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : [],        #<- "Hlt1BBSMOG.*Decision%TOS",
                    'Pair_AMINDOCA_MAX'        : 1.,        #<- 0.10 * mm,
                    'Trk_Max_APT_MIN'          : 0.*MeV,    #<- 400.0 * MeV,  
                    'D0_VCHI2PDOF_MAX'         : 50,        #<- 10.0, 
                    'Comb_AM_MIN'              : 0.*MeV,    #<- 1775.0 * MeV,
                    'Comb_AM_MAX'              : 1000.*GeV, #<- 1955.0 * MeV,
                    'Trk_ALL_PT_MIN'           : 0.*MeV,    #<- 250.0 * MeV,
                    'Trk_ALL_P_MIN'            : 0.*MeV,    #<- 2.0  * GeV,
                    'Mass_M_MIN'               : 0.*MeV,    #<- 1784.0 * MeV,
                    'Mass_M_MAX'               : 1000.*GeV, #<- 1944.0 * MeV,
                },
                'BBLongTrack': {
                    'PID_LIM'                  : -10000.,
                    'Trk_ALL_PT_MIN'           : 0.0 * MeV,
                    'Trk_ALL_P_MIN'            : 0.0 * MeV,
                    'Trk_ALL_TRCHI2DOF_MAX'    : 10.0 * MeV,
                },
                'BELongTrack': {
                    'PID_LIM'                  : -10000.,
                    'Trk_ALL_PT_MIN'           : 0.0 * MeV,
                    'Trk_ALL_P_MIN'            : 0.0 * MeV,
                    'Trk_ALL_TRCHI2DOF_MAX'    : 10.0 * MeV,
                },
            }
        })# Thresholds of HLT2 SMOG


        # HLT2 thresholds from individual files
        sds = self.SubDirs()
        for monthyear, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = __get_conf__(subdir, "_%s" %(monthyear))
                update_thresholds(thresholds, conf.Thresholds())

        return thresholds

    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        hlt2 = [
                "Hlt2BBSMOGD02KPi",
                "Hlt2BBLongTrack",
                "Hlt2BELongTrack",
                ]
        sds = self.SubDirs()
        for monthyear, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = __get_conf__(subdir, "_%s" %(monthyear))
                hlt2.extend(conf.ActiveHlt2Lines())

        """
        lines = [  #HeavyIons
            'Hlt2BBPassThrough',
            'Hlt2SMOGPassThrough',
            'Hlt2BBHighMult',
            'Hlt2BEHighMult',
        ]
        """

        return hlt2

    def ActiveHlt1Lines(self) :
        """
        Returns a list of active lines
        """
        lines =  [
                #--->>> MB lines ==> Hlt2BBPassThrough | Hlt2BBHighMult
                'Hlt1BBNoBias',
                'Hlt1BBMicroBiasLowMultVelo',  #CEP
                'Hlt1BBMicroBiasSoftCEP',  #CEP
                'Hlt1BBHasTrack',
                'Hlt1BBMicroBiasVelo' , 'Hlt1BBHighMult', 'Hlt1BBVeryHighMult',
                #--->>> BE/EB MB lines ==> Hlt2SMOGPassThrough | Hlt2BEHighMult
                'Hlt1BENoBias',
                'Hlt1BEMicroBiasLowMultVelo',  #Lumi
                'Hlt1BEHasTrack',
                'Hlt1BEMicroBiasVelo', 'Hlt1BEHighMult', 'Hlt1BEVeryHighMult',
                'Hlt1EBNoBias',
                'Hlt1EBMicroBiasVelo',
                #--->>> photon CEP line ==> Hlt2BBPassThrough
                'Hlt1L0PhotonLowMult',
                #'Hlt1L0SoftCEP',
                #--->>>
                #'Hlt1L0Any',
                #--->>> Muon lines  ==> Hlt2BBPassThrough/Hlt2SMOGPassThrough
                'Hlt1SingleMuonHighPT' , 'Hlt1DiMuonHighMass', 'Hlt1SMOGDiMuonHighMass',
                #--->>> SMOG lines ==> Hlt2SMOGPassThrough
                'Hlt1SMOGKPi',
                'Hlt1SMOGKPiPi',
                'Hlt1SMOGKKPi',
                'Hlt1SMOGpKPi',
                'Hlt1SMOGGeneric',
                'Hlt1SMOGSingleTrack',
                #--->>> SMOG lines for BB, #TODO do we need these lines ==> Hlt2BBPassThrough
                'Hlt1BBSMOGKPi',
                'Hlt1BBSMOGKPiPi',
                'Hlt1BBSMOGKKPi',
                'Hlt1BBSMOGpKPi',
                'Hlt1BBSMOGGeneric',
                'Hlt1BBSMOGSingleTrack',
                #--->>> Calibration lines ==> disk buffer only
                'Hlt1CalibTrackingKPiDetached',
                'Hlt1CalibHighPTLowMultTrks',
                'Hlt1IncPhi', 'Hlt1CalibMuonAlignJpsi',
                #--->>> RICH calibration ==> disk buffer only
                'Hlt1CalibRICHMirrorRICH1',
                'Hlt1CalibRICHMirrorRICH2',
                #--->>> Lumi calbiration lines ==> Hlt2Lumi
                'Hlt1Lumi', 'Hlt1LumiSequencer',
                'Hlt1LumiHighMult', 'Hlt1LumiHighMultSequencer',
                #--->>> Beamgas lines for ghost charge / monitoring ==> Hlt2BeamGas
                'Hlt1BeamGasBeam1', 'Hlt1BeamGasBeam2',
                'Hlt1BeamGasNoBeamBeam1', 'Hlt1BeamGasNoBeamBeam2',
                'Hlt1BeamGasCrossingForcedReco', 'Hlt1BeamGasCrossingForcedRecoFullZ',
                #--->>> Velo closing ==> disk buffer only
                'Hlt1VeloClosingMicroBias',
                'Hlt1VeloClosingPV',
                #--->>> Technical lines
                'Hlt1Tell1Error',  # ==> disk buffer only (monitoring)
                'Hlt1MBNoBias',  # ==> disk buffer only (monitoring)
                'Hlt1NoBiasEmptyEmpty',  # ==> disk buffer only (monitoring)
                'Hlt1ErrorEvent',  # ==> Hlt2Transparent
                ]

        return lines

    def Streams(self):
        return {
                # None means use default
                'FULL': ("HLT_PASS_RE('^Hlt2BB.*Decision$') | HLT_PASS_RE('^Hlt2MB.*Decision$') | HLT_PASS('Hlt2SingleTrackDecision') "
                         "| HLT_PASS_RE('^Hlt2NoBiasNonBeamBeamDecision$')"),
                #'FULL': "|".join(["Hlt2BB"+line+"Decision" for line in ["PassThrough", "HighMult", "SMOGD02KPi", "LongTrack"]])
                'SMOGPHYS': ("HLT_PASS('Hlt2SMOGPassThroughDecision') | HLT_PASS_RE('^Hlt2BE.*Decision$') "  #BELongTrack, BEHighMult
                             "| HLT_PASS_RE('^Hlt2NoBiasNonBeamBeamDecision$')"),
                'BEAMGAS': "HLT_PASS_RE('Hlt2BeamGasDecision|Hlt2FullLumi.*Decision')",
                'LUMI' : None,
                'HLT1NOBIAS' : None,
                'VELOCLOSING': None,
                }

    def StreamsWithBanks(self):
        return [
            (["FULL"], 'KILL', []),
            (["SMOGPHYS"], 'KILL', []),
            (["BEAMGAS"], 'KILL', []),
            # LUMI is handled separately
            ]

    def StreamsWithLumi(self):
        return ['FULL', 'SMOGPHYS', 'BEAMGAS']

    def LumiPredicates(self):
        return {
            "LUMI": "HLT_PASS('Hlt2LumiDecision')",  # default
            "BEAMGAS": "HLT_PASS('Hlt2SelbanksLumiDecision')",
            }

    def NanoBanks(self):
        default = ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']
        return {
            'LUMI': default,  # default for all streams
            'BEAMGAS': default + [
                'HC', 'L0Calo', 'L0DU', 'L0Muon', 'L0PU',
                'PrsPacked', 'PrsPackedError', 'Velo', 'HltDecReports'],
            }
