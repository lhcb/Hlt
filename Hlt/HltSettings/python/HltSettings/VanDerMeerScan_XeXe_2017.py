from XenonXenon_2017 import XenonXenon_2017
from Utilities.Utilities import update_thresholds


class VanDerMeerScan_XeXe_2017(XenonXenon_2017):
    def HltType(self):
        self.verifyType(VanDerMeerScan_XeXe_2017)
        return 'VanDerMeerScan_XeXe_2017'

    def NanoBanks(self):
        nanobanks = {'LUMI': ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']}
        nanobanks['BEAMGAS'] = nanobanks['LUMI'] + ['Velo', 'L0DU', 'HltDecReports', 'HC']
        return nanobanks

    def StreamsWithLumi(self):
        return ['BEAMGAS']
