from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds
from Physics_pp_2018 import Physics_pp_2018
from Configurables import Hlt2Conf

DEFAULT_FILTER = "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB).*Decision')"


class Physics_pp_Ramp2018(Physics_pp_2018):
    """Settings for pp physics in the start of 2018."""

    def __init__(self):
        Hlt2Conf(_enabled=False).DefaultHlt1Filter = ("HLT_PASS_RE("
            "'Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB)(?!CharmXSec).*Decision')")

    def HltType(self):
        self.verifyType(Physics_pp_Ramp2018)
        return 'Physics_pp_Ramp2018'

    def Thresholds(self):
        """Returns a dictionary of cuts"""
        from Hlt1Lines.Hlt1TrackLines import Hlt1TrackLinesConf
        from Hlt2Lines.CharmHad.Lines import CharmHadLines
        from Hlt1Lines.Hlt1MBLines import Hlt1MBLinesConf

        thresholds = super(Physics_pp_Ramp2018, self).Thresholds()
        new_thresholds = {
            Hlt1MBLinesConf: {
                'Prescale' : {
                    'Hlt1MBNoBias': 0.002,  # 200 kHz -> 400 Hz (nominal)
                },
            },
            Hlt1TrackLinesConf: {
                'CharmXSec_Velo_NHits'  : 9,
                'CharmXSec_Velo_Qcut'   : 999,  #OFF
                'CharmXSec_TrNTHits'    : 0,  #OFF
                'CharmXSec_PT'          : 800.,
                'CharmXSec_P'           : 3000.,
                'CharmXSec_IPChi2'      : 10.0,
                'CharmXSec_TrChi2'      : 3.0,
                'CharmXSec_GEC'         : 'Loose',
                'ODINFilter'            : {
                    'CharmXSec': 'ODIN_PASS(LHCb.ODIN.NoBias)'
                    },
                'L0Channels'            : {
                    'CharmXSec': None,
                    },
            },
            CharmHadLines: {
                'Hlt1Reqs': {
                    'Dpm2KPiPi_XSecTurbo': DEFAULT_FILTER,
                    'Dpm2KKPi_XSecTurbo': DEFAULT_FILTER,
                    'Ds2KKPi_XSecTurbo': DEFAULT_FILTER,
                    'Ds2PiPiPi_XSecTurbo': DEFAULT_FILTER,
                    'Lc2KPPi_XSecTurbo': DEFAULT_FILTER,
                    'Lc2KPK_XSecTurbo': DEFAULT_FILTER,
                    'Lc2PiPPi_XSecTurbo': DEFAULT_FILTER,
                    'D02KPi_XSecTurbo': DEFAULT_FILTER,
                    'Dst_2D0Pi_D02KPi_XSecTurbo': DEFAULT_FILTER,
                    'Dst_2D0Pi0_D02KPi_XSecTurbo': DEFAULT_FILTER,
                    'Dst_2DsGamma_Ds2KKPi_XSecTurbo': DEFAULT_FILTER,
                    'Dst_2D0Pi_D02K3Pi_XSecTurbo': DEFAULT_FILTER,
                    'Dst_2D0Gamma_D02KPi_XSecTurbo': DEFAULT_FILTER,
                    'Sigmac_2LcPi_XSecTurbo': DEFAULT_FILTER,
                    'Xic02PKKPi_XSecTurbo': DEFAULT_FILTER,
                    },
            },
        }
        update_thresholds(thresholds, new_thresholds)

        return thresholds

    def ActiveHlt1Lines(self):
        lines = super(Physics_pp_Ramp2018, self).ActiveHlt1Lines()
        lines += ['Hlt1CharmXSec']
        return lines

    def ActiveHlt2Lines(self):
        lines = super(Physics_pp_Ramp2018, self).ActiveHlt2Lines()
        lines += [
            'Hlt2CharmHadDpm2KPiPi_XSecTurbo',
            'Hlt2CharmHadDpm2KKPi_XSecTurbo',
            'Hlt2CharmHadDs2KKPi_XSecTurbo',
            'Hlt2CharmHadDs2PiPiPi_XSecTurbo',
            'Hlt2CharmHadLc2KPPi_XSecTurbo',
            'Hlt2CharmHadLc2KPK_XSecTurbo',
            'Hlt2CharmHadLc2PiPPi_XSecTurbo',
            'Hlt2CharmHadD02KPi_XSecTurbo',
            'Hlt2CharmHadDst_2D0Pi_D02KPi_XSecTurbo',
            'Hlt2CharmHadDst_2D0Pi0_D02KPi_XSecTurbo',
            'Hlt2CharmHadDst_2DsGamma_Ds2KKPi_XSecTurbo',
            'Hlt2CharmHadDst_2D0Pi_D02K3Pi_XSecTurbo',
            'Hlt2CharmHadDst_2D0Gamma_D02KPi_XSecTurbo',
            'Hlt2CharmHadSigmac_2LcPi_XSecTurbo',
            'Hlt2CharmHadXic02PKKPi_XSecTurbo'
        ]
        return lines
