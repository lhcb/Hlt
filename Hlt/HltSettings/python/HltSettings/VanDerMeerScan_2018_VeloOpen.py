from VanDerMeerScan_2018 import VanDerMeerScan_2018
from Utilities.Utilities import update_thresholds


class VanDerMeerScan_2018_VeloOpen(VanDerMeerScan_2018):
    """Settings for VDM scans and BGI in 2018 with open VELO."""

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def HltType(self):
        self.verifyType(VanDerMeerScan_2018_VeloOpen)
        return 'VanDerMeerScan_2018_VeloOpen'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = super(VanDerMeerScan_2018_VeloOpen, self).ActiveHlt1Lines()
        lines += [
            'Hlt1BeamGasBeam2L0',
            'Hlt1BeamGasNoBeamL0',
        ]
        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = super(VanDerMeerScan_2018_VeloOpen, self).Thresholds()

        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf
        update_thresholds(thresholds, {
            Hlt1BeamGasLinesConf: {
                'PVMinTracks': 4,  # 5 by default
                'VertexMinNTracks': 3,  # (> 3)  9 by default
                'ODIN': {
                    'BeamGasBeam1L0': '(ODIN_BXTYP == LHCb.ODIN.Beam1)',
                    'BeamGasBeam2L0': '(ODIN_BXTYP == LHCb.ODIN.Beam2)',
                    'BeamGasNoBeamL0': '(ODIN_BXTYP == LHCb.ODIN.NoBeam)',
                },
                'L0Filter': {
                    'BeamGasBeam1L0': "L0_CHANNEL('B1gas')",
                    'BeamGasBeam2L0': "L0_CHANNEL('B2gas')",
                    'BeamGasNoBeamL0': "(L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas'))",
                },
                'Prescale': {
                    'Hlt1BeamGasBeam1L0': 0.01,  # B1gas independent of VELO
                    'Hlt1BeamGasBeam2L0': 0.05,  # B2gas x4 less with open velo
                    'Hlt1BeamGasNoBeamL0': 1.,
                },
            },
        })

        return thresholds

    def Streams(self):
        streams = super(VanDerMeerScan_2018_VeloOpen, self).Streams()
        streams['EXPRESS'] = "HLT_PASS_RE('Hlt1(Velo.*|BeamGas.*VeloOpen|Lumi)Decision')"
        return streams
