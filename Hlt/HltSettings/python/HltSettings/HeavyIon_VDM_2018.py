from HeavyIon_2018 import HeavyIon_2018
from Utilities.Utilities import update_thresholds


class HeavyIon_VDM_2018(HeavyIon_2018):
    """Settings for VDM scans and BGI in 2018 heavy ion run."""

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def HltType(self):
        self.verifyType(HeavyIon_VDM_2018)
        return 'HeavyIon_VDM_2018'

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = super(HeavyIon_VDM_2018, self).Thresholds()

        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf
        from Hlt1Lines.Hlt1IFTLines import Hlt1IFTLinesConf
        from Hlt2Lines.Technical.Lines import TechnicalLines
        update_thresholds(thresholds, {
            Hlt1BeamGasLinesConf: {
                'Prescale': {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                },
            },
            Hlt1IFTLinesConf: {
                'Prescale': {'Hlt1BBVeryHighMult': 1.},
            },
            TechnicalLines: {
                'Prescale': {
                    'Hlt2FullLumi'    : 0.1,
                    'Hlt2SelbanksLumi': 1.0,
                },
            },
        })

        return thresholds

    def Streams(self):
        streams = super(HeavyIon_VDM_2018, self).Streams()
        # Remove the LUMI stream for VDM
        return {k: v for k, v in streams.items() if k not in ['LUMI']}

    def StreamsWithLumi(self):
        # Remove (high-rate) lumi events from all but the BEAMGAS stream
        # other streams are not going to be used for physics anyway
        return ['BEAMGAS']
