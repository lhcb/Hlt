from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from TrackEffDiMuon.TrackEffDiMuon_pNe_2017 import TrackEffDiMuon_pNe_2017
from PID.PID_pNe_2017 import PID_pNe_2017
from HeavyIons.HeavyIons_pNe_2017 import HeavyIons_pNe_2017
from Utilities.Utilities import update_thresholds


class protonNeon_5tev2017(object):
    """Settings for proton-neon 2017 (pp@5TeV) data taking."""

    def __init__(self):
        # Extend velo tracking search range in z direction to [-2000,2000]
        from Configurables import HltRecoConf
        HltRecoConf().BeamGasMode = True
        HltRecoConf().VeloTrackingZMin = -2000. * mm
        HltRecoConf().VeloTrackingZMax = 2000. * mm

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1724'

    def HltType(self):
        self.verifyType(protonNeon_5tev2017)
        return 'protonNeon_5tev2017'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = [
            # Technical lines
            'Hlt1Lumi',  # => Hlt2Lumi
            'Hlt1VeloClosingMicroBias',  # disk buffer only
            'Hlt1VeloClosingPV',  # disk buffer only
            # BE+EB lines => SMOGPHYS stream
            # - trivial lines => Hlt2SMOGPassThrough
            'Hlt1BENoBias',
            'Hlt1EBNoBias',
            'Hlt1BEMicroBiasVelo',
            'Hlt1BEMicroBiasLowMultVelo',
            'Hlt1BEMicroBiasLowMultVeloNoBias',
            # - muon lines => Hlt2SMOGDiMuon
            'Hlt1SMOGDiMuonHighMass',
            # - SMOG lines => Hlt2SMOG*
            'Hlt1SMOGKPi',
            'Hlt1SMOGKPiPi',
            'Hlt1SMOGKKPi',
            'Hlt1SMOGpKPi',
            'Hlt1SMOGppbar',
            'Hlt1SMOGGeneric',
            'Hlt1SMOGSingleTrack',
            # BB lines => FULL stream
            # - trivial lines => Hlt2BBSMOGPassThrough
            # 'Hlt1BBNoBias',
            'Hlt1BBMicroBiasVelo',
            # 'Hlt1BBHighMult',
            # - muon lines => ???
            # 'Hlt1DiMuonHighMass',  # not accepted by any stream here!
            # - SMOG lines => Hlt2BBSMOG*
            'Hlt1BBSMOGKPi',
            'Hlt1BBSMOGKPiPi',
            'Hlt1BBSMOGKKPi',
            'Hlt1BBSMOGpKPi',
            'Hlt1BBSMOGppbar',
            'Hlt1BBSMOGGeneric',
            'Hlt1BBSMOGSingleTrack',
            # Calibration lines for alignment, disk buffer only
            'Hlt1CalibHighPTLowMultTrks',
            'Hlt1CalibRICHMirrorRICH1',
            'Hlt1CalibRICHMirrorRICH2',
            'Hlt1CalibTrackingKPiDetached',
            'Hlt1CalibMuonAlignJpsi',
        ]
        return lines

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = [
            'Hlt2Lumi',
            # SMOGPHYS stream  #TODO do we need these and in which stream?
            'Hlt2BBSMOGPassThrough',
            'Hlt2BBSMOGD02KPi',
            'Hlt2BBSMOGDpm2KPiPi',
            'Hlt2BBSMOGDs2KKPi',
            'Hlt2BBSMOGLc2KPPi',
            'Hlt2BBSMOGEtac2PpPm',
            'Hlt2BBSMOGB2PiMu',
            # SMOGPHYS stream
            'Hlt2SMOGD02KPi',
            'Hlt2SMOGDpm2KPiPi',
            'Hlt2SMOGDs2KKPi',
            'Hlt2SMOGLc2KPPi',
            'Hlt2SMOGEtac2PpPm',
            'Hlt2SMOGB2PiMu',
        ]

        # Passthrough lines Hlt2SMOGDiMuon, Hlt2SMOGPassThrough
        lines.extend(HeavyIons_pNe_2017().ActiveHlt2Lines())

        # # TrackEffDiMuon Calib lines
        # lines.extend(TrackEffDiMuon_pNe_2017().ActiveHlt2Lines())
        # # PID Calib lines
        # lines.extend(PID_pNe_2017().ActiveHlt2Lines())

        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1LumiLines import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1IFTLines import Hlt1IFTLinesConf
        from Hlt1Lines.Hlt1MuonLines import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1SMOGLines import Hlt1SMOGLinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf
        from Hlt1Lines.Hlt1CommissioningLines import Hlt1CommissioningLinesConf

        thresholds = {
            Hlt1LumiLinesConf: {
                # Complicated way to downscale only BE lumi events! :crossed_fingers:
                'ODIN': ("(ODIN_PASS(LHCb.ODIN.Lumi) & (ODIN_BXTYP != LHCb.ODIN.Beam1)) | "
                         "scale(ODIN_PASS(LHCb.ODIN.Lumi) & (ODIN_BXTYP == LHCb.ODIN.Beam1), SKIP(100))"),
                'Prescale': {'Hlt1Lumi': 1.},
                'Postscale': {'Hlt1Lumi': 1.},
            },
            Hlt1CommissioningLinesConf: {
                'Prescale': {
                    'Hlt1VeloClosingMicroBias': 1,
                    'Hlt1VeloClosingPV': 0.0005,
                },
                'Postscale': {
                    'Hlt1VeloClosingMicroBias': 'RATE(500)',
                    'Hlt1VeloClosingPV': 'RATE(500)',
                },
                'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                'ODIN': {
                    'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'L0': {
                    'VeloClosingPV': "scale(L0_DECISION(LHCb.L0DUDecision.Any), RATE(10000))",
                },
                'VeloClosingPV': {
                    'ZMin': -150 * mm,
                    'ZMax': 150 * mm,
                    'MinBackwardTracks': 1,
                    'MinForwardTracks': 1,
                },
            },
            Hlt1IFTLinesConf: {
                'ODIN': {
                    'BENoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                    'EBNoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                    'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                    'BEMicroBiasVelo': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                    'BBMicroBiasVelo': 'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
                    'BEMicroBiasLowMultVeloNoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                },
                'L0': {
                    'BEMicroBiasLowMultVelo': "L0_CHANNEL('SPDBElowmult')", # | scale(L0_ALL, GET('/Event/DAQ/ODIN') >> ODIN_PASS(LHCb.ODIN.Lumi))",
                },
                'GEC': {
                    'BEMicroBiasVelo': "Loose",
                    'BBMicroBiasVelo': "Loose",
                    'BEMicroBiasLowMultVelo': "Loose",
                    'BEMicroBiasLowMultVeloNoBias': "Loose",
                },
                'MaxVeloTracks': {
                    'BEMicroBiasLowMultVelo': 7.5,
                    'BEMicroBiasLowMultVeloNoBias': 7.5,
                },
                'MinVeloTracks': {
                    'BBMicroBiasVelo': 1,
                    'BEMicroBiasVelo': 1,
                    'BEMicroBiasLowMultVelo': 1,
                    'BEMicroBiasLowMultVeloNoBias': 1,
                },
                'BEMicroBiasLowMultVelo': {
                    'MinEta': 3.5,
                    'MaxEta': 7.0,
                },
                'BEMicroBiasLowMultVeloNoBias': {
                    'MinEta': 3.5,
                    'MaxEta': 7.0,
                },
                'Prescale': {
                    # NOTE: The prescales below expect x:1:1:y kHz lumi rate,
                    # which is what we should use during BGI/VDM
                    'Hlt1BBNoBias':               1,  # Such events go to NOBIAS, see pp settings
                    'Hlt1BENoBias':               0.02,  # 0.02 x 50 kHz = 1000 Hz
                    'Hlt1EBNoBias':               0.125,  # 0.125 x 400 Hz = 50 Hz
                    'Hlt1BBMicroBiasVelo':        0.00001,  # (250 kHz * (1-exp(-1.5)) + 200 kHz) * 0.00001 = 40 Hz
                    'Hlt1BEMicroBiasVelo':        0.25,
                    'Hlt1BEMicroBiasLowMultVelo': 1,
                    'Hlt1BEMicroBiasLowMultVeloNoBias': 1,
                },
            },
            Hlt1MuonLinesConf: {
                'L0Channels': {
                    # 'DiMuonHighMass':     ('Muon',),
                    'SMOGDiMuonHighMass': "L0_CHANNEL('MuonBE') | L0_ODINCUT(ODIN_PASS(LHCb.ODIN.Lumi))",
                    'CalibMuonAlignJpsi': ('Muon',),
                },
                'ODINFilter': {
                    # 'DiMuonHighMass':     'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                    # TODO should the DiMuonHighMass trigger only on NoBias like Hlt1BBSMOG*?
                    'SMOGDiMuonHighMass': 'ODIN_BXTYP == LHCb.ODIN.Beam1',
                    'CalibMuonAlignJpsi': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                # 'DiMuonHighMass_VxDOCA'    :  0.2,
                # 'DiMuonHighMass_VxChi2'    :   25,
                # 'DiMuonHighMass_P'         : 3000,
                # 'DiMuonHighMass_PT'        :  500,
                # 'DiMuonHighMass_TrChi2'    :    3,
                # 'DiMuonHighMass_M'         : 2500,  # 2700->2500 MeV,  #...
                # 'DiMuonHighMass_GEC'       : 'Loose',
                'SMOGDiMuonHighMass_VxDOCA'    :  0.2,
                'SMOGDiMuonHighMass_VxChi2'    :   25,
                'SMOGDiMuonHighMass_P'         : 3000,  # 6000 in pp
                'SMOGDiMuonHighMass_PT'        :  500,  # TODO 500 -> 300 as in pp?
                'SMOGDiMuonHighMass_TrChi2'    :    3,  # 4 in pp
                'SMOGDiMuonHighMass_M'         : 2500,  # 2700->2500 MeV,  #...
                'SMOGDiMuonHighMass_GEC'       : 'Loose',
                'SMOGDiMuonHighMass_CFTracking': False,
                'CalibMuonAlignJpsi_ParticlePT'             : 800,     # MeV
                'CalibMuonAlignJpsi_ParticleP'              : 6000,    # MeV
                'CalibMuonAlignJpsi_TrackCHI2DOF'           : 2,       # dimensionless
                'CalibMuonAlignJpsi_CombMaxDaughtPT'        : 800,     # MeV
                'CalibMuonAlignJpsi_CombAPT'                : 1500,    # MeV
                'CalibMuonAlignJpsi_CombDOCA'               : 0.2,     # mm
                'CalibMuonAlignJpsi_CombVCHI2DOF'           : 10,     # dimensionless
                'CalibMuonAlignJpsi_CombVCHI2DOFLoose'      : 10,      # dimensionless
                'CalibMuonAlignJpsi_CombDIRA'               : 0.9,     # dimensionless
                'CalibMuonAlignJpsi_CombTAU'                : 0.,     # ps
                'CalibMuonAlignJpsi_JpsiMassWinLoose'       : 150,     # MeV
                'CalibMuonAlignJpsi_JpsiMassWin'            : 100,     # MeV
            },
            Hlt1SMOGLinesConf: {
                'ParticlePT':          400,  # MeV  # 500 -> 400 #...
                'ParticleP':           3000,  # MeV
                'TrackCHI2DOF':        4,
                'CombMaxDaughtPT':     400,  # MeV  500 -> 400 #...
                'CombDOCA':            1.0,  # mm
                'CombVCHI2DOF':        25,
                'MassWinLoose':        250,  # MeV
                'MassWinEtac':         300,  # MeV
                'MassWin':             150,  # MeV
                'GenericMassMinLoose': 0,  # MeV
                'GenericMassMin':      0,  # MeV
                'GenericMaxDaughtPT':  800,  # MeV
                'SingleTrackPT':       1000,  # MeV
                'SingleTrackTrGP':     0.2,
                'GEC':                 'Loose',
                'L0':    '',
                'L0_BB': '',
                # TODO should we explicitly trigger on channels or L0_DECISION_PHYSICS?
                #      With no L0 filter, you get mix of L0 triggered, NoBias, Lumi...)
                'ODIN':    'ODIN_BXTYP == LHCb.ODIN.Beam1',
                'ODIN_BB': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.NoBias)',
                'Prescale': {
                    'Hlt1BBSMOGKPi': 0.0002,
                    'Hlt1BBSMOGKPiPi': 0.0002,
                    'Hlt1BBSMOGKKPi': 0.0002,
                    'Hlt1BBSMOGpKPi': 0.0002,
                    'Hlt1BBSMOGppbar': 0.0002,
                    'Hlt1BBSMOGGeneric': 0.0001,
                    'Hlt1BBSMOGSingleTrack': 0.0001,
                },
            },
            # TODO The Hlt1CalibRICHMirrorLines and Hlt1CalibTrackingLinesConf trigger on L0_DECISION_PHYSICS,
            #      which in the SMOG L0/ODIN configuration means bb+be+eb. Is this fine?
            Hlt1CalibRICHMirrorLinesConf: {
                'Prescale': {
                    'Hlt1CalibHighPTLowMultTrks': 0.0001,
                    'Hlt1CalibRICHMirrorRICH1':   0.281,
                    'Hlt1CalibRICHMirrorRICH2':   1.0,
                },
                # TODO are the Hlt1CalibRICHMirrorLines prescales ok for the SMOG run?
                'DoTiming'   : False,
                'R2L_PT'     : 500.   * MeV,
                'R2L_P'      : 40000. * MeV,
                'R2L_MinETA' : 2.65,
                'R2L_MaxETA' : 2.8,
                'R2L_Phis'   : [(-2.59, -2.49), (-0.65, -0.55), (0.55, 0.65), (2.49, 2.59)],
                'R2L_TrChi2' : 2.,
                'R2L_MinTr'  : 0.5,
                'R2L_GEC'    : 'Loose',
                'R1L_PT'     : 500.   * MeV,
                'R1L_P'      : 20000. * MeV,
                'R1L_MinETA' : 1.6,
                'R1L_MaxETA' : 2.04,
                'R1L_Phis'   : [ ( -2.65, -2.30 ), ( -0.80, -0.50 ), ( 0.50, 0.80 ), ( 2.30, 2.65 ) ],
                'R1L_TrChi2' : 2.,
                'R1L_MinTr'  : 0.5,
                'R1L_GEC'    : 'Loose',
                'LM_PT'      : 500.   * MeV,
                'LM_P'       : 1000.  * MeV,
                'LM_TrChi2'  : 2.,
                'LM_MinTr'   : 1,
                'LM_MaxTr'   : 40,
                'LM_GEC'     : 'Loose'
            },
            Hlt1CalibTrackingLinesConf: {
                'ParticlePT'             : 600,     # MeV
                'ParticleP'              : 4000,    # MeV
                'TrackCHI2DOF'           : 2,       # dimensionless
                'CombMaxDaughtPT'        : 900,     # MeV 900
                'CombAPT'                : 1800,    # MeV 1200
                'CombDOCA'               : 0.1,     # mm
                'CombVCHI2DOF'           : 10,      # dimensionless
                'CombVCHI2DOFLoose'      : 15,      # dimensionless
                'CombDIRA'               : 0.99,    # dimensionless
                'CombTAU'                : 0.25,    # ps
                'D0MassWinLoose'         : 100,     # MeV
                'D0MassWin'              : 60,      # MeV
                'B0MassWinLoose'         : 200,     # MeV
                'B0MassWin'              : 150,     # MeV
                'D0DetachedDaughtsIPCHI2': 9,       # dimensionless
                'D0DetachedIPCHI2'       : 9,       # dimensionless
                'BsPhiGammaMassMinLoose' : 3350,    # MeV
                'BsPhiGammaMassMaxLoose' : 6900,    # MeV
                'BsPhiGammaMassMin'      : 3850,    # MeV
                'BsPhiGammaMassMax'      : 6400,    # MeV
                'PhiMassWinLoose'        : 50,      # MeV
                'PhiMassWin'             : 30,      # MeV
                'PhiMassWinTight'        : 20,      # MeV
                'PhiPT'                  : 1800,    # MeV
                'PhiPTLoose'             : 800,     # MeV
                'PhiSumPT'               : 3000,    # MeV
                'PhiIPCHI2'              : 16,      # dimensionless
                'B0SUMPT'                : 4000,    # MeV
                'B0PT'                   : 1000,    # MeV
                'GAMMA_PT_MIN'           : 2000,    # MeV
                'Velo_Qcut'              : 999,     # OFF
                'TrNTHits'               : 0,       # OFF
                'ValidateTT'             : False,
            }
        }

        from Hlt2Lines.SMOG.Lines import SMOGLines
        thresholds.update({
            SMOGLines: {
                'Prescale': {},
                'Postscale': {},
                'Hlt1Req': {
                    'SMOGDpm2KPiPi': "HLT_PASS_RE('Hlt1SMOG.*Decision')",
                    'SMOGDs2KKPi': "HLT_PASS_RE('Hlt1SMOG.*Decision')",
                    'SMOGLc2KPPi': "HLT_PASS_RE('Hlt1SMOG.*Decision')",
                    'SMOGD02KPi': "HLT_PASS_RE('Hlt1SMOG.*Decision')",
                    'SMOGEtac2PpPm': "HLT_PASS_RE('Hlt1SMOG.*Decision')",
                    'SMOGB2PiMu': "HLT_PASS_RE('Hlt1SMOG.*Decision')",
                    'BBSMOGDpm2KPiPi': "HLT_PASS_RE('Hlt1BBSMOG.*Decision')",
                    'BBSMOGDs2KKPi': "HLT_PASS_RE('Hlt1BBSMOG.*Decision')",
                    'BBSMOGLc2KPPi': "HLT_PASS_RE('Hlt1BBSMOG.*Decision')",
                    'BBSMOGD02KPi': "HLT_PASS_RE('Hlt1BBSMOG.*Decision')",
                    'BBSMOGEtac2PpPm': "HLT_PASS_RE('Hlt1BBSMOG.*Decision')",
                    'BBSMOGB2PiMu': "HLT_PASS_RE('Hlt1BBSMOG.*Decision')",
                },
                'D02HH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Pair_AMINDOCA_MAX'        : 0.10 * mm,
                    'Trk_Max_APT_MIN'          : 400.0 * MeV,  # 500->400
                    'D0_VCHI2PDOF_MAX'         : 10.0,       # neuter
                    'Comb_AM_MIN'              : 1775.0 * MeV,
                    'Comb_AM_MAX'              : 1955.0 * MeV,
                    'Trk_ALL_PT_MIN'           : 250.0 * MeV,
                    'Trk_ALL_P_MIN'            : 2.0  * GeV,
                    'Mass_M_MIN'               : 1784.0 * MeV,
                    'Mass_M_MAX'               : 1944.0 * MeV,
                },
                'Dpm2HHH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Trk_ALL_PT_MIN'           : 200.0 * MeV,
                    'Trk_2OF3_PT_MIN'          : 400.0 * MeV,
                    'Trk_1OF3_PT_MIN'          : 1000.0 * MeV,
                    'VCHI2PDOF_MAX'            : 16.0,
                    'ASUMPT_MIN'               : 0 * MeV,
                    'AM_MIN'                   : 1779 * MeV,
                    'AM_MAX'                   : 1959 * MeV,
                    'Mass_M_MIN'               : 1789.0 * MeV,
                    'Mass_M_MAX'               : 1949.0 * MeV,
                    },
                'Ds2HHH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Trk_ALL_PT_MIN'           : 200.0 * MeV,
                    'Trk_2OF3_PT_MIN'          : 400.0 * MeV,
                    'Trk_1OF3_PT_MIN'          : 1000.0 * MeV,
                    'VCHI2PDOF_MAX'            : 16.0,
                    'ASUMPT_MIN'               : 0 * MeV,
                    'AM_MIN'                   : 1879 * MeV,
                    'AM_MAX'                   : 2059 * MeV,
                    'Mass_M_MIN'               : 1889.0 * MeV,
                    'Mass_M_MAX'               : 2049.0 * MeV,
                },
                'Lc2HHH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Trk_ALL_PT_MIN'           : 200.0 * MeV,
                    'Trk_2OF3_PT_MIN'          : 400.0 * MeV,
                    'Trk_1OF3_PT_MIN'          : 1000.0 * MeV,
                    'VCHI2PDOF_MAX'            : 16.0,
                    'ASUMPT_MIN'               : 0 * MeV,
                    'AM_MIN'                   : 2201 * MeV,
                    'AM_MAX'                   : 2553 * MeV,
                    'Mass_M_MIN'               : 2211.0 * MeV,
                    'Mass_M_MAX'               : 2543.0 * MeV,
                },
                'Etac2PpPm'   : {
                  'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                  'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                  'Pair_AMINDOCA_MAX'        : 0.10 * mm,
                  'Trk_Max_APT_MIN'          : 400.0 * MeV,
                  'Mother_VCHI2PDOF_MAX'     : 10.0,  
                  'Comb_AM_MIN'              : 2673.6 * MeV,
                  'Comb_AM_MAX'              : 3293.6 * MeV,
                  'Trk_ALL_PT_MIN'           : 250.0 * MeV,
                  'Trk_ALL_P_MIN'            : 5.0  * GeV,
                  'Mass_M_MIN'               :  2683.6 * MeV,
                  'Mass_M_MAX'               :  3283.6 * MeV,
                  },
                'B2PiMu': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'AM_MIN'                   : 0 * MeV,
                    'AM_MAX'                   : 1000.0 * GeV,
                    'ASUMPT_MIN'               : 4.0 * GeV,
                    'VCHI2PDOF_MAX'            : 16.0,
                    'Mass_M_MIN'               : 0 * MeV,
                    'Mass_M_MAX'               : 1000.0 * GeV
                },
            }
        })

        # Hlt2 SMOG pass through lines
        update_thresholds(thresholds, HeavyIons_pNe_2017().Thresholds())
        # TrackEffDiMuon Calib thresholds
        update_thresholds(thresholds, TrackEffDiMuon_pNe_2017().Thresholds())
        # PID Calib thresholds
        update_thresholds(thresholds, PID_pNe_2017().Thresholds())

        from Hlt2Lines.CaloPID.Lines import CaloPIDLines
        thresholds.update({
            CaloPIDLines: {
                'Hlt1Req': {
                    'default': "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB).*Decision')",
                },
            },
        })

        return thresholds

    def Streams(self):
        return {
            # Deliberately turn off the LUMI stream to not double the large
            # rate of lumi during VDM (45 kHz) to storage
            'SMOGPHYS': ' | '.join([
                "HLT_PASS_RE('Hlt2(BB)?SMOG.*Decision')",
                "HLT_PASS_RE('Hlt2TrackEffDiMuonMuon.*Decision')",
                # TODO is really only Hlt2TrackEffDiMuonMuon* needed? (note the MuonMuon)
                "HLT_PASS_RE('Hlt2PID.*Decision')",
                # Hlt2Lumi is added by HltOutput
            ]),
            # TODO note that there is one set of Hlt2TrackEffDiMuon and Hlt2PID
            #      lines, which means that some bb events will go to the SMOGPHYS
            #      and some be/eb into FULL unless we change something.
            'VELOCLOSING': None,
            'HLT1NOBIAS': None,
        }

    def StreamsWithBanks(self):
        return [(["FULL"], 'KILL', []),
                (["SMOGPHYS"], 'KILL', [])]
