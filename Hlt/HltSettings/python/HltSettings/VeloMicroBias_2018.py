from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds


class VeloMicroBias_2018(object):
    """Settings for micro bias data taking in 2018."""

    def __init__(self):
        # No TURBO => no aferburner
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

        # Remove implicit GECs in the decoding
        from HltLine.HltDecodeRaw import DecodeVELO
        DecodeVELO.members()[0].MaxVeloClusters = 99999
        from DAQSys.Decoders import DecoderDB
        DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Properties["MaxVeloClusters"] = 99999
        from HltTracking.HltRecoConf import HltRecoConf
        HltRecoConf().Forward_MaxOTHits = 99999

        # Set the lines for the data quality
        from HltConf.HltOutput import HltOutputConf
        HltOutputConf().Hlt2LinesForDQ = ['Hlt2PassThrough']

    def verifyType(self, ref):
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1715'

    def HltType(self):
        self.verifyType(VeloMicroBias_2018)
        return 'VeloMicroBias_2018'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = [
            # Technical lines
            'Hlt1Lumi',  # => Hlt2Lumi
            'Hlt1VeloClosingMicroBias',  # disk buffer only
            'Hlt1VeloClosingPV',  # disk buffer only
            'Hlt1NoBiasEmptyEmpty',  # disk buffer only
            'Hlt1MBNoBias',  # disk buffer only
            # trivial lines => Hlt2PassThrough
            'Hlt1BBNoBias',
            'Hlt1BBMicroBiasVelo',
            'Hlt1BBHighMult',
        ]
        return lines

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = [
            'Hlt2Lumi',
            'Hlt2PassThrough',
        ]
        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1LumiLines import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1IFTLines import Hlt1IFTLinesConf
        from Hlt1Lines.Hlt1MBLines import Hlt1MBLinesConf
        from Hlt1Lines.Hlt1LowMultLines import Hlt1LowMultLinesConf
        from Hlt1Lines.Hlt1MuonLines import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf
        from Hlt1Lines.Hlt1CommissioningLines import Hlt1CommissioningLinesConf

        thresholds = {
            Hlt1IFTLinesConf: {
                'Prescale': {
                    'Hlt1BBNoBias':        0.01,  # NoBias = ~1 MHz
                    'Hlt1BBHighMult':      1,
                    'Hlt1BBMicroBiasVelo': 1,
                },
                'ODIN': {
                    'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
                    'BBHighMult': 'ODIN_PASS(LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
                    'BBMicroBiasVelo': 'ODIN_PASS(LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
                },
                'L0': {
                },
                'GEC': {
                    'BBHighMult': "HeavyIons",  # MaxVeloHits = 8000
                    'BBMicroBiasVelo': "HeavyIons",  # MaxVeloHits = 8000
                },
                'MinVeloTracks': {
                    'BBMicroBiasVelo': 1,
                },
            },
            Hlt1LumiLinesConf: {
                'Prescale': {'Hlt1Lumi': 1.},
                'Postscale': {'Hlt1Lumi': 1.},
            },
            Hlt1MBLinesConf: {
                'Prescale': {'Hlt1MBNoBias': 0.001},
            },
            Hlt1LowMultLinesConf: {
                'Prescale': {'Hlt1NoBiasEmptyEmpty': 0.5},
            },
            Hlt1CommissioningLinesConf: {
                'Prescale': {
                    'Hlt1VeloClosingMicroBias': 1,
                    'Hlt1VeloClosingPV': 1,
                },
                'Postscale': {
                    'Hlt1VeloClosingMicroBias': 'RATE(500)',
                    'Hlt1VeloClosingPV': 'RATE(500)',
                },
                'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                'ODIN': {
                    'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'L0': {
                    'VeloClosingPV': "L0_DECISION(LHCb.L0DUDecision.Any)",
                },
                'VeloClosingPV': {
                    'ZMin': -150 * mm,
                    'ZMax': 150 * mm,
                    'MinBackwardTracks': 1,
                    'MinForwardTracks': 1,
                },
            },
        }

        # Hlt2 pass through lines
        from Hlt2Lines.Technical.Lines import TechnicalLines
        thresholds.update({
            TechnicalLines: {
                'PassThrough': {
                    'VoidFilter': '',
                    'HLT1': "HLT_PASS_RE('Hlt1BB.*Decision')",
                },
                'Prescale': {
                    'Hlt2PassThrough': 1.,
                },
            }
        })

        return thresholds

    def Streams(self):
        return {
            'FULL': None,
            'LUMI': None,
            'EXPRESS': None,
            'VELOCLOSING': None,
            'HLT1NOBIAS': None,
        }
