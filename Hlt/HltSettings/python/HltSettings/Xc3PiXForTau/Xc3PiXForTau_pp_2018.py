from GaudiKernel.SystemOfUnits import GeV, mm, MeV, picosecond

class Xc3PiXForTau_pp_2018(object) :
    """
    Threshold settings for Hlt2 Xc3PiXForTau lines: Xc3PiXForTau_pp_2018

    @author A. Romero Vidal < antonio.romero@usc.es >
    @date 2018-03-10
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            'D02Kpi'
           ,'D02KpiWS'
           ,'D02KpiNonPhys'
           ,'D02K3pi'
           ,'D02K3piWS'
           ,'D02K3piNonPhys'
           ,'Dp2Kpipi'
           ,'Dp2KpipiWS'
           ,'Dp2KpipiNonPhys'
           ,'Ds2kkpi'
           ,'Ds2kkpiWS'
           ,'Ds2kkpiNonPhys'
           ,'Lc2pkpi'
           ,'Lc2pkpiWS'
           ,'Lc2pkpiNonPhys'
           ,'Jpsi2mumu'
           ,'Jpsi2mumuNonPhys'
           ]

        return ['Hlt2Xc3PiXForTau' + l for l in lines]


    def Thresholds(self) :

        d = {}

        from Hlt2Lines.Xc3PiXForTau.Lines import Xc3PiXForTauLines

        d.update (
            {
                Xc3PiXForTauLines: {

                 'XcDaughter'  : {
                              'D_Trk_ALL_PT_MIN'              : 250.0 * MeV
                            , 'D_Trk_ALL_P_MIN'               :   0.0 * GeV
                            , 'D_Trk_ALL_MIPCHI2DV_MIN'       :   9.0
                            , 'D02K3pi_Trk_ALL_MIPCHI2DV_MIN' :  15.0
                            , 'D_K_PIDK'                      :    -3.
                            , 'D_Pi_PROBNNpi'                 :   0.1
                            , 'D02K3pi_K_PIDK'                :    -3.
                            , 'D02K3pi_Pi_PROBNNpi'           :   0.4
                            , 'D_P_PIDp'                      :     5.
                            , 'D_Mu_PIDmu'                    : -5000.
                            },
                 'TauDaughter'  : {
                              'Tau_Trk_ALL_PT_MIN'          :  250.0 * MeV
                            , 'Tau_Trk_ALL_MIPCHI2DV_MIN'   :   15.0
                            , 'Tau_Pi_PROBNNpi'             :    0.6
                            , 'Tau_Trk_ALL_P_MIN'           :    2.0 * GeV
                            },
                 'Xc' : {
                              'D_AMAXDOCA_MAX'           : 0.20 * mm
                            , 'D_Trk_ALL_MAXPT_MIN'      : 1500.0 * MeV
                            , 'Xc_BPVVDCHI2_MIN'         : 25.0
                            , 'Xc_IPCHI2_MIN'            : 9.0
                            , 'Xc_BPVDIRA_MIN'           : 0.995
                            , 'Xc_VCHI2PDOF_MAX'         : 10.0
                            , 'Xc_PT_MIN'                : 1000.0 * MeV
                            , 'D_AM_MIN'                 : 1804.0 * MeV
                            , 'D_AM_MAX'                 : 1929.0 * MeV
                            , 'Ds_AM_MIN'                : 1909.0 * MeV
                            , 'Ds_AM_MAX'                : 2029.0 * MeV
                            , 'Lc_AM_MIN'                : 2226.0 * MeV
                            , 'Lc_AM_MAX'                : 2346.0 * MeV
                            , 'Jpsi_AM_MIN'              : 3000.0 * MeV
                            , 'Jpsi_AM_MAX'              : 3200.0 * MeV
                            },
                 'Tau' : {
                              'Tau_AMAXDOCA_MAX'         : 0.20 * mm
                            , 'Tau_FDT_MIN'              : 0.2 * mm
                            , 'Tau_BPVDIRA_MIN'          : 0.99
                            , 'Tau_VCHI2PDOF_MAX'        : 5.
                            , 'Tau_PT_MIN'               : 1000.0 * MeV
                            , 'Tau_AM_MIN'               : 0. * MeV
                            , 'Tau_AM_MAX'               : 5000.0 * MeV
                            },
                 'B' : {
                              'XcTau_DOCA_MAX'      : 0.20 * mm
                            , 'XcTau_DIRA'          : 0.995
                            , 'XcTau_M_MIN'         :    0.*MeV
                            , 'XcTau_M_MAX'         : 8000.*MeV
                            }

                 } # Xc3PiXForTauLines {
               } # d.update ( {
            ) # d.update (

        return d
