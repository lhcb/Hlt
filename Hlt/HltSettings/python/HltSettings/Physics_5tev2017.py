from protonNeon_5tev2017 import protonNeon_5tev2017
from Physics_pp_5tev2017 import Physics_pp_5tev2017
from Utilities.Utilities import update_thresholds
from Configurables import HltOutputConf, Hlt2Conf

class Physics_5tev2017(object):
    """Settings for combined p-p and proton-neon physics in 2017."""

    def __init__(self):
        self.pp = Physics_pp_5tev2017()
        self.ft = protonNeon_5tev2017()
        # "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB).*Decision')"
        Hlt2Conf().DefaultHlt1Filter = ("HLT_PASS_RE("
            "'Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB)(?!SMOG)(?!BBSMOG)(?!BE)(?!EB)(?!BB).*Decision')")

    def verifyType(self, ref):
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1724'

    def HltType(self):
        self.verifyType(Physics_5tev2017)
        return 'Physics_5tev2017'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = self.pp.ActiveHlt1Lines() + self.ft.ActiveHlt1Lines()
        return list(set(lines))

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = self.pp.ActiveHlt2Lines() + self.ft.ActiveHlt2Lines()
        return list(set(lines))

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = {}
        update_thresholds(thresholds, self.pp.Thresholds())
        update_thresholds(thresholds, self.ft.Thresholds())
        return thresholds

    def Streams(self):
        streams = HltOutputConf().getProp('EnabledStreams').copy()
        streams['FULL'] = ("HLT_NONTURBOPASS_RE('Hlt2"
            "(?!LowMult)(?!NoBiasNonBeamBeam)(?!BeamGas)(?!Lumi)(?!SMOG)(?!BBSMOG)"
            ".*Decision')")
        streams['SMOGPHYS'] = "HLT_PASS_RE('Hlt2(BB)?SMOG.*Decision')"
        streams.pop('LOWMULT')
        return streams
