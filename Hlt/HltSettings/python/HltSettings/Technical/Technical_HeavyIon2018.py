from GaudiKernel.SystemOfUnits import GeV, mm, MeV

class Technical_HeavyIon2018(object) :
    """
    Threshold settings for Hlt2 Commissioning lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author Y. Zhang
    @date 2018-07-11
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            'Hlt2Lumi',
            'Hlt2ErrorEvent',
            'Hlt2Transparent',
            'Hlt2BeamGas',
            'Hlt2SelbanksLumi',
            'Hlt2FullLumi',
            'Hlt2FullLumiHighMult',
        ]

        return lines


    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        # keep pass through thresholds
        #we will only need (Probably) MBNoBias, Lumi. The PassThrough will be copies of (B|BE)PassThrough
        d = { }

        from Hlt2Lines.Technical.Lines     import TechnicalLines
        d.update({TechnicalLines :
                      {'Prescale'    : {'Hlt2PassThrough' : 1.,
                                        'Hlt2Forward'     : 0.00001,
                                        'Hlt2MBNoBias'    : 1.,
                                        'Hlt2DebugEvent'  : 0.000001,
                                        'Hlt2FullLumi'    : 0.05,
                                        'Hlt2FullLumiHighMult' : 1,
                                        'Hlt2SelbanksLumi': 0.25,
                                        },
                       'Postscale'   : {'Hlt2ErrorEvent'  : 'RATE(0.01)'},
                       # do not want debug events on lumi-exclusive Hlt1 events...
                       'DebugEvent'  : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')"},
                       'ErrorEvent'  : {'Priority' : 254,
                                        'VoidFilter' : '',
                                        'HLT1' : '',
                                        'HLT2' : "HLT_COUNT_ERRORBITS_RE('^Hlt2.*',0xffff) > 0"},
                       'PassThrough' : {'HLT1' : "HLT_PASS_RE('^Hlt1BB.*Decision$') | HLT_PASS('Hlt1DiMuonHighMassDecision') | HLT_PASS('Hlt1SingleMuonHighPTDecision') | HLT_PASS('Hlt1L0PhotonDecision')",
                                        'VoidFilter' : ''},
                       'MBNoBias'    : {'HLT1' : "HLT_PASS_RE('^Hlt1.*NoBiasDecision'$)",
                                        'VoidFilter' : ''},
                       'Transparent' : {'HLT1' : "HLT_PASS_RE('^Hlt1(ODIN.*|L0.*|NZS.*|Incident|ErrorEvent)Decision$')",
                                        'VoidFilter' : ''},
                       'BeamGas'     : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1BeamGas')" ,
                                        'VoidFilter' : ''},
                       'Lumi'        : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1Lumi')",
                                        'VoidFilter' : ''},
                       'FullLumi'    : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1Lumi')",
                                        'VoidFilter' : ''},
                       'FullLumiHighMult' : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1LumiHighMult')",
                                        'VoidFilter' : ''},
                       'SelbanksLumi' : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1Lumi')",
                                         'VoidFilter' : ''},
                       'KS0_DD'      : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')",
                                        'VoidFilter' : ''},
                       'KS0_LL'      : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')",
                                        'VoidFilter' : ''},
                       'Turbo'       : ['KS0_DD', 'KS0_LL']
                       }}
                 )
        return d
