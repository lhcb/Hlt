from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds
from Physics_pp_2018 import Physics_pp_2018


class Physics_pp_Tight2018(Physics_pp_2018):
    """Settings for pp physics in 2018 (tight version)."""

    def HltType(self):
        self.verifyType(Physics_pp_Tight2018)
        return 'Physics_pp_Tight2018'

    def Thresholds(self):
        """Returns a dictionary of cuts"""
        from Hlt1Lines.Hlt1MVALines import Hlt1MVALinesConf
        from Hlt1Lines.Hlt1ProtonLines import Hlt1ProtonLinesConf

        thresholds = super(Physics_pp_Tight2018, self).Thresholds()
        new_thresholds = {
            Hlt1MVALinesConf: {
                'Prescale': {
                    'Hlt1TrackMVA': 0.01,
                    'Hlt1TwoTrackMVA': 0.01
                },
            },
            Hlt1ProtonLinesConf: {
                'Prescale': {
                    'Hlt1DiProton': 0.5
                },
            },
        }
        update_thresholds(thresholds, new_thresholds)

        return thresholds
