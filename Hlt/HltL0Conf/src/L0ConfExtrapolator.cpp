// Include files 

#include <cmath>

//Event
#include "Event/StateParameters.h"

// local
#include "L0ConfExtrapolator.h"

#include "DetDesc/ValidDataObject.h"


//-----------------------------------------------------------------------------
// Implementation file for class : L0ConfExtrapolator
//
// 2008-01-18 : Johannes Albrecht
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( L0ConfExtrapolator )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
L0ConfExtrapolator::L0ConfExtrapolator( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface<IL0ConfExtrapolator>(this);

  //for getParabolaHypothesis
  declareProperty( "curvFactor" , m_curvFactor = 42. );
  
  declareProperty("fwdSigmaX2", m_fwdSigmaX2 = {{121.,144.,625.,225.,1225.}});
  declareProperty("fwdSigmaY2", m_fwdSigmaY2 = {{196.,289.,784.,400.,2025.}});
  
  //kick constants for ECal seeding
  declareProperty("ecalKick" , m_ecalKick = 1536443. );
  
  //kick constants for HCal (+ECal) seeding
  declareProperty("hCalEcalKick1" , m_HECalKick[0] = 11.5 );
  declareProperty("hCalEcalKick2" , m_HECalKick[1] = 1.585e6 );
  declareProperty("hCalKick1" , m_HCalKick[0] = 17. );
  declareProperty("hCalKick2" , m_HCalKick[1] = 1.712e6 );
}


StatusCode L0ConfExtrapolator::initialize() 
{
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()){
    return sc;
  }

  // subscribe to the updatemanagersvc with a dependency on the magnetic field svc
  m_magFieldSvc = service("MagneticFieldSvc", true);
  registerCondition( m_magFieldSvc.get(),&L0ConfExtrapolator::updateField) ;
  
  // initialize with the current conditions
  return runUpdate();
}

StatusCode L0ConfExtrapolator::updateField()
{
  if(msgLevel(MSG::DEBUG)) 
    debug()<<"magnetic field is: "<<m_magFieldSvc->signedRelativeCurrent()<<endmsg;

  m_BscaleFactor = m_magFieldSvc->signedRelativeCurrent();
  m_fieldOff=false;
  if( fabs(m_magFieldSvc->signedRelativeCurrent() ) < 0.1 ) {
    m_fieldOff=true;
    if(msgLevel(MSG::INFO)) {
      info()<<" magnetic field is below 10% of nominal field! \n Use options for no field!"<<endmsg;
      info()<<"Tool configured for no B field!"<<endmsg;
      info()<<"Position and slope is set correctly, covariance and momemtum _not_!"<<endmsg;
    }
  }
  return StatusCode::SUCCESS ;
}

//=============================================================================
void L0ConfExtrapolator::muon2T( const LHCb::Track& muonTrack,
                                 LHCb::State& stateAtT ) const
{
  //muon track has x,y,tx,ty information 
  //perform linear extrapolation to T3
  const LHCb::State& aState = muonTrack.firstState();
  double dz = aState.z() - zEndT3;
   
  stateAtT.setState(aState.x() - dz * aState.tx(),
                    aState.y() - dz * aState.ty(),
                    zEndT3,
                    aState.tx(), aState.ty(), aState.qOverP());
  stateAtT.setCovariance( aState.covariance() );
}

// this private method does most of the dirty work for hcal2T and ecal2T
void L0ConfExtrapolator::calo2T( const LHCb::State& aState, double xkick,
                                 LHCb::State& statePosAtT, LHCb::State& stateNegAtT ) const
{
  double x = aState.x(), y = aState.y(), z = aState.z();
  // x and y central position are on line connecting calo with origin
  double xT3 = x * zEndT3 / z, yT3 = y * zEndT3 / z;
  // assume neutral particle slopes, correct for x-kick in T below
  double tx = x / z, ty = y / z;
  double dz = zEndT3 - z;
  //in case of no (low) field, particle gets no kick
  if(m_fieldOff) xkick = 0 ;
  

  statePosAtT.setState(xT3 - xkick, yT3, zEndT3, 
                       tx - xkick / dz, ty, aState.qOverP()); 
  stateNegAtT.setState(xT3 + xkick, yT3, zEndT3,
                       tx + xkick / dz, ty, aState.qOverP());
  statePosAtT.setCovariance( aState.covariance() );
  stateNegAtT.setCovariance( aState.covariance() );
}

void L0ConfExtrapolator::ecal2T( const LHCb::Track& ecalTrack, 
                                 LHCb::State& statePosAtT,
                                 LHCb::State& stateNegAtT ) const
{
  // kick in x depends on momentum
  double xkick = m_ecalKick * ecalTrack.firstState().qOverP();
  calo2T(ecalTrack.firstState(), xkick, statePosAtT, stateNegAtT);
}

void L0ConfExtrapolator::hcal2T( const LHCb::Track& hcalTrack,
                                 LHCb::State& statePosAtT,
                                 LHCb::State& stateNegAtT ) const
{
  // }
  double z = hcalTrack.firstState().z();
  if (msgLevel(MSG::DEBUG)) {
    debug() << "Seeding using ";
    if (13360.0 > z)
      debug() << "ECal and HCal" << endmsg;
    else
      debug() << "HCal only" << endmsg;
  }
  // kick in x depends on momentum
  // different constants for HCal-only and ECal/HCal seeding
  double xkick = hcalTrack.firstState().qOverP();
  if (13360. > z) xkick = m_HECalKick[0] + m_HECalKick[1] * xkick;
  else xkick = m_HCalKick[0] + m_HCalKick[1] * xkick;
  calo2T(hcalTrack.firstState(), xkick, statePosAtT, stateNegAtT);
}




double L0ConfExtrapolator::getBScale()
{
  return m_BscaleFactor;
}

/*
 *   this helper function checks for the physical regions of ECal 
 *   and HCal respectively, depending on z
 */
int L0ConfExtrapolator::getCaloRegion(double stateX, 
                                      double stateY, 
                                      double stateZ ) const
{
  double x = std::abs(stateX);
  double y = std::abs(stateY);
  // two small tables to look up HCAL/ECAL region boundaries
  static const double xpos[] = { 650., 1940., 3880.,   2101., 4202., -1. };
  static const double ypos[] = { 650., 1450., 2420.,   1838., 3414., -1. };

  // depending on ECAL/HCAL+ECAL seeding, the starting offset into the
  // table is different
  int reg = (13360. > stateZ)?0:3;

  while (xpos[reg] > 0.0) {
    if (x < xpos[reg] && y < ypos[reg]) break;
    ++reg;
  }
 
  if (msgLevel(MSG::DEBUG)) {
    if (13360. > stateZ)
      debug() << "HCAL + ECAL Seeding" << endmsg;
    else
      debug() << "HCAL Seeding" << endmsg;
    if (-1. == xpos[reg])
      debug() << "CALO: not in calo!?!,  x,y = "
              << stateX << " , " << stateY << endmsg;

    debug() << "HCAL+ ECAL section 0 : x,y = " << stateX << " , " << stateY
            << endmsg
            << " .................................   region is: " << reg
            << endmsg;
  }
  if (-1. == xpos[reg]) reg = -1;
  
  return reg;
}
