#include "Event/HltDecReports.h"
#include "GaudiAlg/Producer.h"
#include "Kernel/TCK.h"
#include <algorithm>

class HltDecReportsCreator
    : public Gaudi::Functional::Producer<LHCb::HltDecReports()> {
public:
  HltDecReportsCreator(const std::string &name, ISvcLocator *svcLoc)
      : Producer(name, svcLoc,
                 KeyValue("HltDecReportsLocation",
                          {LHCb::HltDecReportsLocation::Default})){};

  LHCb::HltDecReports operator()() const override;

private:
  Gaudi::Property<TCK> m_tck{this, "TCK"};
};

LHCb::HltDecReports HltDecReportsCreator::operator()() const {
  LHCb::HltDecReports outputs;
  outputs.setConfiguredTCK(m_tck.value().uint());
  return outputs;
}

DECLARE_COMPONENT(HltDecReportsCreator)