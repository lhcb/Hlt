"""Trivial Hlt1 lines for Ion and Fixed Targed physics.

Based on Sascha Stahl's Hlt1HeavyIonLines.py
"""


from HltLine.HltLinesConfigurableUser import HltLinesConfigurableUser
from HltLine.HltLine import Hlt1Line
from Configurables import HltRecoConf


class Hlt1IFTLinesConf(HltLinesConfigurableUser):
    __slots__ = {
        'ODIN': {
            'BENoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
            'EBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
            'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
            'BBHighMult': 'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
            'BBVeryHighMult': 'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
            'BEHighMult': 'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
            'BEVeryHighMult': 'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
            'BBMicroBiasVelo': 'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
            'BEMicroBiasLowMultVeloNoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
            'BBHasTrack':  'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
            'BEHasTrack':  'ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.SequencerTrigger)',
        },
        'L0': {
        },
        'GEC': {
            'BEMicroBiasVelo': 'HeavyIonsLoose',
            'EBMicroBiasVelo': 'HeavyIonsLoose',
            'BEMicroBiasLowMultVelo': 'HeavyIonsLoose',
            'BEMicroBiasLowMultVeloNoBias': 'HeavyIonsLoose',
            'BBMicroBiasVelo': 'HeavyIonsLoose',
            'BBMicroBiasLowMultVelo': 'HeavyIonsLoose',
            'BBMicroBiasSoftCEP': 'HeavyIonsLoose',
            'BBHighMult': 'HeavyIonsLoose',
            'BBVeryHighMult': 'HeavyIonsLoose',
            'BEHighMult': 'HeavyIonsLoose',
            'BEVeryHighMult': 'HeavyIonsLoose',
            'BBHasTrack': 'HeavyIonsLoose',
            'BEHasTrack': 'HeavyIonsLoose',
        },
        'MaxVeloTracks': {
            'BEMicroBiasLowMultVelo': 10,
            'BEMicroBiasLowMultVeloNoBias': 10,
        },
        'MinVeloTracks': {
            'BEMicroBiasVelo': 1,
            'EBMicroBiasVelo': 1,
            'BEMicroBiasLowMultVelo': 1,
            'BEMicroBiasLowMultVeloNoBias': 1,
            'BBMicroBiasVelo': 1,
            'BBMicroBiasLowMultVelo': 1,
            'BBMicroBiasSoftCEP': 1,
        },
        'BEMicroBiasLowMultVelo': {
            'MinEta': 3.5,
            'MaxEta': 7.0,
        },
        'BEMicroBiasLowMultVeloNoBias': {
            'MinEta': 3.5,
            'MaxEta': 7.0,
        },
        'SingleTrack':{
            "SingleTrackPT": HltRecoConf().getProp("Forward_HPT_MinPt"),
            'SingleTrackTrGP' : HltRecoConf().getProp("MaxTrGHOSTPROB"),
            'TrChi2'   : HltRecoConf().getProp("MaxTrCHI2PDOF")
        }
    }

    __odin_bxtype_filters = {
        'BB': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
        'BE': 'ODIN_BXTYP == LHCb.ODIN.Beam1',
        'EB': 'ODIN_BXTYP == LHCb.ODIN.Beam2',
    }

    def __odin(self, bxtype, line):
        bx_filter = self.__odin_bxtype_filters[bxtype]
        line_filter = self.getProp("ODIN").get(line, None)
        if line_filter:
            return '({}) & ({})'.format(bx_filter, line_filter)
        else:
            return bx_filter

    def __l0(self, line):
        return self.getProp("L0").get(line, None)

    def __gec_algos(self, line, inverted=False):
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        gec = self.getProp("GEC").get(line, None)
        if gec == 'HeavyIonsPass':
            return [
                    Hlt1GECUnit("HeavyIonsTight", accept=False),
                    Hlt1GECUnit("HeavyIonsLoose", accept=True)
                    ] 
        return [Hlt1GECUnit(gec, accept=not inverted)] if gec else []

    def __min_velo_tracks(self, line):
        return self.getProp("MinVeloTracks").get(line)

    def __max_velo_tracks(self, line):
        return self.getProp("MaxVeloTracks").get(line)

    def __one_track_unit(self, line):
    
        lineCode = """
        SELECTION( 'Hlt1SharedPions' )
        >> ( PT > %(SingleTrackPT)s * MeV )
        >> (TRCHI2DOF < %(TrChi2)s)
        >> (TRGHOSTPROB < %(SingleTrackTrGP)s)
        >> SINK ( 'Hlt1%(line)sDecision' )
        >> ~TC_EMPTY
        """ % dict(self.getProp("SingleTrack"), line=line)
    
        from Configurables import LoKi__HltUnit as HltUnit
        
        hlt1SMOGLine_SingleTrackUnit = HltUnit(
            'Hlt1{}Unit'.format(line),
            Monitor = True,
            Code = lineCode
            )
    
        return hlt1SMOGLine_SingleTrackUnit
  

    def __create_nobias_line(self, bxtype, line):
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            postscale=self.postscale,
        )

    def __create_microbias_line(self, bxtype, line):
        from HltTracking.HltSharedTracking import MinimalVelo
        from Configurables import LoKi__VoidFilter

        filter_code = "CONTAINS('{}') >= {}".format(
            MinimalVelo.outputSelection(), self.__min_velo_tracks(line))
        algos = self.__gec_algos(line) + [
            MinimalVelo,
            LoKi__VoidFilter('Hlt1{}Decision'.format(line), Code=filter_code)
        ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            algos=algos,
            postscale=self.postscale
        )

    def __create_highmult_line(self, bxtype, line):
        from Configurables import LoKi__VoidFilter
        inverted_gecs = self.__gec_algos(line, inverted=True)
        algos = inverted_gecs + [
            # Dummy filter is needed to avoid the segfault issue (the gec
            # algorithm instance being destroyed multiple times)
            LoKi__VoidFilter('Hlt1{}Decision'.format(line), Code="FALL")
        ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            algos=algos,
            postscale=self.postscale
        )

    def __create_lowmult_line(self, bxtype, line):
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo

        from Configurables import LoKi__Hybrid__CoreFactory as CoreFactory
        add = lambda x, e: x if e in x else x + [e]
        CoreFactory('Hlt1CoreFactory').Modules = add(
            CoreFactory('Hlt1CoreFactory').Modules, 'LoKiTrack.decorators')

        props = self.getProp(line).copy()
        code = ("in_range({mintr}, TrNUM('{container}'), {maxtr}) &"
                "(TrNUM('{container}', TrBACKWARD) < 3) & "
                "(TrNUM('{container}', ~TrBACKWARD & in_range({MinEta}, TrETA, {MaxEta})) > 0)"
                .format(container=MinimalVelo.outputSelection(),
                        mintr=self.__min_velo_tracks(line),
                        maxtr=self.__max_velo_tracks(line),
                        **props))
        algos = self.__gec_algos(line) + [
            MinimalVelo,
            VoidFilter('Hlt1{}Decision'.format(line), Code=code)
        ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            algos=algos,
            postscale=self.postscale
        )

    def __create_singletrack_line(self,bxtype,line):
        from Hlt1SharedParticles import Hlt1SharedParticles
        sharedParticles = Hlt1SharedParticles()
        pionUnit = sharedParticles.pionUnit()

        algos = self.__gec_algos(line) + [
                pionUnit, 
                self.__one_track_unit(line)
                ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            postscale=self.postscale,
            algos=algos
        )


    def __apply_configuration__(self):
        self.__create_nobias_line('BE', 'BENoBias')
        self.__create_nobias_line('EB', 'EBNoBias')
        self.__create_nobias_line('BB', 'BBNoBias')

        self.__create_microbias_line('BE', 'BEMicroBiasVelo')
        self.__create_microbias_line('EB', 'EBMicroBiasVelo')
        self.__create_microbias_line('BB', 'BBMicroBiasVelo')
        self.__create_microbias_line('BB', 'BBMicroBiasLowMultVelo')
        self.__create_microbias_line('BB', 'BBMicroBiasSoftCEP')

        self.__create_lowmult_line('BE', 'BEMicroBiasLowMultVelo')
        #self.__create_lowmult_line('BE', 'BEMicroBiasLowMultVeloNoBias')


        self.__create_highmult_line('BB', "BBHighMult")
        self.__create_highmult_line('BE', "BEHighMult")

        self.__create_highmult_line('BB', "BBVeryHighMult")
        self.__create_highmult_line('BE', "BEVeryHighMult")

        self.__create_singletrack_line('BB', "BBHasTrack")
        self.__create_singletrack_line('BE', "BEHasTrack")
