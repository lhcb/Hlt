from Gaudi.Configuration import *
from HltLine.HltLinesConfigurableUser import *
import re

class Hlt1ElectronLinesConf( HltLinesConfigurableUser ):
    # steering variables
    __slots__ = {
        #  Electron Lines
        'DoTiming'                                 : False
        ,'SingleElectronNoIP_L0ElectronThreshold'  :   254
        ,'SingleElectronNoIP_P'                    :  8000
        ,'SingleElectronNoIP_PT'                   :  4800
        ,'SingleElectronNoIP_TrChi2'               :     4
        ,'SingleElectronNoIP_TrNTHits'             :     0 #OFF
        ,'SingleElectronNoIP_Velo_NHits'           :     0 #OFF
        ,'SingleElectronNoIP_Velo_Qcut'            :   999 #OFF
        ,'SingleElectronNoIP_GEC'                  : 'Loose'

        #DiElectronHighMass cuts
        ,'DiElectronHighMass_L0ElectronThreshold' : 0
        ,'DiElectronHighMass_TrChi2' : 4
        ,'DiElectronHighMass_GEC'    : 'Loose'
        ,'DiElectronHighMass_TrPT'   : 500
        ,'DiElectronHighMass_TrP'   :  3000
        ,'DiElectronHighMass_VxDOCA' : 0.5
        ,'DiElectronHighMass_VxChi2' : 10
        ,'DiElectronHighMass_IPChi2' : -1
        ,'DiElectronHighMass_MMIN'   : 0
        ,'DiElectronHighMass_MMAX'   : 4000
        ,'DiElectronHighMass_ShVelo' : 100
        ,'DiElectronHighMass_EID' : 'OR'
        ,'DiElectronHighMass_JpsiPT' : 0

        #DiElectronLowMass cuts
        ,'DiElectronLowMass_L0ElectronThreshold' : 0
        ,'DiElectronLowMass_TrChi2' : 4
        ,'DiElectronLowMass_GEC'    : 'Loose'
        ,'DiElectronLowMass_TrPT'   : 500
        ,'DiElectronLowMass_TrP'   :  3000
        ,'DiElectronLowMass_VxDOCA' : 0.5
        ,'DiElectronLowMass_VxChi2' : 10
        ,'DiElectronLowMass_IPChi2' : 4
        ,'DiElectronLowMass_MMIN'   : 10
        ,'DiElectronLowMass_MMAX'   : 300
        ,'DiElectronLowMass_ShVelo' : 100
        ,'DiElectronLowMass_EID' : 'AND'

        #DiElectronLowMassNoIP cuts
        ,'DiElectronLowMassNoIP_L0ElectronThreshold' : 0
        ,'DiElectronLowMassNoIP_TrChi2' : 4
        ,'DiElectronLowMassNoIP_GEC'    : 'Loose'
        ,'DiElectronLowMassNoIP_TrPT'   : 500
        ,'DiElectronLowMassNoIP_TrP'   :  3000
        ,'DiElectronLowMassNoIP_VxDOCA' : 0.5
        ,'DiElectronLowMassNoIP_VxChi2' : 10
        ,'DiElectronLowMassNoIP_IPChi2' : -1
        ,'DiElectronLowMassNoIP_MMIN'   : 10
        ,'DiElectronLowMassNoIP_MMAX'   : 300
        ,'DiElectronLowMassNoIP_ShVelo' : 100
        ,'DiElectronLowMassNoIP_EID' : 'AND'

        ,'L0Channels'                              : { 'SingleElectronNoIP'   : ( 'Electron', )
                                                     , 'DiElectronHighMass' : ( 'ElectronLoose', )
                                                     , 'DiElectronLowMass' : 'L0_DECISION_PHYSICS'
                                                     , 'DiElectronLowMassNoIP' : 'L0_DECISION_PHYSICS'
                                                     }
        }

    def localise_props( self, prefix ):
        ps = self.getProps()
        props = dict( [ ( key.split( '_', 1 )[ 1 ], ps[ key ] ) for key in ps if key.find( prefix ) != -1 ] )
        props[ 'name' ] = prefix
        return props

    def do_timing( self, unit ):
        from Configurables import LoKi__HltUnit as HltUnit
        if not isinstance( unit, HltUnit ):
            return unit
        reco = set()
        for entry in unit.Preambulo:
            s = entry.split( '=' )
            if s[ 0 ].find( 'PV3D' ) != -1 or s[ 0 ].find( 'GEC' ) != -1: continue
            if len( s ) > ( 1 ):
                reco.add( s[ 0 ].strip() )
        name = unit.name()[ 4 : unit.name().find( 'Streamer' ) ]
        code = unit.Code
        for step in reco:
            sub = " ( timer( '%s_%s' )" % ( name, step ) + ' % ' +  step + ' ) '
            code = re.sub( '\\s+%s\\s+' % step, sub, code )
        unit.Code = code
        return unit

    #make into a function
    def getCaloUnit( self, properties ) :
        from Configurables import LoKi__HltUnit as HltUnit
        from HltTracking.Hlt1Tracking import L0CaloCandidates
        caloUnit = HltUnit(
            'Hlt1%(name)sL0CaloStreamer' % properties,
            Preambulo = [ L0CaloCandidates( properties[ 'name' ] ) ],
            Code = """
            L0CaloCandidates
            >>  TC_CUT( LoKi.L0.L0CaloCut( 0, %(L0ElectronThreshold)s ), 0 )
            >>  tee  ( monitor( TC_SIZE > 0, '# pass L0CaloCut', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE    , 'nCaloCandidates' , LoKi.Monitoring.ContextSvc ) )
            >>  SINK( '%(CaloCandidates)s' )
            >> ~TC_EMPTY
            """ % properties
            )
        return caloUnit

    def singleElectron_preambulo( self, properties ):
        from HltTracking.Hlt1Tracking import ( TrackCandidates, FitTrack )
        from HltTracking.Hlt1TrackMatchConf import FilterVeloL0Calo
        from Configurables import Hlt__L0Calo2Candidate

        ## define the preambulo
        preambulo = [ TrackCandidates( properties[ 'name' ] ),
                      FilterVeloL0Calo( properties[ 'CaloCandidates' ] ),
                      FitTrack ]
        return preambulo

    def singleElectron_streamer( self, properties ):
        from Configurables import LoKi__HltUnit as HltUnit
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        properties[ 'CaloCandidates' ] = 'CaloCandidates'

        trackUnit = HltUnit(
            'Hlt1%(name)sTrackStreamer' % properties,
            ##OutputLevel = 1 ,
            Preambulo = self.singleElectron_preambulo( properties ),
            Code = """
            TrackCandidates
            >>  FilterVeloL0Calo
            >>  ( ( TrIDC('isVelo') > %(Velo_NHits)s ) & ( TrNVELOMISS < %(Velo_Qcut)s ) )
            >>  tee  ( monitor( TC_SIZE > 0, '# pass match', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE    , 'nMatched' , LoKi.Monitoring.ContextSvc ) )
            >>  (TrTNORMIDC > %(TrNTHits)s )
            >>  tee  ( monitor( TC_SIZE > 0, '# pass forward', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE , 'nForward' , LoKi.Monitoring.ContextSvc ) )
            >>  ( ( TrPT > %(PT)s * MeV ) & ( TrP  > %(P)s  * MeV ) )
            >>  FitTrack
            >>  tee  ( monitor( TC_SIZE > 0, '# pass fit', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE , 'nFitted' , LoKi.Monitoring.ContextSvc ) )
            >>  ( ( TrCHI2PDOF < %(TrChi2)s ) )
            >>  SINK( 'Hlt1%(name)sDecision' )
            >> ~TC_EMPTY
            """ % properties
            )
        return [ Hlt1GECUnit( properties[ 'GEC' ] )
                , self.getCaloUnit( properties )
                , trackUnit ]

    def dielectron_preambulo( self, properties ) :
        from HltTracking.Hlt1Tracking import ( TrackCandidates, FitTrack )
        from HltTracking.Hlt1Tracking import LooseForward
        from HltTracking.Hlt1TrackMatchConf import FilterTrackL0Calo
        from HltTracking.Hlt1TrackMatchConf import CandsTrackL0Calo
        from Configurables import Hlt__L0Calo2Candidate

        preambulo = [ TrackCandidates( properties[ 'name' ] ),
                      FilterTrackL0Calo( properties[ 'CaloCandidates' ] ),
                      FitTrack ]

        preambulo += [ "VertexConf = LoKi.Hlt1.VxMakerConf( %(VxDOCA)f * mm, %(VxChi2)f )" % properties,
             "MakeVertex = TC_VXMAKE4( '', VertexConf )",
             "from LoKiPhys.decorators import RV_MASS, RV_TrFUN", #RV_SHAREDHITS" ,
             "from LoKiTrigger.decorators import TrPT, TrQ",
             CandsTrackL0Calo( properties[ 'CaloCandidates' ] ),
             LooseForward ]
        preambulo += [ "Q1 = RV_TrFUN(TrQ,0)",
             "Q2 = RV_TrFUN(TrQ,1)",
             "QProd = Q1*Q2",
             "from LoKiPhys.decorators import RV_TrFUN, NTRACKS",
             "PX1 = RV_TrFUN(TrPX,0)",
             "PY1 = RV_TrFUN(TrPY,0)",
             "PX2 = RV_TrFUN(TrPX,1)",
             "PY2 = RV_TrFUN(TrPY,1)",
             "JpsiPTCut = ( sqrt((PX1+PX2)**2 + (PY1+PY2)**2) > %(DiElectronHighMass_JpsiPT)s * MeV )" % self.getProps()
             ]

        return preambulo

    def dielectron_streamer( self, properties) :
        from Configurables import LoKi__HltUnit as HltUnit
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        properties[ 'CaloCandidates' ] = '%(name)sCandidates' % properties

        # TODO: use in_range for mass cut
        # TODO: add shared velo hits cut  >>  ( RV_SHAREDHITS ( 1 ) < %(ShVelo)s )
        code = """
        TrackCandidates
        """
        # AND = both tracks pass L0 electron ID, so just filter at track level
        if properties['EID'] is 'AND':
            code += ">>  FilterTrackL0Calo"
        code += """
        >>  tee  ( monitor( TC_SIZE > 0, '# pass match', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE    , 'nMatched' , LoKi.Monitoring.ContextSvc ) )
        >>  ( ( TrPT > %(TrPT)s * MeV ) & ( TrP  > %(TrP)s  * MeV ) )
        >>  FitTrack
        >>  tee  ( monitor( TC_SIZE > 0, '# pass fit', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE , 'nFitted' , LoKi.Monitoring.ContextSvc ) )
        >>  ( ( TrCHI2PDOF < %(TrChi2)s )\
        & ( Tr_HLTMIPCHI2 ( 'PV3D' ) > %(IPChi2)s ) )
        >>  tee  ( monitor( TC_SIZE > 0, '# pass chi2ndof and ip chi2', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE , 'nChi2' , LoKi.Monitoring.ContextSvc ) )
        >>  MakeVertex
        >>  tee  ( monitor( TC_SIZE > 0, '# pass vertex', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE , 'nVertices' , LoKi.Monitoring.ContextSvc ) )
         >>  ( QProd == -1 )
        >>  ( RV_MASS ( 'e+' , 'e-' ) < %(MMAX)s * MeV )
        >>  ( RV_MASS ( 'e+' , 'e-' ) > %(MMIN)s * MeV )
        >>  tee  ( monitor( TC_SIZE > 0, '# pass mass', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE , 'nPassedMassCut' , LoKi.Monitoring.ContextSvc ) )
        """
        # OR = require at least one electron, require the vertex has one
        if properties['EID'] is 'OR':
            code += ">>  ( RV_TrHAS( CandsTrackL0Calo ) )"
        code += """
        >>  tee  ( monitor( TC_SIZE > 0, '# has electron', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE , 'nHasElectron' , LoKi.Monitoring.ContextSvc ) )
        >>  JpsiPTCut
        >>  tee  ( monitor( TC_SIZE > 0, '# pass JpsiPT', LoKi.Monitoring.ContextSvc ) )
        >>  tee  ( monitor( TC_SIZE , 'JpsiPT' , LoKi.Monitoring.ContextSvc ) )
        >>  SINK( 'Hlt1%(name)sDecision' )
        >> ~TC_EMPTY
        """
        unit = HltUnit('Hlt1%(name)sStreamer' % properties,
             Preambulo = self.dielectron_preambulo( properties ),
             Code = code % properties)
        gec = properties[ 'GEC' ]
        from HltTracking.HltPVs import PV3D
        return [Hlt1GECUnit(gec),self.getCaloUnit(properties),PV3D('Hlt1'),unit]


    def build_line( self, name, streamer ):
        from HltLine.HltLine import Hlt1Line
        algos = [ self.do_timing( unit ) if self.getProp('DoTiming') else unit for unit in streamer( self.localise_props( name ) ) ]
        L0DU = self.getProp( "L0Channels" )[ name ]
        if type( L0DU ) != type( "" ):
            L0DU = "|".join( [ "L0_CHANNEL('%s')" % channel for channel in L0DU ] )
        line = Hlt1Line(
            name,
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = L0DU,
            algos = algos
            )

    def __apply_configuration__( self ) :
         ## Create the lines
        to_build = [ ( 'SingleElectronNoIP', self.singleElectron_streamer ) ]
        to_build += [ ( 'DiElectronHighMass',     self.dielectron_streamer ) ]
        to_build += [ ( 'DiElectronLowMass',     self.dielectron_streamer ) ]
        to_build += [ ( 'DiElectronLowMassNoIP',     self.dielectron_streamer ) ]
        for line, streamer in to_build:
            self.build_line( line, streamer )
