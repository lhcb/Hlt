###
# @file   Ds.py
# @author Max Chefdeville (chefdevi@lapp.in2p3.fr)
# @date   17.01.2017
# Ds2KKPi taken from stripping StdLookseDsplus2KKPi for CaloPID studies with Ds*2DsGamma
##
from Gaudi.Configuration import *
from Configurables import CombineParticles
from HltLine.HltLine import bindMembers, Hlt2Member
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedKaons, BiKalmanFittedPions

__all__ = ('Ds')

Hlt2SharedDs = Hlt2Member( CombineParticles
                                , "Ds"
                                , Inputs = [ BiKalmanFittedPions, BiKalmanFittedKaons ]
                                , DecayDescriptor = "[D_s+ -> K+ K- pi+]cc"
                                , DaughtersCuts = { "K+" : "(PROBNNk>0.1) & (PT>250*MeV) & (MIPCHI2DV(PRIMARY)>4)" , "pi+" : "(PT>250*MeV) & (MIPCHI2DV(PRIMARY)>4)" }
                                , CombinationCut = "((ADAMASS('D_s+')<30*MeV) & ((APT>1.*GeV) | (ASUM(PT)>1.*GeV)) & (ADOCAMAX('')<0.5*mm))"
                                , MotherCut = "((VFASPF(VCHI2PDOF)<30) & (BPVVDCHI2>36) & (BPVDIRA>0.98))" )

Ds = bindMembers( 'Shared' , [ BiKalmanFittedKaons, BiKalmanFittedPions , Hlt2SharedDs ] )
