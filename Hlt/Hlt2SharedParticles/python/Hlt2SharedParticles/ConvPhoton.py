### @file
#
# Standard converted photons
#  @author S. Benson sean.benson@cern.ch
#  @date 2015-02-07
#
##
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import *
from Configurables import CombineParticles, DiElectronMaker, BremAdder
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedElectrons as Electrons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedNoBremElectrons as NoBremElectrons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedBremPhotons as BremPhotons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedDownElectrons as DownElectrons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedNoBremDownElectrons as NoBremDownElectrons
from HltLine.HltLine import bindMembers, Hlt2Member
from HltTracking.HltPVs import PV3D
from HltLine.HltLine import Hlt1Tool as Tool

__all__ = ( 'ConvPhotonLL', 'ConvPhotonDD', 'ConvPhotonNoBremLL', 'ConvPhotonNoBremDD' )

BA = Tool(type = BremAdder, name = 'BremAdder', BremInput = BremPhotons.outputSelection())
Hlt2DiEl_LL = Hlt2Member(DiElectronMaker
        ,'LL'
        , DecayDescriptor = "gamma -> e+ e-"
        ## Input/Inputs are not actually used, but this silences the silly warnings
        , Input = ""
        , Inputs = []
        , ElectronInputs = [Electrons.outputSelection()]
        , DeltaY = 3.
        , DeltaYmax = 200. * mm
        , DiElectronMassMax = 80.*MeV
        , DiElectronPtMin = 200.*MeV
        , tools = [BA]
        )

Hlt2DiEl_DD = Hlt2Member(DiElectronMaker
        ,'DD'
        , DecayDescriptor = "gamma -> e+ e-"
        ## Input/Inputs are not actually used, but this silences the silly warnings
        , Input = ""
        , Inputs = []
        , ElectronInputs = [DownElectrons.outputSelection()]
        , DeltaY = 3.
        , DeltaYmax = 200. * mm
        , DiElectronMassMax = 100.*MeV
        , DiElectronPtMin = 200.*MeV
        , tools = [BA]
        )

# Alternative set of converted photons using CombineParticles and no bremsstrahlung correction
# Kinematic cuts are similar, but the DeltaY cuts used by DiElectronMaker are not simply
# reproducible using a LoKi cut string.
Hlt2DiEl_NoBrem_LL = Hlt2Member(CombineParticles
        , 'LLNoBrem'
        , DecayDescriptor = "gamma -> e+ e-"
        , Inputs = [ NoBremElectrons ]
        , DaughtersCuts = { 'e+' : '(PT > 100.0 * MeV)' }
        , CombinationCut = "(APT > 200.0 * MeV) & (in_range(0.0, AM, 80.0 * 1.5 * MeV)) & (ADOCACHI2('') < 15.0)"
        , MotherCut = "(in_range(0.0 * MeV, M, 80.0 * MeV)) & (VFASPF(VCHI2) < 15.0)"
        )

Hlt2DiEl_NoBrem_DD = Hlt2Member(CombineParticles
        , 'DDNoBrem'
        , DecayDescriptor = "gamma -> e+ e-"
        , Inputs = [ NoBremDownElectrons ]
        , DaughtersCuts = { 'e+' : '(PT > 100.0 * MeV)' }
        , CombinationCut = "(APT > 200.0 * MeV) & (in_range(0.0, AM, 100.0 * 1.5 * MeV)) & (ADOCACHI2('') < 15.0)"
        , MotherCut = "(in_range(0.0 * MeV, M, 100.0 * MeV)) & (VFASPF(VCHI2) < 15.0)"
        )

ConvPhotonLL = bindMembers( "ConvPhoton", [ BremPhotons, Electrons, Hlt2DiEl_LL ] )
ConvPhotonDD = bindMembers( "ConvPhoton", [ BremPhotons, DownElectrons, Hlt2DiEl_DD ] )
ConvPhotonNoBremLL = bindMembers( "ConvPhoton", [ NoBremElectrons, Hlt2DiEl_NoBrem_LL ] )
ConvPhotonNoBremDD = bindMembers( "ConvPhoton", [ NoBremDownElectrons, Hlt2DiEl_NoBrem_DD ] )
