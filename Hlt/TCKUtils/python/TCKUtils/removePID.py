"""Remove PID information from trigger lines.

See https://twiki.cern.ch/twiki/bin/view/LHCb/HltFAQ#How_do_I_run_the_HLT2_without_PI

"""
__all__ = [
    'removePID',
]

import re
import sys
from collections import defaultdict
from TCKUtils.utils import (getProperties, updateProperties, createTCKEntries,
                            getTCKList, getTCKInfo, tck_to_int, tck_to_str, default_cas,
                            ConfigAccessSvc, diff)

testedTCKs = [  # list of TCKs that were used to develop the default values in this script; referenced by Hlt/TCKUtils/tests/options/removepid.py
    0x411400a2,  # 2015 MC
    0x6139160F,  # 2016 MC
    0x5138160f,  # 2016 HLT1 MC
    0x62661709,  # 2017 MC
    0x51611709,  # 2017 HLT1 MC
    0x217d18a4,  # 2018 data
    0x617d18a4,  # 2018 MC
    0x517a18a4,  # 2018 HLT1 MC
]

_RE_PARTS = {
    "PIDx": r"PID(e|mu|pi|K|p)",
    "float": r"[-+]?(\d+\.?\d*|\.\d+)",
    "comp": r"(>=?|<=?|==)",
    "CombDLL": r"CombDLL\((e|mu|pi|K|p)-(e|mu|pi|K|p|)\)",
    "PROBNNx": r"PROBNN(e|mu|pi|(K|k)|p)",
}

DEFAULT_BLACKLISTED_PROPS = [
    r".*(Input|Output|Location).*",
    r"^(Filter1?|Prescale|Postscale|HLT1|L0DU)$",
    r"^(Context|DecisionName|Members|.*Dir|Histogram.*)$",
    r"^(RoutingBits|DecToGroup|ContainerMap|PackedContainers|Rates)$",
    r"^(Hlt2SelectionID|InfoID)$",
    r"^(GlobalName|Calos|ConditionName|Table|SplitPhotons|SplitClusters|MergedPi0s)$",
    r"^(MotherMonitor|LinesToCopy|VetoedContainers)$",
    r"^RawBankTypes$",
]

DEFAULT_PID_FLAGS = [  # indicate the presence of PID information
    r'.*(PID[^s]|PROBNN(?!ghost)|IsMuon|ISMUON|(?<!Photon)DLL).*',
]


def get_PID_Variables(tck, pid_flags=DEFAULT_PID_FLAGS, cas=default_cas):
    '''finds Variables property in tck containing a PID flag and returns a dictionary of the form
        r"(\()? *<PIDVAR> *{comp} *{float} *(\))?": '( ALL )'
    where <PIDVAR> is the found variable and {comp} and {float} come from _RE_PARTS (defined in removePID.py).
    The regular expression describes comparison to a float. So,
        '(DLLe < 3.0)'
    would match.
    '''
    assert isinstance(pid_flags, list)
    PID_REPLACEMENTS = {}
    PID_Variables = set()
    for pid in pid_flags:
        props = getProperties(tck, '.*', 'Variables', value=pid, cas=cas)
        for algorithm, properties in props.iteritems():
            for name, value in properties.iteritems():
                if value[0] == '{' and value[-1] == '}':
                    variable_dict = eval(value)
                    for key, val in variable_dict.iteritems():
                        if re.search(pid, val):
                            PID_Variables.add(key)
                else:
                    raise ValueError('{}.{} is not a dictionary: {}'.format(algorithm, name, value))
    
    for v in PID_Variables:
        PID_REPLACEMENTS[r"(\()? *{v444444} *{comp} *{float} *(\))?".format(v444444=v, **_RE_PARTS)] = '( ALL )'
    
    return PID_REPLACEMENTS


def make_DEFAULT_REPLACEMENTS(add_PID_vars=None):
    '''returns dictionary of RegEx keys to-be-replaced and string values to replace them with
    If add_PID_vars is not None (should be tuple of form (tck, dict, cas) and contain at least tck),
        the dictionary includes get_PID_Variables(tck, dict, cas)
    '''
    DEFAULT_REPLACEMENTS = {
        # (CHILD(1,PIDe) > 5.0)
        r"\( *CHILD\( *{float} *, *{PIDx} *\) *{comp} *{float} *\)".format(**_RE_PARTS):
            '( ALL )',
        # (CHILD(PIDp-PIDK,3) < 3.0)
        r"\( *CHILD\( *\(? *{PIDx} *- *{PIDx} *\)? *, *{float}\) *{comp} *{float} *\)".format(**_RE_PARTS):
            '( ALL )',
        # (PIDK < 10000) or (PROBNNe > 0.93)
        r"\( *({PIDx}|{PROBNNx}) *{comp} *{float} *\)".format(**_RE_PARTS):
            '( ALL )',
        # ((PIDp-PIDK) > 5)
        r"\( *\(? *{PIDx} *- *{PIDx} *\)? *{comp} *{float} *\)".format(**_RE_PARTS):
            '( ALL )',
        # MINTREE('mu-' == ABSID, PIDmu) > 1 or MINTREE('mu-' == ABSID, PROBNN) > 1 or MAXTREE
        r"(MIN|MAX)TREE\([^(),]+, *({PIDx}|{PROBNNx}) *\) *{comp} *{float}".format(**_RE_PARTS):
            'ALL',
        # CombDLL(mu-pi)>'-8.0'
        r"{CombDLL} *{comp} *'{float}'".format(**_RE_PARTS):
            '',
        # IsMuon=True or IsMuonLoose=False
        r"(\n *>> *)?(?<!'# pass )(?<!'n)IsMuon(Loose|Tight)?(=(True|False))?":
            '',
        # ISMUON
        r"ISMUON(LOOSE|TIGHT)?":
            'ALL',
        # PPCUT(PP_HASINFO(LHCb.ProtoParticle.RichPIDStatus))
        r"PPCUT\( *PP_HASINFO\( *LHCb\.ProtoParticle\.RichPIDStatus *\) *\)":
            'ALL',
    }
    if add_PID_vars is not None:
        if not isinstance(add_PID_vars, tuple):
            raise TypeError('expected tuple for add_PID_vars, not {}'.format(type(add_PID_vars)))
        DEFAULT_REPLACEMENTS.update(get_PID_Variables(*add_PID_vars))
    return DEFAULT_REPLACEMENTS


def removePID(
        tck,
        newtck=None,
        algname='.*',
        property='DaughtersCuts|Combination.*Cut|MotherCut|Selection|Code|ConfidenceLevel(Switch|Base)',
        replacements=None,
        label_extra='',
        cas=default_cas,
        print_diff=False):
    """Create a derived TCK without PID requirements.
    
    Removes PID requirements from trigger lines, e.g.
    - "(PIDK < 10000)"to "( ALL )" and
    - '[ "RequiresDet='CALO' CombDLL(e-pi)>'0.0'" ]' to '["RequiresDet='CALO' "]'
    
    Args:
        tck (int): Existing TCK.
        newtck (int): Non-existant TCK for the derived configuration.
            If None (default), `tck | 0x08000000` is attempted.
        algname (str): Pattern to match algorithms to be adjusted.
            See `getProperties` for more information.
        property (str): Pattern to match properties to be adjusted.
        replacements (dict): Dictionary of patterns and replacements
            of the form `{<pattern>: <repl>}`.
            If None, uses make_DEFAULT_REPLACEMENTS(tck, cas=cas).
        label_extra (str): String to be appended to the label of the
            new TCK.
    
    Returns:
        The TCK for the new configuration.
    
    """
    if not newtck:
        newtck = tck | 0x08000000
        print "Will map new configuration to TCK {}".format(tck_to_str(newtck))
    if tck_to_str(newtck) in getTCKList(cas):
        raise ValueError("TCK {} already exists".format(tck_to_str(newtck)))
    if tck not in testedTCKs:
        print 'WARNING: TCK {} was not used to develop removePID.py'.format(tck_to_str(tck))
    
    if replacements is None:
        print 'Making default replacements...'
        replacements = make_DEFAULT_REPLACEMENTS(add_PID_vars=(tck, DEFAULT_PID_FLAGS, cas))
    
    props = getProperties(tck, algname, property, cas=cas)
    newprops = defaultdict(dict)
    for algorithm, properties in props.iteritems():
        for name, value in properties.iteritems():
            new_value = value
            for pattern, repl in replacements.iteritems():
                new_value = re.sub(pattern, repl, new_value)
            if new_value != value:
                newprops[algorithm][name] = new_value
    newprops = dict(newprops)
    
    label = getTCKInfo(tck, cas=cas)[2]
    id = updateProperties(
        tck,
        newprops,
        label='{} - Same as {} without PID cuts'.format(
            label, tck_to_str(tck)) + label_extra,
        cas=cas)
    createTCKEntries({newtck: id}, cas=cas)
    
    if print_diff:
        print "==================> diff <===================="
        diff(tck, newtck, human=True, cas=cas)
    
    if not isPIDless(newtck, pid_flags=DEFAULT_PID_FLAGS + replacements.keys(), cas=cas):
        raise RuntimeError(
            "Found unhandled PID-like properties, check stderr.")
    
    print 'done'
    return newtck


def isPIDless(tck, blacklisted_properties=DEFAULT_BLACKLISTED_PROPS, pid_flags=DEFAULT_PID_FLAGS,
              warn=sys.stderr, cas=default_cas):
    """Return if a TCK looks like to have no PID requirements.
    
    The logic is to take non-blacklisted_properties and check if
    PIDx / PROBNN / IsMuon / DLL (the default pid_flags) is found in the value.
    
    Returns:
        True if no PID-like properties are found, False otherwise.
    
    """
    unhandled = []
    for pid in pid_flags:
        pid_like_props = getProperties(
            tck, value=pid, cas=cas)
        for algorithm, properties in pid_like_props.iteritems():
            for name, value in properties.iteritems():
                if not any(re.match(pat, name) for pat in blacklisted_properties):
                    unh = True
                    if name == 'Variables':
                        # for Variables, it's okay if they contain PID information as long as the
                        # Variables themselves are not used
                        if value[0] == '{' and value[-1] == '}':
                            variable_dict = eval(value)
                            key_unh = []
                            for key, val in variable_dict.iteritems():
                                # if a Variable is included in pid_flags, it should not be
                                # considered unhandled just for referring to PID information,
                                # since anything that refers to it will be considered unhandled
                                if any(re.search(x, key) for x in pid_flags):
                                    # this key is considered a PID flag and is handled elsewhere (it will trigger if it appears)
                                    key_unh.append(False)
                                elif any(re.search(x, val) for x in pid_flags):
                                    # this key was not considered a PID flag and is unhandled
                                    key_unh.append(True)
                                else:
                                    # this key contains no PID flag information
                                    key_unh.append(False)
                            if not any(key_unh):
                                # none of the key, val pairs are unhandled
                                unh = False
                        else:
                            raise TypeError('{}.{} is not a dictionary: {}'.format(algorithm, name, value))
                    elif name == 'Preambulo':
                        # sometimes, the Preambulo will redefine, e.g., IsMuon.
                        # This is okay as long as IsMuon doesn't acutally get used as a requirement.
                        if value[0] == '[' and value[-1] == ']':
                            variable_list = eval(value)
                            key_unh = []
                            for vardec in variable_list:
                                if '=' in vardec:
                                    splvardec = vardec.split('=')
                                    if len(splvardec) != 2:
                                        raise ValueError('{}.{} is a list, but "{}" has more than one "="'.format(algorithm, name, vardec))
                                    key = splvardec[0].strip()
                                    val = splvardec[-1].strip()
                                    if any(re.search(x, key) for x in pid_flags):
                                        # this key is considered a PID flag and is handled elsewhere (it will trigger if it appears)
                                        key_unh.append(False)
                                    elif any(re.search(x, val) for x in pid_flags):
                                        # this key was not considered a PID flag and is unhandled
                                        key_unh.append(True)
                                    else:
                                        # this key contains no PID flag information
                                        key_unh.append(False)
                                else:
                                    if 'import' not in vardec:
                                        raise ValueError('{}.{} is a list, but "{}" does not declare anything'.format(algorithm, name, vardec))
                            if not any(key_unh):
                                # none of the key, val pairs are unhandled
                                unh = False
                        else:
                            raise TypeError('{}.{} is not a list: {}'.format(algorithm, name, value))
                    
                    if unh:
                        unhandled.append((algorithm, name, value))
    
    if unhandled and warn:
        warn.write('WARNING: found unhandled PID-like property values:\n')
        for algorithm, name, value in unhandled:
            warn.write('  {}.{} = \n    "{}"\n'.format(algorithm, name, value))
    return not unhandled


if __name__ == "__main__":
    from os.path import expandvars
    from shutil import copyfile
    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, description='make PID replacements in the trigger')
    parser.add_argument('TCK', type=tck_to_int,
                        help='What TCK would you like to make a PIDless TCK out of?')
    parser.add_argument('newTCK', nargs='?', type=tck_to_int, help='New TCK (optional)')
    parser.add_argument('--label-extra', default='', help='String to be appended to label of new TCK')
    modgrp = parser.add_mutually_exclusive_group(required=False)
    modgrp.add_argument('--io', nargs=2, metavar=('REF', 'DEST'), default=('$HLTTCKROOT/config.cdb', './config.cdb'),
                        help='Default behavior. The .cdb file to copy and the destination to copy it to.'
                        ' (removePID will modify the copied .cdb file.)')
    modgrp.add_argument('--modify', metavar='CDB_FILE', default=None,
                        help='If specified, removePID will instead modify this .cdb file in-place.')
    args = parser.parse_args()
    
    print "Create PID-less TCK from TCK {}".format(tck_to_str(args.TCK))
    if args.modify is not None:
        print 'modifying', args.modify
        newconf = args.modify
    else:
        print 'copy', args.io[0], 'to', args.io[-1], 'for modification...'
        newconf = args.io[-1]
        copyfile(expandvars(args.io[0]), newconf)
    removePID(tck=args.TCK, newtck=args.newTCK, label_extra=args.label_extra,
              cas=ConfigAccessSvc(Mode='ReadWrite', File=newconf), print_diff=True)
