"""Utilities for working with TCKs."""

import os
import re
from pprint import pprint, pformat

import backend
import helpers
import trees
import flow
import printers
from backend import default_cas, accept_id, getConfigTree
from backend import (ConfigAccessSvc, ConfigStackAccessSvc, ConfigDBAccessSvc,
                     ConfigCDBAccessSvc)

from pureutils import *
from trees import *
from flow import *
from level0 import *


def _getProperty(table, algname, property):
    if algname not in table : raise KeyError("no algorithm named %s in specified config"%algname )
    properties = table[algname].properties()
    if property not in properties: raise KeyError("could not locate property %s for algorithm %s in specified config"%(property,algname) )
    return properties[property]


@accept_id
def getProperty(tree, algname, property):
    table = xget(tree)
    return _getProperty(table, algname, property)


def _pformat(x):
    """pformat with a triple-quote repr of multi-line strings."""
    if isinstance(x, basestring):
        lines = x.splitlines()
        if len(lines) > 1:
            lines = [repr(l)[1:-1] for l in lines]
            return '"""' + '\n'.join(lines) + '"""'
    return pformat(x)


def diff_leafs(old, new, old_tck, new_tck, human=False, exclusions=[]):
    def exclude(leaf, props):
        props = props.copy()
        for ex in exclusions:
            for k in props.keys():
                if re.match(ex, leaf.type + ' ' + leaf.name + '.' + k):
                    del props[k]
        return props

    from difflib import unified_diff, ndiff
    if not human:
        return unified_diff(old.fmt(), new.fmt(),
                            old.fqn(), new.fqn(),
                            old_tck, new_tck, n=0)
    else:
        lines = [
            '--- {} ({})\t{}\n'.format(old.name, old.type, old_tck),
            '+++ {} ({})\t{}\n'.format(new.name, new.type, new_tck),
        ]
        old_props = exclude(old, old.evaled_props())
        new_props = exclude(new, new.evaled_props())
        for k in set(old_props.keys() + new_props.keys()):
            old_prop = old_props.get(k)
            new_prop = new_props.get(k)
            # TODO should we compare the unevaluated properties instead?
            if old_prop != new_prop:
                header = [sgn + k for sgn, p in [('-', old_prop), ('+', new_prop)] if p is not None]
                lines.append('@@ {} @@\n'.format(' '.join(header)))
                l = ndiff(_pformat(old_prop).splitlines() if old_prop is not None else [],
                          _pformat(new_prop).splitlines() if new_prop is not None else [],
                          None, None)
                lines.extend(i + '\n' for i in l)
        if len(lines) == 2:
            return []
        return lines


def diff(lhs, rhs, file=None, human=False, exclusions=[], cas=default_cas, cas_rhs=None):
    import cStringIO

    cas_rhs = cas_rhs or cas
    ltree = lhs if isinstance(lhs, backend.Tree) else getConfigTree(lhs, cas=cas)
    rtree = rhs if isinstance(rhs, backend.Tree) else getConfigTree(rhs, cas=cas_rhs)

    lnodes = {i.leaf.fqn(): i for i in ltree if i.leaf}
    rnodes = {i.leaf.fqn(): i for i in rtree if i.leaf}

    lleafs = set(lnodes.keys())
    rleafs = set(rnodes.keys())

    lhs_nice = hex(lhs) if isinstance(lhs, int) else lhs
    rhs_nice = hex(rhs) if isinstance(rhs, int) else rhs

    with helpers.smart_open(file) as f:
        common_keys = sorted(lleafs & rleafs)
        for i, k in enumerate(common_keys):
            l, r = lnodes[k].leaf, rnodes[k].leaf
            if l.digest != r.digest:
                lines = diff_leafs(l, r, lhs_nice, rhs_nice, human=human,
                                   exclusions=exclusions)
                if lines:
                    f.writelines(lines)
                    f.write('\n')

        # Find the top level RHS-only trees
        onlyRhsTopTrees = {}
        onlyRhsSubTrees = set()
        for tree in [rnodes[i] for i in rleafs - lleafs]:
            fqn = tree.leaf.fqn()
            if fqn not in onlyRhsSubTrees:
                onlyRhsTopTrees[fqn] = tree
                for st in tree.iter():
                    if st != tree:
                        onlyRhsSubTrees.add(st.leaf.fqn())
                        onlyRhsTopTrees.pop(st.leaf.fqn(), None)

        if lleafs - rleafs:
            f.write('--- Only in {}\n'.format(lhs_nice))
        if rleafs - lleafs:
            f.write('+++ Only in {}\n'.format(rhs_nice))
        for i in sorted(lleafs ^ rleafs):
            if i in lleafs:
                tree = lnodes[i]
                f.write('- {} ({})\n'.format(tree.leaf.name, tree.leaf.type))
            else:
                tree = rnodes[i]
                if tree.leaf.fqn() in onlyRhsTopTrees:
                    output = cStringIO.StringIO()
                    dump(tree, file=output, cas=cas)
                    f.writelines('+ ' + x for x in
                                 output.getvalue().splitlines(True))
                else:
                    f.write('+ {} ({})\n'
                            .format(tree.leaf.name, tree.leaf.type))


def copy(source=default_cas, target=None, glob=None):
    from Gaudi.Configuration import DEBUG
    if not target:
        target = ConfigDBAccessSvc(ReadOnly=False)
    if source == target:
        print 'WARNING: source and target are the same -- no-op ...'
        return
    stack_cas = ConfigStackAccessSvc(
        ConfigAccessSvcs=[target.getFullName(), source.getFullName()])
    r = backend.AccessProxy().access(cas=stack_cas).rcopy(glob)
    backend.AccessProxy().flush()
    return r


def _getComponents(tree, depth=0, kinds=None):
    leaf = tree.leaf
    if leaf and (not kinds or leaf.kind in kinds):
        yield (depth, leaf.name, str(leaf.digest))
    # # Don't write anything below the WriterFilter
    # if leaf and leaf.name == 'WriterFilter':
    #     return
    for i in tree.nodes:
        for x in _getComponents(i, depth + 1, kinds=kinds):
            yield x


@accept_id
def getComponents(tree, kinds=None):
    return _getComponents(tree, kinds=kinds)


@accept_id
def getAlgorithms(tree):
    return getComponents(tree, kinds=['IAlgorithm'])


@accept_id
def listComponents(tree, kinds=None):
    for depth, name, digest in getComponents(tree, kinds):
        s = depth * '   ' + name
        print s + (80-len(s))*' ' + digest


@accept_id
def listAlgorithms(tree):
    listComponents(tree, kinds=['IAlgorithm'])


@accept_id
def getProperties(tree, algorithm=None, property=None, value=None):
    """Return properties matching all of the given patterns.

    Args:
        id (str/int): Configuration id or TCK.
        algorithm (str): Pattern to match to algo's "type/name".
        property (str): Pattern to match to property names.
        value (str): Pattern to match to property values.

    Returns:
        A dict of dicts of the form
        {<algorithm name>: {<property>: <value>}}.

    """
    def identity(x):
        return True

    if algorithm:
        reqNode = re.compile(algorithm)
        matchNode = lambda leaf: reqNode.match(leaf.type + '/' + leaf.name)
    else:
        matchNode = identity
    matchProp = re.compile(property).match if property else identity
    matchValue = re.compile(value).match if value else identity

    retlist = {}
    for leaf in tree.iterleafs():
        if not leaf or not matchNode(leaf):
            continue
        pdict = {k: v for k, v in leaf.properties().iteritems() if matchProp(k) and matchValue(v)}
        if pdict:
            retlist[leaf.name] = pdict
    return retlist


@accept_id
def listProperties(tree, algname=None, property=None, value=None):
    """Print properties matching all of the given patterns.

    Args:
        id (str/int): Configuration id or TCK.
        algorithm (str): Pattern to match to algos "type/name".
        property (str): Pattern to match to property names.
        value (str): Pattern to match to property values.

    """
    for c, d in getProperties(tree, algname, property, value).iteritems():
        print '\nRequested Properties for %s' % c
        for k, v in d.iteritems():
            print "      '%s':%s" % (k, v)


def getConfigurations(cas=default_cas):
    return backend.AccessProxy().access(cas).rgetConfigurations()


def getTCKInfo(x, cas=default_cas):
    for i in getConfigurations(cas).itervalues():
        if x in i['TCK']:
            return (i['hlttype'], i['release'], i['label'])
    return None


def getReleases(cas=default_cas):
    return set([i['release'] for i in getConfigurations(cas).itervalues()])


def getHltTypes(release, cas=default_cas):
    info = getConfigurations(cas)
    return set([i['hlttype'] for i in info.itervalues() if i['release'] == release])


def getTCKs(release=None, hlttype=None, cas=default_cas):
    def pred(x):
        return (x['TCK'] and (not release or x['release'] == release) and
                (not hlttype or x['hlttype'] == hlttype))
    info = getConfigurations(cas)
    result = []
    for i in filter(pred, info.itervalues()):
        for tck in i['TCK']:
            result.append(('0x%08x' % tck, i['label']))
    return result


def getTCKList(cas=default_cas):
    info = getConfigurations(cas)
    result = []
    for i in info.itervalues():
        for tck in i['TCK']:
            result.append('0x%08x' % tck)
    return result


@accept_id
def getRoutingBits(tree):
    """Return the routing bits.

    Note that since 2016 there are multiple routing bit writers in HLT.
    This function returns the "union" of the bits set for all writers.
    There is one bit (for the moment) that has a different meaning in
    each HLT2 RB writer. The expression set to "<MULTIPLE>" for such
    bits.

    Args:
        tree (obj): Configuration tree or id.

    Returns:
        A dict of {bit: expression}.
    """
    rbs = {}
    for rbwriter_tree in matchInTree(tree, type='HltRoutingBitsWriter'):
        bits = rbwriter_tree.leaf.evaled_props()['RoutingBits']
        for bit, expr in bits.items():
            try:
                if rbs[bit] != expr:
                    rbs[bit] = '<MULTIPLE>'
                    # raise RuntimeError('Inconsistent RB writer for bit {}:'
                    #                    '{} != {}'.format(bit, rbs[bit], expr))
            except KeyError:
                rbs[bit] = expr
    return rbs if rbs else None


@accept_id
def dump(tree, properties=None, lines=None, file=None):
    with helpers.smart_open(file) as f:
        p = printers.TextTreePrinter(properties=properties, lines=lines, file=f)
        p.pprint_tree(tree)


def dump_flow(id, id1=None, file=None, cas=default_cas):
    """Write out the data flow of the lines in a TCK.

    Args:
        id (obj): Configuration tree or id.
        id1 (obj): Configuration tree or id containing the HLT 1 lines.
            Defaults to `id`. Only used in case `id` contains HLT 2 lines.
        file (obj): File object, filename or None (stdout, default).

    """
    def print_inputs(inputs):
        for typ, (inp, pretty) in inputs.iteritems():
            f.write('   {} ({}):\n'.format(typ, pretty))  # e.g. L0DU: or HLT1:
            for i in sorted(inp):
                f.write('      {}\n'.format(i))  # e.g. channel or line name

    id1 = id1 or id
    lines1 = getHlt1Lines(id1, cas=cas)
    lines2 = getHlt2Lines(id, cas=cas)
    with helpers.smart_open(file) as f:
        for line in sorted(lines1):
            f.write(line + '\n')
            print_inputs(getHltLineInputs(id1, line, None, ret_filter=True, cas=cas))
        if lines2:
            for line in sorted(lines2):
                f.write(line + '\n')
                print_inputs(getHltLineInputs(id, line, id1, ret_filter=True, cas=cas))

        from Configurables import HltOutputConf
        STREAMS = {v: k for k, v in HltOutputConf.__streams__.items()}
        rbits = getRoutingBits(id, cas=cas)
        for bit, code in sorted(rbits.items()):
            f.write('RB_{} {}\n'.format(bit, STREAMS.get(bit, '')))
            if code != '<MULTIPLE>':
                print_inputs(getRoutingBitInputs(id1 if bit < 64 else id, bit,
                                                 ret_filter=True, cas=cas))


def resolveTCK(tck, cas=default_cas):
    return backend.AccessProxy().access(cas).rresolveTCK(tck)


@accept_id
def xget(tree):
    id = tree.digest
    if id not in xget.forest.keys():
        xget.forest[id] = tree.leafs()
    return xget.forest[id]
xget.forest = {}


def updateProperties(id, updates, label='', hlttype=None, cas=default_cas):
    ret = backend.AccessProxy().access(cas).rupdateProperties(
        id, updates, label, hlttype=hlttype)
    if ret : backend.AccessProxy().flush() # explicit flush in case we wrote something
    return ret


def createTCKEntries(d, cas=default_cas):
    ret = backend.AccessProxy().access(cas).rcreateTCKEntries(d)
    backend.AccessProxy().flush()
    return ret


def _getHltLines(tree, sequence):
    try:
        fqns = eval(getProperty(tree, sequence, 'Members'))
        return [i.split('/')[1] for i in fqns]
    except KeyError:
        return []


@accept_id
def getHlt1Lines(tree):
    return _getHltLines(tree, 'Hlt1')


@accept_id
def getHlt2Lines(tree):
    return _getHltLines(tree, 'Hlt2')


@accept_id
def getHltLines(tree):
    return getHlt1Lines(tree) + getHlt2Lines(tree)


@accept_id
def getHlt1Decisions(tree):
    table = xget(tree)
    lines = eval(_getProperty(table,'Hlt1','Members'))
    return [ _getProperty(table,i.split('/')[-1],'DecisionName') for i in lines ]


def _sortReleases( release ):
    version = release.split('_')[ 1 ]
    m = re.match('v(\d+)r(\d+)(?:p(\d+))?', version)
    if not m:
        # sort special (nightly) versions lexicographically
        return (999, version)
    return tuple(int(x) if x else 0 for x in m.groups())

def _sortConfigs( config ):
    return config[ 'TCK' ] if config[ 'TCK'] else []

def printConfigurations( info ) :
    for release in sorted(set( [ i['release'] for i in info.itervalues()  ] ), key = _sortReleases ) :
        print release
        confInRelease = [ i for i in info.itervalues() if i['release']==release ]
        for hlttype in sorted(set( [ i['hlttype'] for i in confInRelease ] ) ) :
            confInHltType = sorted( [ i for i in confInRelease if i['hlttype']==hlttype ], key = _sortConfigs )
            print '    ' + hlttype
            [ i.printSimple('      ') for i in confInHltType ]

def dumpForPVSS( info, root ) :
    if not os.access(root,os.F_OK) : os.makedirs(root)
    for release in sorted(set( [ i['release'] for i in info.itervalues()  ] ) ) :
        f=open( root + '/' + release,  'w')
        [ f.write( i.PVSS() ) for i in info.itervalues() if i['release']==release ]
        f.close()

def printReleases( rel ) : pprint(rel)
def printHltTypes( rt ) : pprint(rt)
def printTCKs( tcks ) : pprint(tcks)

def listConfigurations(cas=default_cas):
    printConfigurations(getConfigurations(cas=cas))
def listReleases(cas=default_cas):
    printReleases(getReleases(cas=cas))
def listHltTypes(release, cas=default_cas):
    printHltTypes(getHltTypes(release, cas=cas))
def listTCKs(release, hlttype, cas=default_cas):
    printTCKs(getTCKs(release, hlttype, cas=cas))


@accept_id
def listRoutingBits(tree):
    pprint(getRoutingBits(tree))


@accept_id
def listHlt1Lines(tree):
    pprint(getHlt1Lines(tree))


@accept_id
def listHlt2Lines(tree):
    pprint(getHlt2Lines(tree))


@accept_id
def listHlt1Decisions(tree):
    pprint(getHlt1Decisions(tree))


@accept_id
def isTurboLine(tree, line):
    """Check if a given line is a Turbo line.

    Args:
        id (str/int): Configuration id or TCK.
        line (str): Line name or type/name.

    """
    linetree = flow.getLineTree(tree, line)
    return eval(linetree.leaf.properties()['Turbo'])


@accept_id
def isTurboPPLine(tree, line):
    """Check if a given line is a Turbo++ (persist reco) line.

    Args:
        id (str/int): Configuration id or TCK.
        line (str): Line name or type/name.

    """
    # In 2017 the list of Turbo++ lines is a property of a service
    try:
        lines = eval(getProperty(tree, 'HltLinePersistenceSvc', 'TurboPPLines'))
        return (line + 'Decision') in lines
    except KeyError:
        pass
    # In 2016 we can only find it out from an HDRFilter
    linetree = flow.getLineTree(tree, line)
    try:
        hdrfilter = trees.findInTree(tree, name=r'^Hlt2PersistRecoLineFilter$',
                                     type=r'^LoKi::HDRFilter$')
    except KeyError:
        return False
    functor = helpers.functorFromHybridFilter(hdrfilter.leaf, tree)
    matches = list(flow.applyHDRFunctor([linetree], functor))
    return bool(matches)


isPersistRecoLine = isTurboPPLine
