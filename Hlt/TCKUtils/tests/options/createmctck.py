import os, errno
from TCKUtils.utils import (default_cas, ConfigCDBAccessSvc, ConfigStackAccessSvc,
                            updateProperties, createTCKEntries, diff)
from TCKUtils.createMCversion import createMCversion

try:
    os.remove('createmctck.cdb')
except OSError as e:
    if e.errno != errno.ENOENT:
        raise
cas_rw = ConfigCDBAccessSvc('WriteConfigAccessSvc', File='createmctck.cdb', Mode='ReadWrite')
cas = ConfigStackAccessSvc('StackConfigAccessSvc', ConfigAccessSvcs=[cas_rw, default_cas])

createMCversion(0x21611709, newtck=0x61661709, cas=cas)
