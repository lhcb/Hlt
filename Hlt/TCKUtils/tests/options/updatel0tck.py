import os, errno
from TCKUtils.utils import (default_cas, ConfigCDBAccessSvc, ConfigStackAccessSvc,
                            updateL0TCK, createTCKEntries, diff)

try:
    os.remove('updatel0tck.cdb')
except OSError as e:
    if e.errno != errno.ENOENT:
        raise
cas_rw = ConfigCDBAccessSvc('WriteConfigAccessSvc', File='updatel0tck.cdb', Mode='ReadWrite')
cas = ConfigStackAccessSvc('StackConfigAccessSvc', ConfigAccessSvcs=[cas_rw, default_cas])

new_id = updateL0TCK(0x212c1605, 0x160f, label='An updated TCK.', cas=cas)
new_tck = 0x212c160f
createTCKEntries({new_tck: new_id}, cas=cas)

diff(0x212c1605, 0x212c160f, human=True, cas=cas)
