import os, errno
from TCKUtils.utils import (default_cas, ConfigCDBAccessSvc, ConfigStackAccessSvc,
                            updateProperties, createTCKEntries, diff)

try:
    os.remove('updateproperties.cdb')
except OSError as e:
    if e.errno != errno.ENOENT:
        raise
cas_rw = ConfigCDBAccessSvc('WriteConfigAccessSvc', File='updateproperties.cdb', Mode='ReadWrite')
cas = ConfigStackAccessSvc('StackConfigAccessSvc', ConfigAccessSvcs=[cas_rw, default_cas])

updates = {
    'Hlt2B2HH_B2HHPreScaler': {'AcceptFraction': '0.01'},
    'Hlt2BeamGasPreScaler': {'AcceptFraction': 0.01},  # repr(0.01) is called
}

new_id = updateProperties(0x212c1605, updates, label='An updated TCK.', cas=cas)
new_tck = 0x2fff1605
createTCKEntries({new_tck: new_id}, cas=cas)

diff(0x212c1605, 0x2fff1605, human=True, cas=cas)
