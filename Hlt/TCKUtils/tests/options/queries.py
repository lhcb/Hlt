import ast
import os
import unittest
import itertools
from TCKUtils.utils import *


def reference(filename):
    with open(os.path.join('../refs', filename)) as f:
        s = f.read()
    return ast.literal_eval(s)


class TestGenericMethods(unittest.TestCase):
    TCK = 0x011400a2  # pre-split TCK (2015 and earlier)
    TCK1 = 0x11291600
    TCK2 = 0x21291600

    def test_getComponents(self):
        expected = [
            (7, 'Hlt1ForwardHPT', 'b7aab5b0114f4dc8378148c6148ef46a'),
            (8, 'Hlt1ForwardHPT.PatForwardTool', 'a9df2f268ba78325058b7184aa3a43c3'),
            (9, 'Hlt1ForwardHPT.PatForwardTool.PatFwdTool', '3d9ba410de73d72172ba775099b28df1')
        ]
        result = getComponents(self.TCK1)
        result = list(itertools.islice(result, 298, 301))
        self.assertEqual(expected, result)

    def test_getAlgorithms(self):
        expected = [
            (1, 'Hlt', 'c87b1cfc29bb0f766a03a346c15af9c6'),
            (2, 'HltDecisionSequence', '70aab61e02f643b518a0cb5cde49296f'),
            (3, 'Hlt1', 'bf20bdf0ddee88643841d87002eaed30'),
            (4, 'Hlt1TrackMVA', 'f3a75204403fa8914f6243ec3d19eeca')
        ]
        result = getAlgorithms(self.TCK1)
        result = list(itertools.islice(result, 0, 4))
        self.assertEqual(expected, result)

    def test_getProperties(self):
        expected = {'PatPV3DHltBeamGas.PVOfflineTool':
                    {'BeamSpotRCut': '0.20000000'}}
        result = getProperties(self.TCK1, '.*PatPV3D.*', 'BeamSpotRCut', r'^0\.2.*')
        self.assertEqual(expected, result)

    def test_getTCKInfo(self):
        expected = ('Physics_pp_May2016', 'MOORE_v25r2', 'Hlt1, Physics_pp_May2016, 0x1600')
        result = getTCKInfo(self.TCK1)
        self.assertEqual(expected, result)

    def test_getReleases(self):
        releases = getReleases()
        self.assertTrue('MOORE_v25r2' in releases)

    def test_getTCKs(self):
        expected = ('0x{:08x}'.format(self.TCK1), 'Hlt1, Physics_pp_May2016, 0x1600')
        tcks = getTCKs()
        tcks_subset = getTCKs(release='MOORE_v25r2', hlttype='Physics_pp_May2016')
        self.assertTrue(expected in tcks)
        self.assertTrue(expected in tcks_subset)

    def test_getTCKList(self):
        tcks = getTCKList()
        self.assertTrue('0x{:08x}'.format(self.TCK1) in tcks)

    def test_getHlt1Lines(self):
        expected = ['Hlt1TrackMVA',
                    'Hlt1TwoTrackMVA',
                    'Hlt1TrackMVALoose']
        result = getHlt1Lines(self.TCK1)
        self.assertEqual(expected, result[:len(expected)])
        self.assertEqual([], getHlt1Lines(self.TCK2))

    def test_getHlt2Lines(self):
        expected = ['Hlt2B2HH_B2HH',
                    'Hlt2B2HH_B2KK',
                    'Hlt2B2HH_B2KPi']
        result = getHlt2Lines(self.TCK2)
        self.assertEqual(expected, result[:len(expected)])
        self.assertEqual([], getHlt2Lines(self.TCK1))

    def test_getHlt1Decisions(self):
        expected0 = ['Hlt1TrackMVADecision',
                     'Hlt1TwoTrackMVADecision',
                     'Hlt1TrackMVALooseDecision']
        expected1 = 'Hlt1Global'
        result = getHlt1Decisions(self.TCK1)
        self.assertEqual(expected0, result[:len(expected0)])
        self.assertEqual(expected1, result[-1])

    def test_isTurboLine(self):
        self.assertTrue(isTurboLine(self.TCK2, 'Hlt2DiMuonJPsiTurbo'))
        self.assertFalse(isTurboLine(self.TCK2, 'Hlt2DiMuonJPsi'))

    def test_isTurboPPLine(self):
        TCK2_2016 = 0x21291600
        TCK2_2017 = 0x21611709
        self.assertTrue(isTurboPPLine(TCK2_2016, 'Hlt2DiMuonJPsiTurbo'))
        self.assertFalse(isTurboPPLine(TCK2_2016, 'Hlt2DiMuonJPsi'))
        self.assertFalse(isTurboPPLine(TCK2_2016, 'Hlt2BottomoniumDiKstarTurbo'))
        self.assertTrue(isTurboPPLine(TCK2_2017, 'Hlt2DiMuonJPsiTurbo'))
        self.assertFalse(isTurboPPLine(TCK2_2017, 'Hlt2DiMuonJPsi'))
        self.assertFalse(isTurboPPLine(TCK2_2017, 'Hlt2BottomoniumDiKstarTurbo'))

    def test_getRoutingBits(self):
        self.assertDictEqual(reference('rbTCK.txt'), getRoutingBits(self.TCK))
        self.assertDictEqual(reference('rbTCK1.txt'), getRoutingBits(self.TCK1))
        self.assertDictEqual(reference('rbTCK2.txt'), getRoutingBits(self.TCK2))


class TestL0Methods(unittest.TestCase):
    TCK1 = 0x1138160f

    def test_getL0Config(self):
        l0 = getL0Config(self.TCK1)
        self.assertEqual('0x160F', l0['TCK'])
        expected_muon = {'MASK': '001',
                         'conditions': ['Muon1(Pt)>36', 'Spd_Had(Mult)<450'],
                         'name': 'Muon',
                         'rate': '100'}
        self.assertDictEqual(expected_muon, l0['Channels']['Muon'])
        expected_cond = {'bx': '-1',
                         'comparator': '<',
                         'data': 'Sum(Et)',
                         'name': 'SumEtPrev<250',
                         'threshold': '250'}
        self.assertDictEqual(expected_cond, l0['Conditions']['SumEtPrev<250'])

    def test_getL0Channels(self):
        expected = [
            'B1gas', 'B2gas', 'CALO', 'DiEM,lowMult', 'DiHadron,lowMult',
            'DiMuon', 'DiMuon,lowMult', 'Electron', 'Electron,lowMult',
            'Hadron', 'JetEl', 'JetPh', 'Muon', 'Muon,lowMult', 'MuonEW',
            'Photon', 'Photon,lowMult'
        ]
        result = getL0Channels(self.TCK1)
        self.assertListEqual(expected, result)

    def test_getL0Prescales(self):
        prescales = getL0Prescales(self.TCK1)
        self.assertEqual('100', prescales['0x160F']['Muon'])


if __name__ == '__main__':
    unittest.main()
