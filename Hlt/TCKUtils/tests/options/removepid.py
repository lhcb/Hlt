import os
import errno
import sys
from TCKUtils.utils import (default_cas, ConfigCDBAccessSvc,
                            ConfigStackAccessSvc, diff)
from TCKUtils.removePID import removePID, testedTCKs

try:
    os.remove('removepid.cdb')
except OSError as e:
    if e.errno != errno.ENOENT:
        raise
cas_rw = ConfigCDBAccessSvc(
    'WriteConfigAccessSvc', File='removepid.cdb', Mode='ReadWrite')
cas = ConfigStackAccessSvc(
    'StackConfigAccessSvc', ConfigAccessSvcs=[cas_rw, default_cas])

for tck in testedTCKs:
    newtck = (tck | 0x08000000) + 0x000A0000  # avoid collisions with released TCKs
    newtck = removePID(tck, newtck=newtck, cas=cas)
    diff(tck, newtck, cas=cas)
