********************************* TCK = 0x1600 *********************************
*********************************** Channels ***********************************
B1gas: {'MASK': '010', 'rate': '100', 'conditions': ['PU(Mult)<30', 'SumEt>208'], 'name': 'B1gas'}
B2gas: {'MASK': '100', 'rate': '100', 'conditions': ['PU(Mult)>9', 'SumEt<167'], 'name': 'B2gas'}
CALO: {'MASK': '000', 'rate': '100', 'conditions': ['Hadron(Et)>10', 'Spd(Mult)>2'], 'name': 'CALO'}
DiEM,lowMult: {'MASK': '001', 'rate': '50', 'conditions': ['Electron(Et)>20', 'Photon(Et)>20', 'Spd(Mult)<20'], 'name': 'DiEM,lowMult'}
DiHadron,lowMult: {'MASK': '001', 'rate': '15', 'conditions': ['Hadron(Et)>17', 'PU(Mult)<2', 'Spd(Mult)<20'], 'name': 'DiHadron,lowMult'}
DiMuon: {'MASK': '001', 'rate': '100', 'conditions': ['Muon12(Pt)>324', 'Spd_DiMu(Mult)<900'], 'name': 'DiMuon'}
DiMuon,lowMult: {'MASK': '001', 'rate': '100', 'conditions': ['Muon1(Pt)>2', 'Muon2(Pt)>2', 'Spd(Mult)<20'], 'name': 'DiMuon,lowMult'}
Electron: {'MASK': '001', 'rate': '100', 'conditions': ['Electron(Et)>78', 'Spd_Had(Mult)<450'], 'name': 'Electron'}
Electron,lowMult: {'MASK': '001', 'rate': '100', 'conditions': ['Electron(Et)>50', 'Spd(Mult)<20'], 'name': 'Electron,lowMult'}
Hadron: {'MASK': '001', 'rate': '100', 'conditions': ['Hadron(Et)>122', 'Spd_Had(Mult)<450'], 'name': 'Hadron'}
JetEl: {'MASK': '001', 'rate': '100', 'conditions': ['Electron(Et)>254', 'Hadron(Et)>254', 'Spd_Jet(Mult)<10000', 'SumEt>1458'], 'name': 'JetEl'}
JetPh: {'MASK': '001', 'rate': '100', 'conditions': ['Hadron(Et)>254', 'Photon(Et)>254', 'Spd_Jet(Mult)<10000', 'SumEt>1458'], 'name': 'JetPh'}
Muon: {'MASK': '001', 'rate': '100', 'conditions': ['Muon1(Pt)>14', 'Spd_Had(Mult)<450'], 'name': 'Muon'}
Muon,lowMult: {'MASK': '001', 'rate': '100', 'conditions': ['Muon1(Pt)>8', 'Spd(Mult)<20'], 'name': 'Muon,lowMult'}
MuonEW: {'MASK': '001', 'rate': '100', 'conditions': ['Muon1(Pt)>120', 'Spd_Jet(Mult)<10000'], 'name': 'MuonEW'}
MuonNoSPD: {'MASK': '000', 'rate': '100', 'conditions': ['Muon1(Pt)>56'], 'name': 'MuonNoSPD'}
NoPVFlag: {'MASK': '000', 'rate': '100', 'conditions': ['PUPeak1=0', 'PUPeak2=0'], 'name': 'NoPVFlag'}
Photon: {'MASK': '001', 'rate': '100', 'conditions': ['Photon(Et)>88', 'Spd_Had(Mult)<450'], 'name': 'Photon'}
Photon,lowMult: {'MASK': '001', 'rate': '100', 'conditions': ['Photon(Et)>50', 'Spd(Mult)<20'], 'name': 'Photon,lowMult'}
********************************** Conditions **********************************
Electron(Et)>20: {'threshold': '20', 'data': 'Electron(Et)', 'name': 'Electron(Et)>20', 'comparator': '>'}
Electron(Et)>254: {'threshold': '254', 'data': 'Electron(Et)', 'name': 'Electron(Et)>254', 'comparator': '>'}
Electron(Et)>50: {'threshold': '50', 'data': 'Electron(Et)', 'name': 'Electron(Et)>50', 'comparator': '>'}
Electron(Et)>78: {'threshold': '78', 'data': 'Electron(Et)', 'name': 'Electron(Et)>78', 'comparator': '>'}
Hadron(Et)>10: {'threshold': '10', 'data': 'Hadron(Et)', 'name': 'Hadron(Et)>10', 'comparator': '>'}
Hadron(Et)>122: {'threshold': '122', 'data': 'Hadron(Et)', 'name': 'Hadron(Et)>122', 'comparator': '>'}
Hadron(Et)>17: {'threshold': '17', 'data': 'Hadron(Et)', 'name': 'Hadron(Et)>17', 'comparator': '>'}
Hadron(Et)>254: {'threshold': '254', 'data': 'Hadron(Et)', 'name': 'Hadron(Et)>254', 'comparator': '>'}
Muon1(Pt)>120: {'threshold': '120', 'data': 'Muon1(Pt)', 'name': 'Muon1(Pt)>120', 'comparator': '>'}
Muon1(Pt)>14: {'threshold': '14', 'data': 'Muon1(Pt)', 'name': 'Muon1(Pt)>14', 'comparator': '>'}
Muon1(Pt)>2: {'threshold': '2', 'data': 'Muon1(Pt)', 'name': 'Muon1(Pt)>2', 'comparator': '>'}
Muon1(Pt)>56: {'threshold': '56', 'data': 'Muon1(Pt)', 'name': 'Muon1(Pt)>56', 'comparator': '>'}
Muon1(Pt)>8: {'threshold': '8', 'data': 'Muon1(Pt)', 'name': 'Muon1(Pt)>8', 'comparator': '>'}
Muon12(Pt)>324: {'threshold': '324', 'data': 'DiMuonProd(Pt1Pt2)', 'name': 'Muon12(Pt)>324', 'comparator': '>'}
Muon2(Pt)>2: {'threshold': '2', 'data': 'Muon2(Pt)', 'name': 'Muon2(Pt)>2', 'comparator': '>'}
PU(Mult)<2: {'threshold': '2', 'data': 'PUHits(Mult)', 'name': 'PU(Mult)<2', 'comparator': '<'}
PU(Mult)<30: {'threshold': '30', 'data': 'PUHits(Mult)', 'name': 'PU(Mult)<30', 'comparator': '<'}
PU(Mult)>9: {'threshold': '9', 'data': 'PUHits(Mult)', 'name': 'PU(Mult)>9', 'comparator': '>'}
PUPeak1=0: {'threshold': '0', 'data': 'PUPeak1(Cont)', 'name': 'PUPeak1=0', 'comparator': '=='}
PUPeak2=0: {'threshold': '0', 'data': 'PUPeak2(Cont)', 'name': 'PUPeak2=0', 'comparator': '=='}
Photon(Et)>20: {'threshold': '20', 'data': 'Photon(Et)', 'name': 'Photon(Et)>20', 'comparator': '>'}
Photon(Et)>254: {'threshold': '254', 'data': 'Photon(Et)', 'name': 'Photon(Et)>254', 'comparator': '>'}
Photon(Et)>50: {'threshold': '50', 'data': 'Photon(Et)', 'name': 'Photon(Et)>50', 'comparator': '>'}
Photon(Et)>88: {'threshold': '88', 'data': 'Photon(Et)', 'name': 'Photon(Et)>88', 'comparator': '>'}
Spd(Mult)<20: {'threshold': '20', 'data': 'Spd(Mult)', 'name': 'Spd(Mult)<20', 'comparator': '<'}
Spd(Mult)>2: {'threshold': '2', 'data': 'Spd(Mult)', 'name': 'Spd(Mult)>2', 'comparator': '>'}
Spd_DiMu(Mult)<900: {'threshold': '900', 'data': 'Spd(Mult)', 'name': 'Spd_DiMu(Mult)<900', 'comparator': '<'}
Spd_Had(Mult)<450: {'threshold': '450', 'data': 'Spd(Mult)', 'name': 'Spd_Had(Mult)<450', 'comparator': '<'}
Spd_Jet(Mult)<10000: {'threshold': '10000', 'data': 'Spd(Mult)', 'name': 'Spd_Jet(Mult)<10000', 'comparator': '<'}
SumEt<167: {'threshold': '167', 'data': 'Sum(Et)', 'name': 'SumEt<167', 'comparator': '<'}
SumEt>1458: {'threshold': '1458', 'data': 'Sum(Et)', 'name': 'SumEt>1458', 'comparator': '>'}
SumEt>208: {'threshold': '208', 'data': 'Sum(Et)', 'name': 'SumEt>208', 'comparator': '>'}
********************************************************************************
