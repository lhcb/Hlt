Hlt1B2GammaGamma
   L0DU (L0_CHANNEL('Photon') | L0_CHANNEL('Electron')):
      Electron
      Photon
Hlt1B2HH_LTUNB_KK
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1B2HH_LTUNB_KPi
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1B2HH_LTUNB_PiPi
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1B2PhiGamma_LTUNB
   L0DU (L0_CHANNEL('Photon')|L0_CHANNEL('Electron')):
      Electron
      Photon
Hlt1B2PhiPhi_LTUNB
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1BeamGasBeam1
   ODIN ((ODIN_BXTYP == LHCb.ODIN.Beam1)):
      Beam1Gas:be:
   L0DU (L0_CHANNEL('B1gas')):
      B1gas
Hlt1BeamGasBeam2
   ODIN ((ODIN_BXTYP == LHCb.ODIN.Beam2)):
      Beam2Gas:eb:
   L0DU (L0_CHANNEL('B2gas')):
      B2gas
Hlt1BeamGasHighRhoVertices
   ODIN ((scale((ODIN_BXTYP == LHCb.ODIN.BeamCrossing), SCALE(0.)) | scale(((ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2)), SCALE(1.0)))):
      Beam1Gas:be,eb:
      Beam2Gas:be,eb:
      Physics:be,eb:
   L0DU ((L0_DATA('Spd(Mult)') > 5) | (L0_DATA('PUHits(Mult)') > 5)):
      B1gas
      B2gas
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1Bottomonium2KstarKstar
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1Bottomonium2PhiPhi
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibHighPTLowMultTrks
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibMuonAlignJpsi
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')):
      DiMuon
      Muon
Hlt1CalibRICHMirrorRICH1
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibRICHMirrorRICH2
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibTrackingKK
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibTrackingKPi
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibTrackingKPiDetached
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1CalibTrackingPiPi
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1DiMuonHighMass
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')):
      DiMuon
      Muon
Hlt1DiMuonLowMass
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')):
      DiMuon
      Muon
Hlt1DiMuonNoIP
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')):
      DiMuon
      Muon
Hlt1DiMuonNoL0
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1DiProton
   L0DU (L0_CHANNEL( 'Hadron' ) & ( L0_DATA('Spd(Mult)') < 300.0 )):
      Hadron
Hlt1DiProtonLowMult
   L0DU (( L0_DATA('Spd(Mult)') < 10.0 )):
      B1gas
      B2gas
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1ErrorEvent
Hlt1Global
Hlt1IncPhi
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1L0Any
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1L0AnyNoSPD
   L0DU (L0_CHANNEL_RE('.*NoSPD')):
Hlt1LowMult
   L0DU (( ( L0_DATA('Spd(Mult)') < 100.0 ) & ( ( L0_CHANNEL_RE('.*lowMult') & ~(L0_CHANNEL_RE('Photon,lowMult')) ) ) )):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon,lowMult
      Electron,lowMult
      Muon,lowMult
Hlt1LowMultMaxVeloCut
   L0DU (( ( L0_DATA('Spd(Mult)') < 100.0 ) & ( ( L0_CHANNEL_RE('Photon,lowMult') | L0_CHANNEL_RE('DiEM,lowMult') ) ) )):
      DiEM,lowMult
      Photon,lowMult
Hlt1LowMultPassThrough
   L0DU (( L0_CHANNEL_RE('.*lowMult') )):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon,lowMult
      Electron,lowMult
      Muon,lowMult
      Photon,lowMult
Hlt1LowMultVeloCut_Hadrons
   L0DU (( ( L0_DATA('Spd(Mult)') < 20 ) & ( ( L0_CHANNEL_RE('DiHadron,lowMult') ) ) )):
      DiHadron,lowMult
Hlt1LowMultVeloCut_Leptons
   L0DU (( ( L0_DATA('Spd(Mult)') < 100.0 ) & ( ( L0_CHANNEL_RE('Muon,lowMult') | L0_CHANNEL_RE('DiMuon,lowMult') | L0_CHANNEL_RE('Electron,lowMult') ) ) )):
      DiMuon,lowMult
      Electron,lowMult
      Muon,lowMult
Hlt1Lumi
   ODIN (ODIN_PASS(LHCb.ODIN.Lumi)):
      Lumi:ee,be,eb,bb:
Hlt1MBNoBias
   ODIN (ODIN_PASS(LHCb.ODIN.NoBias)):
      NoBias:ee,be,eb,bb:
Hlt1MultiDiMuonNoIP
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')):
      DiMuon
      Muon
Hlt1MultiMuonNoL0
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1NoBiasNonBeamBeam
   ODIN (ODIN_PASS(LHCb.ODIN.Lumi) & ~(ODIN_BXTYP == LHCb.ODIN.BeamCrossing)):
      Lumi:ee,be,eb:
Hlt1ODINTechnical
   ODIN (( ODIN_TRGTYP == LHCb.ODIN.TechnicalTrigger )):
Hlt1SingleElectronNoIP
   L0DU (L0_CHANNEL('Electron')):
      Electron
Hlt1SingleMuonHighPT
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('MuonEW')):
      Muon
      MuonEW
Hlt1SingleMuonNoIP
   L0DU (L0_CHANNEL('Muon')):
      Muon
Hlt1Tell1Error
Hlt1TrackMVA
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1TrackMVALoose
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1TrackMuon
   L0DU (L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')):
      DiMuon
      Muon
Hlt1TrackMuonNoSPD
   L0DU (L0_CHANNEL('MuonNoSPD')):
Hlt1TwoTrackMVA
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1TwoTrackMVALoose
   L0DU (L0_DECISION_PHYSICS):
      DiEM,lowMult
      DiHadron,lowMult
      DiMuon
      DiMuon,lowMult
      Electron
      Electron,lowMult
      Hadron
      JetEl
      JetPh
      Muon
      Muon,lowMult
      MuonEW
      Photon
      Photon,lowMult
Hlt1VeloClosingMicroBias
   ODIN (scale(ODIN_PASS(LHCb.ODIN.VeloOpen), RATE(10000))):
      Beam1Gas:ee,be,eb,bb:VeloOpen
      Beam2Gas:ee,be,eb,bb:VeloOpen
      Lumi:ee,be,eb,bb:VeloOpen
      NoBias:ee,be,eb,bb:VeloOpen
      Physics:ee,be,eb,bb:VeloOpen
      SequencerTrigger:ee,be,eb,bb:VeloOpen
RB_33 
   HLT1 (HLT_PASS_RE('^Hlt1Lumi.*Decision$')):
      Hlt1Lumi
RB_35 
   HLT1 (HLT_PASS_RE('Hlt1BeamGas.*Decision')):
      Hlt1BeamGasBeam1
      Hlt1BeamGasBeam2
      Hlt1BeamGasHighRhoVertices
RB_40 VELOCLOSING
   HLT1 (HLT_PASS_RE('Hlt1(Velo|BeamGas).*Decision')):
      Hlt1BeamGasBeam1
      Hlt1BeamGasBeam2
      Hlt1BeamGasHighRhoVertices
      Hlt1VeloClosingMicroBias
RB_46 
   HLT1 (HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision')):
      Hlt1B2GammaGamma
      Hlt1B2HH_LTUNB_KK
      Hlt1B2HH_LTUNB_KPi
      Hlt1B2HH_LTUNB_PiPi
      Hlt1B2PhiGamma_LTUNB
      Hlt1B2PhiPhi_LTUNB
      Hlt1Bottomonium2KstarKstar
      Hlt1Bottomonium2PhiPhi
      Hlt1CalibHighPTLowMultTrks
      Hlt1CalibMuonAlignJpsi
      Hlt1CalibRICHMirrorRICH1
      Hlt1CalibRICHMirrorRICH2
      Hlt1CalibTrackingKK
      Hlt1CalibTrackingKPi
      Hlt1CalibTrackingKPiDetached
      Hlt1CalibTrackingPiPi
      Hlt1DiMuonHighMass
      Hlt1DiMuonLowMass
      Hlt1DiMuonNoIP
      Hlt1DiMuonNoL0
      Hlt1DiProton
      Hlt1DiProtonLowMult
      Hlt1ErrorEvent
      Hlt1IncPhi
      Hlt1LowMult
      Hlt1LowMultMaxVeloCut
      Hlt1LowMultPassThrough
      Hlt1LowMultVeloCut_Hadrons
      Hlt1LowMultVeloCut_Leptons
      Hlt1MultiDiMuonNoIP
      Hlt1MultiMuonNoL0
      Hlt1NoBiasNonBeamBeam
      Hlt1SingleElectronNoIP
      Hlt1SingleMuonHighPT
      Hlt1SingleMuonNoIP
      Hlt1TrackMVA
      Hlt1TrackMVALoose
      Hlt1TrackMuon
      Hlt1TrackMuonNoSPD
      Hlt1TwoTrackMVA
      Hlt1TwoTrackMVALoose
RB_48 HLT1NOBIAS
   HLT1 (HLT_PASS('Hlt1MBNoBiasDecision')):
      Hlt1MBNoBias
RB_53 
   HLT1 (HLT_PASS_RE('Hlt1Calib(TrackingKPiDetached|HighPTLowMultTrks)Decision')):
      Hlt1CalibHighPTLowMultTrks
      Hlt1CalibTrackingKPiDetached
RB_54 
   HLT1 (HLT_PASS_RE('Hlt1CalibRICH.*Decision')):
      Hlt1CalibRICHMirrorRICH1
      Hlt1CalibRICHMirrorRICH2
RB_56 
   HLT1 (HLT_PASS('Hlt1CalibMuonAlignJpsiDecision')):
      Hlt1CalibMuonAlignJpsi
RB_57 
   HLT1 (HLT_PASS('Hlt1Tell1ErrorDecision')):
      Hlt1Tell1Error
