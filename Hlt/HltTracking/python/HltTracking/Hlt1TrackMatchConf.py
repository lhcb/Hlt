__all__ = ('FilterVeloL0Calo', 'MatchVeloL0CaloCands', 'FilterTrackL0Calo', 'CandsTrackL0Calo')

# Configure the matching tool.
from HltLine.HltLine import Hlt1Tool
from Hlt1TrackNames import Hlt1Tools
from Configurables import Hlt__MatchTrackL0Calo, ToolSvc
Hlt1Tool(Hlt__MatchTrackL0Calo, Hlt1Tools["TrackL0Calo"].split('/')[1],
         MaxChi2 = 4, MinEP = 0.6, MaxEP = 1.2).createConfigurable(ToolSvc())

# Define the matching filter streamer.
def FilterTrackL0Calo(selection):
    return ("FilterTrackL0Calo = TC_MATCHFLTR( '%s', "
            "HltTracking.Hlt1StreamerConf.TrackL0Calo )" % selection)

# Define the candidate matching streamer.
def CandsTrackL0Calo(selection):
    return ("CandsTrackL0Calo = TC_MATCHCANDS( '%s', "
            "HltTracking.Hlt1StreamerConf.TrackL0Calo )" % selection)

# Legacy code below that uses the old VELO-track matching. Should be removed.
# =============================================================================
## Symbols for streamer users
# =============================================================================
import HltLine.HltDecodeRaw
from Configurables import Hlt__MatchVeloL0Calo, ToolSvc
from HltLine.HltLine import Hlt1Tool

## Helper functions
from Hlt1TrackNames import Hlt1Tools
def to_name( key ):
    return Hlt1Tools[key]

def ConfiguredVeloL0Calo( parent, name = None, chi2 = 9 ):
    if name == None: name = Hlt__MatchVeloL0Calo.__name__
    return Hlt1Tool( Hlt__MatchVeloL0Calo
                     , name
                     , MaxMatchChi2 = chi2 ).createConfigurable( parent )

## Configure tool, set the match chi2
ConfiguredVeloL0Calo( ToolSvc(), to_name( "VeloL0Calo" ), 9 )

## Streamer symbol
def FilterVeloL0Calo( selection ):
    return "FilterVeloL0Calo = TC_MATCHFLTR( '%s', HltTracking.Hlt1StreamerConf.VeloL0Calo )" \
           % selection

def MatchVeloL0CaloCands( selection ):
    return "MatchVeloL0CaloCands = TC_MATCHCANDS( '%s', HltTracking.Hlt1StreamerConf.VeloL0Calo  )" \
           % selection
