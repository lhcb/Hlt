#include "GaudiAlg/GaudiTool.h"
#include "Event/Track.h"
#include "Event/L0DUBase.h"
#include "Event/L0CaloCandidate.h"
#include "Event/HltCandidate.h"
#include "HltBase/ITrack2CandidateMatch.h"

namespace Hlt {
  /**
   * Class to match tracks with L0 calorimetry objects.
   *
   * Tracks are linearly extrapolated from their state closest in z to
   * the L0 calorimetry object, to the z of the object. The
   * chi-squared is calculated, where the x and y-uncertainty are
   * calculated from the L0 calorimeter object tolerance and the track
   * uncertainty (which is typically negligible). If the track is a
   * VELO track, or if the mode is forced, the track is extrapolated
   * from its first state to the z of the calorimeter object using the
   * pT-kick method as implemented originally in HltVeloEcalMatch.
   *
   * If the track and L0 object pass a maximum chi-squared
   * requirement, the two objects are matched. The chi-squared of the
   * match is written to extra info Track::AdditionalInfo::IsMuonTight
   * + 1. The matched calorimeter x, y, z, e, and e untertainty are
   * written to Track::AdditionalInfo::IsMuonTight + 2, 3, 4, 5, and 6
   * respectively.
   *
   * @class MatchTrackL0Calo
   *
   * @see LHCb::Track
   * @see LHCb::L0CaloCandidate
   * @see Hlt::Candidate
   * @see Hlt::ITrack2CandidateMatch
   *
   * @author Philip Ilten philten@cern.ch
   * @author Mike Williams mwill@mit.edu
   * @author Vanya Belyaev ivan.belyaev@cern.ch
   * @author Mariusz Witek mariusz.witek@cern.ch
   * @date 2018-02-20
   */
  class MatchTrackL0Calo : virtual public
  extends<GaudiTool, Hlt::ITrack2CandidateMatch> {

    /// Friend factory for instantiation.
    friend struct ToolFactory<Hlt::MatchTrackL0Calo>;

  public:

    /**
     * Match a track with an HLT candidate (TC_MATCHCANDS mode).
     *
     * @see Hlt::ITrack2CandidateMatch
     * @see ITrackMatch
     * @param track     (INPUT)  the input track
     * @param candidate (INPUT)  the input candidate
     * @param matched   (OUTPUT) the matched track
     * @param quality   (OUTPUT) the matching quality
     * @param quality2  (OUTPUT) the matching quality-2
     * @return status code
     */
    StatusCode match(const LHCb::Track &track, const Hlt::Candidate &candidate,
                     LHCb::Track &matched, double &quality,
                     double &quality2) const override;

    /**
     * Match a track with an HLT candidate (TC_MATCHFLTR mode).
     *
     * @see Hlt::ITrack2CandidateMatch
     * @param track    (INPUT)  the input track
     * @param candidat (INPUT)  the input candidate
     * @return true if track and candidate are matched
     */
    bool match(const LHCb::Track *track, const Hlt::Candidate *candidate,
               const double  quality, const double quality2) const override;

  protected:

    /**
     * Standard constructor.
     *
     * @param type   (INPUT) tool type
     * @param name   (INPUT) tool instance name
     * @param parent (INPUT) tool parent
     */
    MatchTrackL0Calo(const std::string &type, const std::string &name,
                     const IInterface *parent);

    /// Protected and virtual destructor.
    virtual ~MatchTrackL0Calo () {;}

  private:

    /// Disabled default constructor.
    MatchTrackL0Calo();

    /// Disabled copy constructor.
    MatchTrackL0Calo(const MatchTrackL0Calo &);

    /// Disabled assigment operator.
    MatchTrackL0Calo &operator=(const MatchTrackL0Calo &);

  private:

    // Properties.
    double m_maxChi2; ///< Maximum allowed chi-squared for a match.
    /// Minimum allowed calorimeter E over track p. If negative, no
    /// cut is applied.
    double m_minEP;
    /// Maximum allowed calorimeter E over track p. If negative, no
    /// cut is applied.
    double m_maxEP;
    double m_tol;     ///< ECAL tolerance to distance conversion.
    double m_velo;    ///< Flag to use only the old VELO pT-kick matching.
    double m_ptKick;  ///< Average pT-kick for VELO track (in MeV).
    double m_zKick;   ///< Position of z-kick for VELO track (in mm).
    double m_eRes0;   ///< ECAL constant resolution term.
    double m_eRes1;   ///< ECAL stochastic resolution term.
  };
}

//=============================================================================
// Standard constructor.
//=============================================================================
Hlt::MatchTrackL0Calo::MatchTrackL0Calo
(const std::string &type, const std::string &name, const IInterface * parent)
  : base_class (type , name , parent) {
  declareProperty("MaxChi2", m_maxChi2 = 4,
                  "Maximum matched chi-squared.");
  declareProperty("MinEP", m_minEP = 0.6,
                  "Minimum allowed calorimeter E over track p. If negative,"
                  " no cut is applied.");
  declareProperty("MaxEP", m_maxEP = 1.2,
                  "Maximum allowed calorimeter E over track p. If negative,"
                  " no cut is applied.");
  declareProperty("Tol", m_tol = 4.0/sqrt(12.0),
                  "ECAL tolerance to distance conversion.");
  declareProperty("VELO", m_velo = false,
                  "Flag to use only the old VELO pT-kick matching.");
  declareProperty("PtKick", m_ptKick = 1263.0,
                  "Average pT-kick for VELO track (in MeV).");
  declareProperty("ZKick", m_zKick = 5250.0,
                  "Position of z-kick for VELO track (in mm).");
  declareProperty("ERes0", m_eRes0 = 0.6,
                  "ECAL constant resolution term.");
  declareProperty("ERes1", m_eRes1 = 22.136,
                  "ECAL stochastic resolution term (in sqrt(MeV)).");
}

//=============================================================================
// Match a track with an HLT candidate.
//=============================================================================
StatusCode Hlt::MatchTrackL0Calo::match
(const LHCb::Track &track, const Hlt::Candidate& candidate,
 LHCb::Track &matched, double &quality, double &quality2) const {

  // Get the L0 candidate and check it is valid.
  const LHCb::L0CaloCandidate* l0 = candidate.get<LHCb::L0CaloCandidate>();
  if (!l0) {return Error ("Candidate is not an L0 calorimeter object.");}
  if (l0->type() != L0DUBase::CaloType::Electron &&
      l0->type() != L0DUBase::CaloType::Photon) {
    return StatusCode::FAILURE;}
  double minChi2 = std::numeric_limits<double>::infinity();

  // Get the calorimeter position, position uncertainty, and energy.
  double x  = l0->position().x();
  double y  = l0->position().y();
  double z  = l0->position().z();
  double dx = l0->posTol()*m_tol;
  double dy = l0->posTol()*m_tol;
  double et = l0->et();
  double e  = fabs(et)*(sqrt(x*x + y*y + z*z)/sqrt(x*x + y*y));

  // Use the pT kick method for extrapolation.
  if (m_velo || track.type() == LHCb::Track::Types::Velo) {

    // Get the track slopes.
    double tx = track.firstState().tx();
    double ty = track.firstState().ty();

    // Determine the x-kick and update the x uncertainty.
    double xKick  = (z - m_zKick)*m_ptKick/e;
    double dxKick = xKick*(sqrt(m_eRes0*m_eRes0 + m_eRes1*m_eRes1/e));
    dx = sqrt(dx*dx + dxKick*dxKick);

    // Loop over positive and negative charge hypotheses.
    for (double q = -1; q <= 1; q += 2) {
      double difx = (x - z*tx + q*xKick)/dx;
      double dify = (y - z*ty)/dy;
      double chi2 = difx*difx + dify*dify;
      if (chi2 < minChi2) {minChi2 = chi2;}
    }

  // Use linear extrapolation from nearest state.
  } else {
    // Only cut on E/p if not saturated
    if (l0->etCode() < 254) {
      double ep = e/track.p();
      if (m_minEP >= 0 && ep < m_minEP) return StatusCode::FAILURE;
      if (m_maxEP >= 0 && ep > m_maxEP) return StatusCode::FAILURE;
    }
    const LHCb::State &state = track.closestState(z);
    const Gaudi::TrackSymMatrix &cov = state.covariance();
    double difz = z - state.z();
    dx = sqrt(dx*dx + cov(0, 0) + difz*difz*cov(2, 2) + 2*difz*cov(2, 0));
    dy = sqrt(dy*dy + cov(1, 1) + difz*difz*cov(3, 3) + 2*difz*cov(3, 1));
    double difx = (x - (state.x() + difz*state.tx()))/dx;
    double dify = (y - (state.y() + difz*state.ty()))/dy;
    double chi2 = difx*difx + dify*dify;
    if (chi2 < minChi2) {minChi2 = chi2;}
  }

  // Return.
  if (minChi2 < m_maxChi2) {
    matched.copy(track);
    // FIXME define and use dedicated HCb::Track::AdditionalInfo::L0CaloMatchXXX
    // matched.addInfo(401, minChi2);
    // matched.addInfo(402, x);
    // matched.addInfo(403, y);
    // matched.addInfo(404, z);
    // matched.addInfo(405, e);
    quality = sqrt(minChi2);
    quality2 = minChi2;
    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}

// ============================================================================
// Match a track with an HLT candidate (TC_MATCHFLTR mode).
// ============================================================================
bool Hlt::MatchTrackL0Calo::match
(const LHCb::Track *track, const Hlt::Candidate *candidate, const double,
 const double) const {
  if (!track) {
    Error("Null track, result is false.").ignore(); return false;}
  if (!candidate) {
    Error("Null candidate, result is false.").ignore(); return false;}
  if (!candidate->get<LHCb::L0CaloCandidate>()) {
    Error("Candidate is not an L0 calorimeter object.").ignore(); return false;}
  LHCb::Track matched;
  double quality1(0), quality2(0);
  return match(*track, *candidate, matched, quality1, quality2);
}

// ============================================================================
// Declare the factory.
// ============================================================================
DECLARE_NAMESPACE_TOOL_FACTORY(Hlt, MatchTrackL0Calo)
