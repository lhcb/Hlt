### Hack written by Roel Aaij.
### Contact names: Carlos Vazquez Sierra, Igor Kostiuk.

def static_vars(**kwargs):
    """ Add an attribute to the function that can be used as a static variable. """
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


@static_vars(contextSvc=None)
def input_source():
    from GaudiPython.Bindings import InterfaceCast
    from LoKiCore.basic import Gaudi, cpp
    from LoKiCore.functions import strings
    from LoKiPhys.functions import SOURCE, ALL
    if input_source.contextSvc is None:
        from GaudiPython.Bindings import AppMgr
        input_source.contextSvc = AppMgr().service('AlgContextSvc',
                                                   cpp.IAlgContextSvc)
    if not hasattr(Gaudi.Utils, 'getGaudiAlg'):
        cpp.gInterpreter.Declare('#include <GaudiAlg/GetAlgs.h>')
    alg = Gaudi.Utils.getGaudiAlg(input_source.contextSvc)
    prop = InterfaceCast(cpp.IProperty)(alg)
    inputs = prop.getProperty("Inputs").value()
    return SOURCE(strings(*inputs), ALL)
    