##
#  @author V. V. Gligorov vladimir.gligorov@cern.ch
#
#  Please contact the responsible before editing this file
#
##

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
class TrackEffElectronLines(Hlt2LinesConfigurableUser) :
    __slots__ = {'Prescale'             : {},
                 'TrackGEC'             : {'NTRACK_MAX'           : 100},
                 'SharedChild'          : {'TrChi2Mu'   :   5,  #these don't do anything at the moment
                                           'TrChi2Ele'  :   5,
                                           'TrChi2Kaon' :   5,
                                           'TrChi2Pion' :   5,
                                           'TrChi2Proton' :   5,
                                           'IPMu'       :   0.0 * mm,
                                           'IPEle'      :   0.0 * mm,
                                           'IPKaon'     :   0.0 * mm,
                                           'IPPion'     :   0.0 * mm,
                                           'IPProton'   :   0.0 * mm,
                                           'IPChi2Mu'   :   16,
                                           'IPChi2Ele'  :   16,
                                           'IPChi2Kaon' :   16,
                                           'IPChi2Pion' :   16,
                                           'IPChi2Proton' :   16,
                                           'EtaMinMu'   :   1.8,
                                           'EtaMinEle'  :   1.8,
                                           'EtaMaxMu'   :   4.5,
                                           'EtaMaxEle'  :   4.5,
                                           'ProbNNe'    :   0.5,
                                           'ProbNNmu'   :   0.5,    
                                           'ProbNNk'    :   0.5,
                                           'ProbNNpi'   :   0.8,
                                           'ProbNNp'    :   0.5,
                                           'PtMu'       :   2750 * MeV,
                                           'PtEle'      :   2400 * MeV,
                                           'PtKaon'     :   500 * MeV,
                                           'PtProton'   :   500 * MeV,
                                           'PtPion'     :   800 * MeV },
                 'DetachedPhi_ForEleTrackEff': {'MINFDZ': 2.5*mm,
                                                'MAXVCHI2P': 12.0,
                                                'MAXTRCHI2': 3.0,
                                                'MINIPCHI2': 6,
                                                'KMINPIDK': -1,
                                                'MINPT': 250*MeV},
                 'DetachedKstars_ForEleTrackEff': {'MINFDZ': 2.5*mm,
                                                'MAXVCHI2P': 12.0,
                                                'MAXTRCHI2': 3.0,
                                                'MINIPCHI2': 6,
                                                'MINPT': 250*MeV},
                 'DetachedProbeElectrons': {'IPprobe': 0.04*mm,
                                            'MAXTRCHI2probe': 5},
                 'DetachedProbeMuons': {'IPprobe': 0.08*mm,
                                            'MAXTRCHI2probe': 5},
                 'DetachedProbeProtons': {'IPprobe': 0.06*mm,
                                            'MAXTRCHI2probe': 6},
                 'DetachedProbeKaons': {'IPprobe': 0.03*mm,
                                            'MAXTRCHI2probe': 6},
                 'DetachedEPhi'           : {'AMMAX'         :   6000*MeV,
                                             'AMMIN'         :   1500*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedEKstar'           : {'AMMAX'         :   6000*MeV,
                                             'AMMIN'         :   1500*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedMuPhi'           : {'AMMAX'         :   6000*MeV,
                                             'AMMIN'         :   1500*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedMuKstar'           : {'AMMAX'         :   6000*MeV,
                                             'AMMIN'         :   1500*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedpPhi'           : {'AMMAX'         :   6000*MeV,
                                             'AMMIN'         :   1500*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'CORRMMIN'   :   4000*MeV,
                                           'CORRMMAX'   :   5700*MeV,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedpKstar'        : {'AMMAX'         :   6000*MeV,
                                             'AMMIN'         :   1500*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedEK'           : {'AM'         :   6000*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedEPi'          : {'AM'         :   2250*MeV,
                                           'VCHI2'      :   10, 
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedMuK'          : {'AM'         :   5500*MeV,
                                           'VCHI2'      :   10, 
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                          },
                 'DetachedMuPi'         : {'AM'         :   2100*MeV,
                                           'VCHI2'      :   10, 
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                          },
                 'DetachedMuMuK'        : {'AMTAP'      :   5800*MeV,
                                           'VCHI2TAP'   :   20,
                                           'MINCONSTRAINTDIRA'    :   0.999,
                                           'MINFDZ'     :   3.0*mm,
                                           'MLOW'       :   5000.*MeV,
                                           'MHIGH'      :   5900.*MeV,
                                           'CORRMMIN'   :   2000 * MeV,
                                           'CORRMMAX'   :   6750 * MeV,
                                           'PMINMUON'   :   2000 * MeV,
                                           'PMAXMUON'   :   100000*MeV,
                                           'PVMMIN'     :   4000*MeV,
                                           'PVMMAX'     :   6200*MeV,
                                           'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                          },
                 'DetachedEEK'          : {'AMTAP'      :   6000*MeV,
                                           'VCHI2TAP'   :   20,
                                           'MLOW'       :   4500.*MeV,
                                           'MHIGH'      :   6000.*MeV,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedEEPhi'          : {
                                             'AMTAP'      :   6000*MeV,
                                           'VCHI2TAP'   :   20,
                                           'MLOW'       :   4000.*MeV,
                                           'MHIGH'      :   6000.*MeV,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedEEKstar'          : {
                                           'AMTAP'      :   6000*MeV,
                                           'VCHI2TAP'   :   20,
                                           'MLOW'       :   4000.*MeV,
                                           'MHIGH'      :   6000.*MeV,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedMuMuPhi'          : {
                                            'AMTAP'      :   5800*MeV,
                                            'VCHI2TAP'   :   20,
                                            'MINFDZ'     :   2.5 * mm,
                                            'MLOW'       :   5000.*MeV,
                                            'MHIGH'      :   5900.*MeV,
                                            'MINCONSTRAINTDIRA'    :   0.999,
                                            'CORRMMIN'   :   2200 * MeV,
                                            'CORRMMAX'   :   6750 * MeV,
                                           'PMINMUON'   :   2000 * MeV,
                                           'PMAXMUON'   :   100000*MeV,
                                           'PVMMIN'  :   4000*MeV,
                                           'PVMMAX'  :   6200*MeV,
                                            'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                         },
                 'DetachedMuMuKstar'          : {
                                            'AMTAP'      :   5800*MeV,
                                           'VCHI2TAP'   :   20,
                                           'MINFDZ'     :   3.0*mm,
                                           'MINCONSTRAINTDIRA'    :   0.999,
                                           'MLOW'       :   5000.*MeV,
                                           'MHIGH'      :   5900.*MeV,
                                           'CORRMMIN'   :   2000 * MeV,
                                           'CORRMMAX'   :   6750 * MeV,
                                           'PMINMUON'   :   2200 * MeV,
                                           'PMAXMUON'   :   100000*MeV,
                                           'PVMMIN'  :   4000*MeV,
                                           'PVMMAX'  :   6200*MeV,
                                           'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                          },
                 'DetachedProtonProtonPhi'  : {
                                           'AMTAP'      :   6000*MeV,
                                           'VCHI2TAP'   :   15,
                                           'MLOW'       :   4000.*MeV,
                                           'MHIGH'      :   5700.*MeV,
                                           'PMINPROTON'       :   3000*MeV,
                                           'PMAXPROTON'       :   65000*MeV,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'L0Req'                : {'DetachedMuMuK': "L0_CHANNEL('Muon')",
                                           'DetachedMuMuPhi'  : "L0_CHANNEL('Muon')",
                                           'DetachedMuMuKstar'  : "L0_CHANNEL('Muon')",
                                           'DetachedEEK'  : "L0_CHANNEL('Electron')",
                                           'DetachedEEPhi'  : "L0_CHANNEL('Electron')|L0_CHANNEL('Muon')",
                                           'DetachedEEKstar'  : "L0_CHANNEL('Electron')|L0_CHANNEL('Muon')",
                                           'DetachedProtonProtonPhi'  : "L0_CHANNEL('Hadron')",
                                           'DetachedEK'   : "L0_CHANNEL('Electron')",
                                           'DetachedEPi'  : "L0_CHANNEL('Electron')",
                                           'DetachedMuK'  : "L0_CHANNEL('Muon')",
                                           'DetachedMuPi' : "L0_CHANNEL('Muon')" },
                 'Hlt1Req'              : {'DetachedMuMuK': "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                           'DetachedMuMuPhi'  : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                           'DetachedMuMuKstar'  : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                           'DetachedEEK'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedEEPhi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedEEKstar'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedProtonProtonPhi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedEK'   : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedEPi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedMuK'  : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                           'DetachedMuPi' : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')" }

                 }

    def stages(self, nickname = ""):
        if hasattr(self, '_stages') and self._stages:
            if nickname:
                return self._stages[nickname]
            else:
                return self._stages

        from Stages import (DetachedEKPair, DetachedEPiPair, DetachedMuKPair, DetachedMuPiPair)
        from Stages import (DetachedBtoEEK, DetachedBtoMuMuK)
        from Stages import (DetachedBtoEEPhi, DetachedBtoMuMuPhi)
        from Stages import (DetachedBtoMuMuKstar, DetachedBtoEEKstar)
        from Stages import (DetachedBtoProtonJpsiPhi)

        self._stages = {
                        'DetachedEK'       : [DetachedEKPair('DetachedEK')],
                        'DetachedEPi'      : [DetachedEPiPair('DetachedEPi')],
                        'DetachedMuK'      : [DetachedMuKPair('DetachedMuK')],
                        'DetachedMuPi'     : [DetachedMuPiPair('DetachedMuPi')],

                        'DetachedEEK'      : [DetachedBtoEEK],
                        'DetachedEEPhi'    : [DetachedBtoEEPhi],
                        'DetachedEEKstar'    : [DetachedBtoEEKstar],
                        
                        'DetachedMuMuK'    : [DetachedBtoMuMuK],
                        'DetachedMuMuPhi'    : [DetachedBtoMuMuPhi],
                        'DetachedMuMuKstar'    : [DetachedBtoMuMuKstar],
                        
                        'DetachedProtonProtonPhi'    : [DetachedBtoProtonJpsiPhi]
                        }
        if nickname:
            return self._stages[nickname]
        else:
            return self._stages

    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Configurables import HltANNSvc
        stages = self.stages()
        for (nickname, algos) in self.algorithms(stages):
            Hlt2Line('TrackEffElectron'+nickname+'TurboCalib', prescale = self.prescale,
                     L0DU = self.L0Req[nickname],
                     HLT1 = self.Hlt1Req[nickname],
                     algos = algos, postscale = self.postscale,
                     Turbo=True)
