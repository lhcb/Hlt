##
#  @author V. V. Gligorov vladimir.gligorov@cern.ch
# 
#  Please contact the responsible before editing this file
#
##

# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter

from HltTracking.HltPVs import PV3D
from Inputs import Hlt2Muons,Hlt2Electrons,Hlt2Kaons,Hlt2Pions, Hlt2Protons
from Inputs import Hlt2ProbeMuons, Hlt2ProbeElectrons, Hlt2ProbeKaons
from Inputs import Hlt2Phi2KK, Hlt2Kstars

class TrackGEC(Hlt2VoidFilter):
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules =  Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] : 
            if i not in modules : modules.append(i)
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, "TrackEffElectron" + name, code, [tracks], shared = True, nickname = 'TrackGEC')

# Single track selection filters
class DetachedMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPMu)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Mu)s) & (PT> %(PtMu)s) & \
                 (TRCHI2DOF<%(TrChi2Mu)s)  & in_range(%(EtaMinMu)s, ETA, %(EtaMaxMu)s) & (PROBNNmu > %(ProbNNmu)s)")
        inputs = [ Hlt2Muons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedElectronFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPEle)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Ele)s) & (PT> %(PtEle)s) & \
                 (TRCHI2DOF<%(TrChi2Ele)s)  & in_range(%(EtaMinEle)s, ETA, %(EtaMaxEle)s) & (PROBNNe > %(ProbNNe)s)") 
        inputs = [ Hlt2Electrons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedKaonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPKaon)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Kaon)s) & (PT> %(PtKaon)s) & \
                 (TRCHI2DOF<%(TrChi2Kaon)s) & (PROBNNk > %(ProbNNk)s)")
        inputs = [ Hlt2Kaons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedProtonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPProton)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Proton)s) & (PT> %(PtProton)s) & \
                 (TRCHI2DOF<%(TrChi2Proton)s) & (PROBNNp > %(ProbNNp)s)")
        inputs = [ Hlt2Protons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedPionFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPPion)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Pion)s) & (PT> %(PtPion)s) & \
                 (TRCHI2DOF<%(TrChi2Pion)s) & (PROBNNpi > %(ProbNNpi)s)")
        inputs = [ Hlt2Pions ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedProbeFilterForTrackEffElectron(Hlt2ParticleFilter) :
    def __init__(self, name, inputs):
        code = ("(MIPDV(PRIMARY) > %(IPprobe)s) & (TRCHI2DOF<%(MAXTRCHI2probe)s) & in_range(1.9, ETA, 5.1)")

        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    dependencies = [PV3D('Hlt2')], shared = True)

###
# Common single-track selections
###
# Tag particles
DetachedTagPions = DetachedPionFilter('DetachedTagPions_ForEleTDet','SharedChild')
DetachedTagKaons = DetachedKaonFilter('DetachedTagKaons_ForEleTDet','SharedChild')
DetachedTagMuons = DetachedMuonFilter('DetachedTagMuons_ForEleTDet','SharedChild')
DetachedTagElectrons = DetachedElectronFilter('DetachedTagElectrons_ForEleTDet','SharedChild')
DetachedTagProtons = DetachedProtonFilter('DetachedTagProtons_ForEleTDet','SharedChild')

# Probe particles
DetachedProbeElectrons = DetachedProbeFilterForTrackEffElectron("DetachedProbeElectrons", [Hlt2ProbeElectrons])
DetachedProbeMuons = DetachedProbeFilterForTrackEffElectron("DetachedProbeMuons", [Hlt2ProbeMuons])
DetachedProbeKaons = DetachedProbeFilterForTrackEffElectron("DetachedProbeKaons", [Hlt2ProbeKaons])

###
# Common combined tag selections
###
class PhiFilterForTrackEffElectron(Hlt2ParticleFilter) :
    def __init__(self, name):
        code = ("(PT > %(MINPT)s) " +
                "& (MINTREE('K+'==ABSID,BPVIPCHI2()) > %(MINIPCHI2)s) " +
                "& (MINTREE('K+'==ABSID,PIDK) > %(KMINPIDK)s) " +
                "& (MAXTREE('K+'==ABSID,TRCHI2DOF) < %(MAXTRCHI2)s) " +
                "& (VFASPF(VCHI2PDOF) < %(MAXVCHI2P)s) "+
                "& (BPVVDZ > %(MINFDZ)s) ")

        inputs = [Hlt2Phi2KK]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    dependencies = [PV3D('Hlt2')], shared = True) #What does the shared=True do??

class KstarFilterForTrackEffElectron(Hlt2ParticleFilter) :
    def __init__(self, name, inputs=[Hlt2Kstars]):
        code = ("(PT > %(MINPT)s) " +
                "& (MINTREE('K+'==ABSID,BPVIPCHI2()) > %(MINIPCHI2)s) " +
                "& (MAXTREE('K+'==ABSID,TRCHI2DOF) < %(MAXTRCHI2)s) " +
                "& (VFASPF(VCHI2PDOF) < %(MAXVCHI2P)s) "+
                "& (BPVVDZ > %(MINFDZ)s) ")
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    dependencies = [PV3D('Hlt2')], shared = True) #What does the shared=True do??

DetachedPhis = PhiFilterForTrackEffElectron("DetachedPhi_ForEleTrackEff");
DetachedKstars = KstarFilterForTrackEffElectron("DetachedKstars_ForEleTrackEff", inputs=[Hlt2Kstars]);

###
# Specific combined tag selections
###
class DetachedEKPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'K+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagElectrons,DetachedTagKaons]
        motherMonitor   = (   "process(monitor(M,  mHist_eK , 'eK_mass'))"
                           ">> process(monitor(PT, ptHist_eK, 'eK_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_e_eK, 'e_eK_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ K-]cc","[J/psi(1S) -> e+ K+]cc"], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_eK     = Gaudi.Histo1DDef('eK_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_eK    = Gaudi.Histo1DDef('eK_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_e_eK = Gaudi.Histo1DDef('e_eK_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedEPiPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'pi+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagElectrons,DetachedTagPions]
        motherMonitor   = (   "process(monitor(M,  mHist_ePi , 'ePi_mass'))"
                           ">> process(monitor(PT, ptHist_ePi, 'ePi_PT'  ))"
                           ">> ~EMPTY"
                          )   
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_e_ePi, 'e_ePi_ETA'))"
                                    ">> ~EMPTY"
                                   )   
                          } 
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ pi-]cc", "[J/psi(1S) -> e+ pi+]cc"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc, 
                                    Preambulo  = ["mHist_ePi     = Gaudi.Histo1DDef('ePi_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_ePi    = Gaudi.Histo1DDef('ePi_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_e_ePi = Gaudi.Histo1DDef('e_ePi_ETA'  , 0       , 6       , 120)",
                                                 ],  
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedMuKPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'K+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagMuons,DetachedTagKaons]
        motherMonitor   = (   "process(monitor(M,  mHist_muK , 'muK_mass'))"
                           ">> process(monitor(PT, ptHist_muK, 'muK_PT'  ))"
                           ">> ~EMPTY"
                          )   
        daughterMonitor = { "mu+" : (    "process(monitor(ETA, etaHist_mu_muK, 'mu_muK_ETA'))"
                                     ">> ~EMPTY"
                                    )   
                          }  
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ K-]cc","[J/psi(1S) -> mu+ K+]cc"], 
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc, 
                                    Preambulo  = ["mHist_muK      = Gaudi.Histo1DDef('muK_mass'    , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_muK     = Gaudi.Histo1DDef('muK_PT'      , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_mu_muK = Gaudi.Histo1DDef('mu_muK_ETA'  , 0       , 6       , 120)",
                                                 ],  
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             ) 

class DetachedMuPiPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'pi+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagMuons, DetachedTagPions]
        motherMonitor   = (   "process(monitor(M,  mHist_muPi , 'muPi_mass'))"
                           ">> process(monitor(PT, ptHist_muPi, 'muPi_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "mu+" : (    "process(monitor(ETA, etaHist_mu_muPi, 'mu_muPi_ETA'))"
                                     ">> ~EMPTY"
                                    )
                          }
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ pi-]cc", "[J/psi(1S) -> mu+ pi+]cc"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_muPi      = Gaudi.Histo1DDef('muPi_mass'    , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_muPi     = Gaudi.Histo1DDef('muPi_PT'      , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_mu_muPi = Gaudi.Histo1DDef('mu_muPi_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )


class DetachedEPhiPair(Hlt2Combiner) :
    def __init__(self,name):
        cc = ("(AM < %(AMMAX)s) & (AM>%(AMMIN)s)")
        
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagElectrons,DetachedPhis]
        
        motherMonitor   = (   "process(monitor(M,  mHist_ePhi , 'ePhi_mass'))"
                           ">> process(monitor(PT, ptHist_ePhi, 'ePhi_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_e_ePhi, 'e_ePhi_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ phi(1020)]cc"], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_ePhi     = Gaudi.Histo1DDef('ePhi_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_ePhi    = Gaudi.Histo1DDef('ePhi_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_e_ePhi = Gaudi.Histo1DDef('e_ePhi_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )


class DetachedMuPhiPair(Hlt2Combiner) :
    def __init__(self,name):
        cc = ("(AM < %(AMMAX)s) & (AM>%(AMMIN)s)")
        
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagMuons, DetachedPhis]
        
        motherMonitor   = (   "process(monitor(M,  mHist_muPhi , 'muPhi_mass'))"
                           ">> process(monitor(PT, ptHist_muPhi, 'muPhi_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_mu_muPhi, 'mu_muPhi_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ phi(1020)]cc"], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_muPhi     = Gaudi.Histo1DDef('muPhi_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_muPhi    = Gaudi.Histo1DDef('muPhi_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_mu_muPhi = Gaudi.Histo1DDef('mu_muPhi_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )


class DetachedProtonPhiPair(Hlt2Combiner) :
    def __init__(self,name):
        cc = ("(AM < %(AMMAX)s) & (AM>%(AMMIN)s)")
        
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & ( in_range( %(CORRMMIN)s, BPVCORRM, %(CORRMMAX)s ) ) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagProtons, DetachedPhis]
        
        motherMonitor   = (   "process(monitor(M,  mHist_pPhi , 'pPhi_mass'))"
                           ">> process(monitor(PT, ptHist_pPhi, 'pPhi_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_p_pK, 'p_pK_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> p+ phi(1020)]cc"], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_pPhi     = Gaudi.Histo1DDef('pPhi_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_pPhi    = Gaudi.Histo1DDef('pPhi_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_p_pPhi = Gaudi.Histo1DDef('p_pPhi_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedEKstarPair(Hlt2Combiner) :
    def __init__(self,name):
        cc = ("(AM < %(AMMAX)s) & (AM>%(AMMIN)s)")
        
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedTagElectrons, DetachedKstars]
        
        motherMonitor   = (   "process(monitor(M,  mHist_eKstar , 'eKstar_mass'))"
                           ">> process(monitor(PT, ptHist_eKstar, 'eKstar_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_e_eKstar, 'e_eKstar_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ K*(892)0]cc",
                                     "[J/psi(1S) -> e- K*(892)0]cc"
                                     ], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                     CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_eKstar     = Gaudi.Histo1DDef('eKstar_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_eKstar    = Gaudi.Histo1DDef('eKstar_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_e_eKstar = Gaudi.Histo1DDef('e_eKstar_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedMuKstarPair(Hlt2Combiner) :
    def __init__(self,name):
        cc = ("(AALL)") # make this in_range
        
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s) ")
        inputs = [DetachedTagMuons, DetachedKstars]
        
        motherMonitor   = (   "process(monitor(M,  mHist_muKstar , 'muKstar_mass'))"
                           ">> process(monitor(PT, ptHist_muKstar, 'muKstar_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_muKstar, 'mu_muKstar_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ K*(892)0]cc",
                                     "[J/psi(1S) -> mu- K*(892)0]cc"
                                     ], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_muKstar     = Gaudi.Histo1DDef('muKstar_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_muKstar    = Gaudi.Histo1DDef('muKstar_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_muKstar = Gaudi.Histo1DDef('mu_muKstar_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )
        

###
# B-selections (Tag combination + VELO particle)
###
class DetachedBtoECombiner(Hlt2Combiner) :
    def __init__(self,name, inputs) :
        dc = {"e+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = ("(AM < %(AMTAP)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2TAP)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s)")

        preambulo =  [ 
            'from numpy import inner', 
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',

            'TagElectronMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagElectronEnergy = CHILD(CHILD(E,1), 1)',

            'ProbeElectron    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeElectron[:]',
            'ProbeElectron = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',

            'TagPCosineTheta = inner(ProbeElectron, TagElectronMomentumVector)',
            'Electron_M = 0.511', 
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Electron_M**2 - Electron_M**2)/(TagElectronEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeElectron[i] + TagElectronMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagElectronEnergy + math.sqrt(Electron_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)' 
        ]

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,
                                    ["B+ -> J/psi(1S) e+","B- -> J/psi(1S) e-"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = preambulo
                             )
        
class DetachedBtoMuCombiner(Hlt2Combiner) :
    def __init__(self,name, inputs) :
        dc = {"mu+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = ("(AM < %(AMTAP)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2TAP)s) & in_range(%(CORRMMIN)s, BPVCORRM, %(CORRMMAX)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s) & (BPVVDZ > %(MINFDZ)s) & in_range(%(PVMMIN)s, PVMASS, %(PVMMAX)s) & (MCONSTDIRA > %(MINCONSTRAINTDIRA)s) & in_range(%(PMINMUON)s, Probe_Momentum_From_Mass_constraint, %(PMAXMUON)s)")

        preambulo =  [ # With thanks to L. Dufour!
            'from numpy import inner', #if you want you can also calculate all inner products yourself.
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',
            #   
            'TagMuonMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagMuonEnergy = CHILD(CHILD(E,1), 1)',
            #   
            'ProbeMuon    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeMuon[:]',
            'ProbeMuon = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
            #       
            'TagPCosineTheta = inner(ProbeMuon, TagMuonMomentumVector)', # |p_tag| Cos(Theta) 
            #   
            'Muon_M = 105.658', # in MeV
            #   
            # ideally would replace the 3096.9 with a functor to get the PDG mass for the J/Psi(1S) in MeV
            # (there must be a functor for this PDG mass...)
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Muon_M**2 - Muon_M**2)/(TagMuonEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeMuon[i] + TagMuonMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagMuonEnergy + math.sqrt(Muon_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)',
            'BMomentumVector = [JPsi_momentum[i] + TagMuonMomentumVector[i] for i in range(3)]',
            'B_FD_Vector = [VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)]',
            'B_P_CONST = math.sqrt(BMomentumVector[0]**2 + BMomentumVector[1]**2 + BMomentumVector[2]**2)',
            'B_FD_OWNPV = math.sqrt(B_FD_Vector[0]**2 + B_FD_Vector[1]**2 + B_FD_Vector[2]**2)',
            'MCONSTDIRA = inner(BMomentumVector, B_FD_Vector)/(B_P_CONST*B_FD_OWNPV)',
            'PVMASS = POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) )'
        ]   

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,
                                    ["B+ -> J/psi(1S) mu+","B- -> J/psi(1S) mu-"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc, 
                                    Preambulo  = preambulo
                             ) 

class DetachedBtoProtonCombiner(Hlt2Combiner) :
    def __init__(self, name, inputs) :
        cc = ("(AM < %(AMTAP)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2TAP)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s) & in_range(%(PMINPROTON)s, Probe_Momentum_From_Mass_constraint, %(PMAXPROTON)s)")

        preambulo =  [ 
            'from numpy import inner', 
            'TagPhiMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagPhiEnergy            = CHILD(CHILD(E,2),1)',

            'TagProtonMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagProtonEnergy = CHILD(CHILD(E,1), 1)',

            'ProbeProton    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeProton[:]',
            'ProbeProton = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',

            'TagPCosineTheta = inner(ProbeProton, TagProtonMomentumVector)',
            'Proton_M = 938.2',

            'Probe_Momentum_From_Mass_constraint=CHILD(CHILD(P,1),1)', #initial guess for momentum
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096**2 - Proton_M**2 - Proton_M**2)/(TagProtonEnergy*math.sqrt(1+(CHILD(M,2)/Probe_Momentum_From_Mass_constraint)**2) - TagPCosineTheta)',
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096**2 - Proton_M**2 - Proton_M**2)/(TagProtonEnergy*math.sqrt(1+(CHILD(M,2)/Probe_Momentum_From_Mass_constraint)**2) - TagPCosineTheta)',
            
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeProton[i] + TagProtonMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagProtonEnergy + math.sqrt(Proton_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagPhiEnergy)**2 - (JPsi_momentum[0]+TagPhiMomentumVector[0])**2 -(JPsi_momentum[1]+TagPhiMomentumVector[1])**2 - (JPsi_momentum[2]+TagPhiMomentumVector[2])**2)'
        ]

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,
                                    ["B+ -> J/psi(1S) K+","B- -> J/psi(1S) K-"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = preambulo
                             )

#####
# Lines
#####
DetachedBtoEEK = DetachedBtoECombiner("DetachedEEK", [DetachedEKPair('DetachedEK'), 
                                                           DetachedProbeElectrons])
DetachedBtoEEKstar = DetachedBtoECombiner("DetachedEEKstar", [DetachedEKstarPair('DetachedEKstar'), 
                                                                   DetachedProbeElectrons])
DetachedBtoEEPhi = DetachedBtoECombiner("DetachedEEPhi", [DetachedEPhiPair('DetachedEPhi'), 
                                                                   DetachedProbeElectrons])

DetachedBtoMuMuK = DetachedBtoMuCombiner("DetachedMuMuK", [DetachedMuKPair('DetachedMuK'), 
                                                           DetachedProbeMuons])
DetachedBtoMuMuKstar = DetachedBtoMuCombiner("DetachedMuMuKstar", [DetachedMuKstarPair('DetachedMuKstar'), 
                                                                   DetachedProbeMuons])
DetachedBtoMuMuPhi = DetachedBtoMuCombiner("DetachedMuMuPhi", [DetachedMuPhiPair('DetachedMuPhi'), 
                                                                   DetachedProbeMuons])

DetachedBtoProtonJpsiPhi = DetachedBtoProtonCombiner("DetachedProtonProtonPhi", [DetachedProtonPhiPair('DetachedpPhi'), 
                                                                               DetachedProbeKaons]) 
