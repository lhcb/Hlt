## Search for Tetraquak state X(cs\bar{u}\bar{d}\bar:
## mass(X_cs)<mass(D+K-):  X_cs -> K- + K- + pi+ + pi+

## Signal model lines:
##   Xcsud2KmKmPipPipTurbo

## Wrong sign lines for background studies:
##   Xcsud2KmKpPipPipTurbo
##   Xcsud2KmKmPimPipTurbo
## author Chen Chen
__author__ =['Chen Chen']

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
import math

class CharmHadXcsudLines:
    def __init__(self):
        self.slotDict = {
           'Xcsud2KmKmPipPip' : {
                'Trk_ALL_PT_MIN'           :  200 * MeV,
                 'Trk_ALL_MIPCHI2DV_MIN'    :  3.0,
                 'AM12_MAX'                 : 2200.0 * MeV,
                 'AM_4'                     : (139.5) * MeV,
                 'AM_MIN'                   :  2000 * MeV,
                 'AM_MAX'                   :  2500 * MeV,
                 'ASUMPT_MIN'               :  2000.0 * MeV,
                 'ACHI2DOCA_MAX'            :  10.0,
                 'VCHI2PDOF_MAX'            :  12.0,
                 'acosBPVDIRA_MAX'          :  141.5 *mrad,
                 'BPVLTIME_MIN'             :  0.1 *picosecond,
                 'PT_MIN'                   :  2000 * MeV,
                 'IPCHI2_MAX'               :  15.0,
                 'Mass_M_MIN'               :  2070 * MeV,
                 'Mass_M_MAX'               :  2400 * MeV,
            },
            'CharmHadSharedDetachedXcsudChild_pi' : {
                                 'PID_LIM'                  :  0,
                                 'Trk_ALL_PT_MIN'           :  250 * MeV,
                                 'Trk_ALL_MIPCHI2DV_MIN'    :  3.0,
            },            
            'CharmHadSharedDetachedXcsudChild_K' : {
                'PID_LIM'                  :  7,
                'Trk_ALL_PT_MIN'           :  300 * MeV,
                'Trk_ALL_MIPCHI2DV_MIN'    :  3.0,
            }

        }
        self.__stages={ }
        
    def slots(self) :
        return self.slotDict

    def stages(self) :
        if len(self.__stages) == 0 : 
            from Stages import SharedDetachedXcsudChild_pi, SharedDetachedXcsudChild_K

            from Stages import PentaCombiner
            from Stages import MassFilter
    
            XcsudToKmKmPipPip_1 = PentaCombiner("XcsudToKmKmPipPip_1"
                    , decay = '[Xi_c0 -> K- K- pi+ pi+]cc'
                    , inputs = [SharedDetachedXcsudChild_K
                                , SharedDetachedXcsudChild_pi]
                    , nickname ='Xcsud2KmKmPipPip'
                    , shared =True
                    , MotherMonitor = "" )
            XcsudToKmKmPipPip = MassFilter('XcsudToKmKmPipPip'
                    , inputs = [XcsudToKmKmPipPip_1]
                    , nickname ='Xcsud2KmKmPipPip'
                    , shared =True, reFitPVs =True)


            XcsudToKmKpPipPip_1 = PentaCombiner("XcsudToKmKpPipPip_1"
                    , decay = '[Xi_c0 -> K- K+ pi+ pi+]cc'
                    , inputs = [SharedDetachedXcsudChild_K                  
                                 , SharedDetachedXcsudChild_pi]
                    , nickname ='Xcsud2KmKmPipPip'
                    , shared =True
                    , MotherMonitor = "" ) 
            XcsudToKmKpPipPip = MassFilter('XcsudToKmKpPipPip'
                    , inputs = [XcsudToKmKpPipPip_1]
                    , nickname ='Xcsud2KmKmPipPip'
                    , shared =True, reFitPVs =True)

            XcsudToKmKmPimPip_1 = PentaCombiner("XcsudToKmKmPimPip_1"
                    , decay = '[Xi_c0 -> K- K- pi- pi+]cc'
                    , inputs = [SharedDetachedXcsudChild_K
                               , SharedDetachedXcsudChild_pi]
                    , nickname ='Xcsud2KmKmPipPip'
                    , shared =True
                    , MotherMonitor = "" ) 
            XcsudToKmKmPimPip = MassFilter('XcsudToKmKmPimPip'
                    , inputs = [XcsudToKmKmPimPip_1]
                    , nickname ='Xcsud2KmKmPipPip'
                    , shared =True, reFitPVs =True)


            self.__stages = {
                "Xcsud2KmKmPipPipTurbo"        : [XcsudToKmKmPipPip],
                "Xcsud2KmKpPipPipTurbo"        : [XcsudToKmKpPipPip],
                "Xcsud2KmKmPimPipTurbo"        : [XcsudToKmKmPimPip]
            }

        return self.__stages





