##
#  @author A. Romero Vidal antonio.romero@usc.es 
#  @date 2018-02-23
#
##

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class Xc3PiXForTauLines(Hlt2LinesConfigurableUser):
    __slots__ = {'_stages' : {},
                 'XcDaughter'  : {
                              'D_Trk_ALL_PT_MIN'              : 250.0 * MeV
                            , 'D_Trk_ALL_P_MIN'               :   0.0 * GeV
                            , 'D_Trk_ALL_MIPCHI2DV_MIN'       :   9.0
                            , 'D02K3pi_Trk_ALL_MIPCHI2DV_MIN' :  15.0
                            , 'D_K_PIDK'                      :    -3.
                            , 'D_Pi_PROBNNpi'                 :   0.1
                            , 'D02K3pi_K_PIDK'                :    -3.
                            , 'D02K3pi_Pi_PROBNNpi'           :   0.4
                            , 'D_P_PIDp'                      :     5.
                            , 'D_Mu_PIDmu'                    : -5000.
                            },
                 'TauDaughter'  : {
                              'Tau_Trk_ALL_PT_MIN'          :  250.0 * MeV
                            , 'Tau_Trk_ALL_MIPCHI2DV_MIN'   :   15.0
                            , 'Tau_Pi_PROBNNpi'             :    0.6
                            , 'Tau_Trk_ALL_P_MIN'           :    2.0 * GeV
                            },
                 'Xc' : {
                              'D_AMAXDOCA_MAX'           : 0.20 * mm
                            , 'D_Trk_ALL_MAXPT_MIN'      : 1500.0 * MeV
                            , 'Xc_BPVVDCHI2_MIN'         : 25.0
                            , 'Xc_IPCHI2_MIN'            : 9.0
                            , 'Xc_BPVDIRA_MIN'           : 0.995
                            , 'Xc_VCHI2PDOF_MAX'         : 10.0
                            , 'Xc_PT_MIN'                : 1000.0 * MeV
                            , 'D_AM_MIN'                 : 1804.0 * MeV
                            , 'D_AM_MAX'                 : 1929.0 * MeV
                            , 'Ds_AM_MIN'                : 1909.0 * MeV
                            , 'Ds_AM_MAX'                : 2029.0 * MeV
                            , 'Lc_AM_MIN'                : 2226.0 * MeV
                            , 'Lc_AM_MAX'                : 2346.0 * MeV
                            , 'Jpsi_AM_MIN'              : 3000.0 * MeV
                            , 'Jpsi_AM_MAX'              : 3200.0 * MeV
                            },
                 'Tau' : {
                              'Tau_AMAXDOCA_MAX'         : 0.20 * mm
                            , 'Tau_FDT_MIN'              : 0.2 * mm
                            , 'Tau_BPVDIRA_MIN'          : 0.99
                            , 'Tau_VCHI2PDOF_MAX'        : 5.
                            , 'Tau_PT_MIN'               : 1000.0 * MeV
                            , 'Tau_AM_MIN'               : 0. * MeV
                            , 'Tau_AM_MAX'               : 5000.0 * MeV
                            },
                 'B' : {

                              'XcTau_DOCA_MAX'      : 0.20 * mm
                            , 'XcTau_DIRA'          : 0.995
                            , 'XcTau_M_MIN'         :    0.*MeV
                            , 'XcTau_M_MAX'         : 8000.*MeV
                            }

                 }

    def __apply_configuration__(self):

        from Stages import (B2Xc3PiXCombD02Kpi,B2Xc3PiXWSCombD02Kpi,B2Xc3PiXCombD02K3pi,B2Xc3PiXWSCombD02K3pi
                           ,B2Xc3PiXCombDp2Kpipi,B2Xc3PiXWSCombDp2Kpipi,B2Xc3PiXCombDs2kkpi,B2Xc3PiXWSCombDs2kkpi
                           ,B2Xc3PiXCombLc2pkpi,B2Xc3PiXWSCombLc2pkpi,B2Xc3PiXCombJpsi2mumu
                           ,B2Xc3PiXNonPhysCombD02Kpi,B2Xc3PiXNonPhysCombD02K3pi,B2Xc3PiXNonPhysCombDp2Kpipi
                           ,B2Xc3PiXNonPhysCombDs2kkpi,B2Xc3PiXNonPhysCombLc2pkpi,B2Xc3PiXNonPhysCombJpsi2mumu)

        stages = {
                   'D02Kpi' : [ B2Xc3PiXCombD02Kpi ]
                  ,'D02KpiWS' : [ B2Xc3PiXWSCombD02Kpi ]
                  ,'D02KpiNonPhys' : [ B2Xc3PiXNonPhysCombD02Kpi ]
                  ,'D02K3pi' : [ B2Xc3PiXCombD02K3pi ]
                  ,'D02K3piWS' : [ B2Xc3PiXWSCombD02K3pi ]
                  ,'D02K3piNonPhys' : [ B2Xc3PiXNonPhysCombD02K3pi ]
                  ,'Dp2Kpipi' : [ B2Xc3PiXCombDp2Kpipi ]
                  ,'Dp2KpipiWS' : [ B2Xc3PiXWSCombDp2Kpipi ]
                  ,'Dp2KpipiNonPhys' : [ B2Xc3PiXNonPhysCombDp2Kpipi ]
                  ,'Ds2kkpi' : [ B2Xc3PiXCombDs2kkpi ]
                  ,'Ds2kkpiWS' : [ B2Xc3PiXWSCombDs2kkpi ]
                  ,'Ds2kkpiNonPhys' : [ B2Xc3PiXNonPhysCombDs2kkpi ]
                  ,'Lc2pkpi' : [ B2Xc3PiXCombLc2pkpi ]
                  ,'Lc2pkpiWS' : [ B2Xc3PiXWSCombLc2pkpi ]
                  ,'Lc2pkpiNonPhys' : [ B2Xc3PiXNonPhysCombLc2pkpi ]
                  ,'Jpsi2mumu' : [ B2Xc3PiXCombJpsi2mumu ]
                  ,'Jpsi2mumuNonPhys' : [ B2Xc3PiXNonPhysCombJpsi2mumu ]
                 }

        from HltLine.HltLine import Hlt2Line

        for (nickname, algos) in self.algorithms(stages):
            linename = 'Xc3PiXForTau' + nickname if nickname != 'Xc3PiXForTau' else nickname
            Hlt2Line(linename, prescale = self.prescale,
                     algos = algos, postscale = self.postscale)
