from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedMuons       as Hlt2Muons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedProtons     as Hlt2Protons
##phiphi
from Hlt2SharedParticles.Phi                       import UnbiasedPhi2KK            as Hlt2UnbiasedPhi
from Hlt2SharedParticles.Phi                       import Phi2KK                    as Hlt2WidePhi


