##  @author Hang Yin , and Qundong Han
__author__ = [ 'Hang Yin', 'Qundong Han' ]

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
import math

class RareWZWZ2MesonLines() :
    def localcuts(self) :
        return {
            'WZD02HH'   : {
                'Trk_ALL_P_MIN'               : 5.0 * GeV,
                'Trk_ALL_PT_MIN'              : 500.0 * MeV,
                'Trk_Max_APT_MIN'             : 1000.0 * MeV,
                'Pair_AMINDOCA_MAX'           : 0.1 * mm,
                'AM_MIN'                      : 1715.0 * MeV,
                'AM_MAX'                      : 2015.0 * MeV,
                'acosBPVDIRA_MAX'             : 100.0 * mrad,
                'BPVVDCHI2_MIN'               : 25.0,
                'PT_MIN'                      : 1500.0 * MeV,
                'VCHI2PDOF_MAX'               : 10.0,
                'TisTosSpec'                  : "Hlt1.*Track.*Decision%TOS"
            },              
            'WZDpm2HHH' : {
                'Trk_ALL_PT_MIN'              : 250.0 * MeV,
                'Trk_2OF3_PT_MIN'             : 400.0 * MeV,
                'Trk_1OF3_PT_MIN'             : 1000.0 * MeV,
                'ASUMPT_MIN'                  : 3000 * MeV,
                'acosBPVDIRA_MAX'             : 100.0 * mrad,
                'BPVLTIME_MIN'                : 0.4 * picosecond,
                'BPVVDCHI2_MIN'               : 150.0,
                'VCHI2PDOF_MAX'               : 6,
                'AM_MAX'                      : 1959 * MeV,
                'AM_MIN'                      : 1779 * MeV,
                'PT_MIN'                      : 15000 * MeV,
                'TisTosSpec'                  : "Hlt1.*Track.*Decision%TOS"
            },
            'WZDs2HHH' : {
                'Trk_ALL_PT_MIN'              : 250.0 * MeV,
                'Trk_2OF3_PT_MIN'             : 400.0 * MeV,
                'Trk_1OF3_PT_MIN'             : 1000.0 * MeV,
                'ASUMPT_MIN'                  : 3000 * MeV,
                'acosBPVDIRA_MAX'             : 100.0 * mrad,
                'BPVLTIME_MIN'                : 0.2 * picosecond,
                'BPVVDCHI2_MIN'               : 100.0,
                'VCHI2PDOF_MAX'               : 6,
                'AM_MAX'                      : 2059 * MeV,
                'AM_MIN'                      : 1879 * MeV,
                'PT_MIN'                      : 15000 * MeV,
                'TisTosSpec'                  : "Hlt1.*Track.*Decision%TOS"
            },
            'WZKaonGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZPionGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZGammaGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZPi0Gamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZPi0Pi0' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZD0Gamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZDpGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZDsGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZDiMuonGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZKstGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZKs0Gamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZRho0Gamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZPhiGamma' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
            'WZJpsiDiJets' :{
                'AM_MIN'                      : 30000 * MeV,
                'AM_MAX'                      : 130000 * MeV,
            },
        }
        

    def locallines(self):
        from Stages import MassFilter, TagDecayWithNeutral, DiMuonFilter
        from Stages import SharedChild_HighPTk, SharedChild_HighPTpi
       
        # Gamma
        from Stages import SharedChild_Gamma, SharedChild_HighPTGamma
        # pi0 
        from Stages import SharedChild_Pi0, SharedChild_LoosePi0, SharedChild_HighPTPi0
        # D0
        from Stages import WZD02HH_D0ToKmPip
        # D+
        from Stages import WZD2HHH_DpToKmPipPip
        # Dsp
        from Stages import WZD2HHH_DspToKmKpPip
        # Kst
        from Stages import WZRare_Ks0, WZRare_KstHHH
        # rho
        from Stages import WZRare_rho0
        # phi
        from Stages import WZRare_phi
        # dijets
        from Stages import DiJetCombiner, FilterMuon, FilterSV

        # meson reconstruction
        WZD0ToKpPim  = MassFilter('WZD0ToKpPim'
                                  , inputs=[WZD02HH_D0ToKmPip]
                                  , nickname = 'WZD02HH'
                                  , shared = True, reFitPVs = True
                                  , monitor = 'D0')

        WZDpToKpPipPim  = MassFilter('WZDpToKpPipPim'
                                     , inputs=[WZD2HHH_DpToKmPipPip]
                                     , nickname = 'WZDpm2HHH'
                                     , shared = True, reFitPVs = True
                                     , monitor = 'D+')
        
        WZDspToKmKpPip   = MassFilter('WZDspToKmKpPip'
                                      , inputs=[WZD2HHH_DspToKmKpPip]
                                      , nickname = 'WZDs2HHH'
                                      , shared = True, reFitPVs = True
                                      , monitor = 'D_s+')       
        WZJPsiDiMuon    = DiMuonFilter('RareWZSharedDiMuon')

        ## W and Z reconstruction
        WZD0Gamma = TagDecayWithNeutral('WZMG_D0Gamma'
                                        , ["[Z0 -> D0 gamma]cc"]
                                        , inputs = [WZD0ToKpPim, SharedChild_Gamma]
                                        , nickname = 'WZD0Gamma'
                                        , shared = True)
        WZDiMuonGamma = TagDecayWithNeutral('WZMG_DiMuonGamma'
                                            , ["Z0 -> J/psi(1S) gamma"]
                                            , inputs = [WZJPsiDiMuon, SharedChild_Gamma]
                                            , nickname = 'WZDiMuonGamma'
                                            , shared = True)
        WZDpGamma = TagDecayWithNeutral('WZMG_DpGamma'
                                        , ["[W+ -> D+ gamma]cc"]
                                        , inputs = [WZDpToKpPipPim, SharedChild_Gamma]
                                        , nickname = 'WZDpGamma'
                                        , shared = True)
        WZDsGamma = TagDecayWithNeutral('WZMG_DsGamma'
                                        , ["[W+ -> D_s+ gamma]cc"]
                                        , inputs = [WZDspToKmKpPip, SharedChild_Gamma]
                                        , nickname = 'WZDsGamma'
                                        , shared = True)

        WZKaonGamma   = TagDecayWithNeutral('WZMG_KaonGamma'
                                            , ["[W+ -> K+ gamma]cc"]
                                            , inputs = [SharedChild_HighPTk, SharedChild_Gamma]
                                            , nickname = 'WZKaonGamma'
                                            , shared = True)
        WZPionGamma   = TagDecayWithNeutral('WZMG_PionGamma'
                                            , ["[W+ -> pi+ gamma]cc"]
                                            , inputs = [SharedChild_HighPTpi, SharedChild_Gamma]
                                            , nickname = 'WZPionGamma'
                                            , shared = True)

        WZGammaGamma   = TagDecayWithNeutral('WZMG_GammaGamma'
                                            , ["Z0 -> gamma gamma"]
                                            , inputs = [SharedChild_HighPTGamma]
                                            , nickname = 'WZGammaGamma'
                                            , shared = True)
        WZPi0Pi0   = TagDecayWithNeutral('WZMG_Pi0Pi0'
                                         , ["Z0 -> pi0 pi0"]
                                         , inputs = [SharedChild_HighPTPi0]
                                         , nickname = 'WZPi0Pi0'
                                         , shared = True)
        WZPi0Gamma   = TagDecayWithNeutral('WZMG_Pi0Gamma'
                                         , ["Z0 -> pi0 gamma"]
                                           , inputs = [SharedChild_HighPTPi0, SharedChild_HighPTGamma]
                                         , nickname = 'WZPi0Gamma'
                                         , shared = True)
        WZKs0Gamma = TagDecayWithNeutral('WZMG_Ks0Gamma'
                                        , ["Z0 -> K*(892)0 gamma"]
                                        , inputs = [WZRare_Ks0, SharedChild_Gamma]
                                        , nickname = 'WZKs0Gamma'
                                        , shared = True)
        WZKstGamma = TagDecayWithNeutral('WZMG_KstGamma'
                                        , ["[W+ -> K*(892)+ gamma]cc"]
                                        , inputs = [WZRare_KstHHH, SharedChild_Gamma]
                                        , nickname = 'WZKstGamma'
                                        , shared = True)
        WZRho0Gamma = TagDecayWithNeutral('WZMG_Rho0Gamma'
                                          , ["Z0 -> rho(770)0 gamma"]
                                        , inputs = [WZRare_rho0, SharedChild_Gamma]
                                        , nickname = 'WZRho0Gamma'
                                        , shared = True)
        WZPhiGamma = TagDecayWithNeutral('WZMG_PhiGamma'
                                          , ["Z0 -> phi(1020) gamma"]
                                        , inputs = [WZRare_phi, SharedChild_Gamma]
                                        , nickname = 'WZPhiGamma'
                                        , shared = True)

        ### jet configrations, copied from Jet package
        from Hlt2Lines.Topo.Lines import TopoLines
        from Hlt2Lines.Utilities.Hlt2Stage import Hlt2ExternalStage
        from Hlt2Lines.Utilities.Hlt2JetBuilder import Hlt2JetBuilder

        topo_lines = TopoLines()
        topoCombiner = topo_lines.intermediateStages('Topo2BodyCombos')[0]
        topo_sv = Hlt2ExternalStage(topo_lines, topoCombiner)
        sv = [FilterSV([topo_sv])]

        mu = [FilterMuon()]

        wzjets = [Hlt2JetBuilder('DiJetsJetBuilder',
                       sv + mu,
                       reco = 'TURBO',
                       #shared = True, reco = sv.getProps()['JetBuilder']['Reco'],
                       nickname = 'RareWZDiJets')]

        WZRare_DiJets = DiJetCombiner(wzjets, '', '');
        WZDiMuonDiJets = TagDecayWithNeutral('WZMG_DiMuonDiJets'
                                             , ["[W+ -> J/psi(1S) CLUSjet]cc"]
                                             , inputs = [WZJPsiDiMuon, WZRare_DiJets]
                                             , nickname = 'WZJpsiDiJets'
                                             , shared = True)

        stages = {
            'D0GammaTurbo'          : [WZD0Gamma],
            'DpGammaTurbo'          : [WZDpGamma],
            'DsGammaTurbo'          : [WZDsGamma],
            'KaonGammaTurbo'        : [WZKaonGamma],
            'PionGammaTurbo'        : [WZPionGamma],
            'GammaGammaTurbo'       : [WZGammaGamma],
            'Pi0GammaTurbo'         : [WZPi0Gamma],
            'Pi0Pi0Turbo'           : [WZPi0Pi0],
            'DiMuonGammaTurbo'      : [WZDiMuonGamma],
            'Ks0GammaTurbo'         : [WZKs0Gamma],
            'KstGammaTurbo'         : [WZKstGamma],
            'Rho0GammaTurbo'        : [WZRho0Gamma],
            'PhiGammaTurbo'         : [WZPhiGamma],
            'JPsiDiJetsTurbo'       : [WZDiMuonDiJets],
        }
        return stages
