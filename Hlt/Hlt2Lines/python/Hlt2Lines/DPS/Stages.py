#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file
#  Set of ``stages'' for Hlt2-lines devoted to the study of
#  the associative production of various objects: e.g. J/psi&J/psi, J/psi&D, D&D, ...
#
#  The primary goal is to ``protect'' these events from prescaling 
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-03-10  
# =============================================================================
""" Set of ``stages'' for Hlt2-lines devoted to the study of
the associative production of various objects: e.g. J/psi&J/psi, J/psi&D, D&D, ...

The primary goal is to ``protect'' these events from prescaling 
"""
# =============================================================================
__version__ = "$Revision: $"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2015-03-10"
__all__     = ( 'MergeCharm'           ,
                ## double charm  
                'DoubleDiMuon'         ,
                'DoubleCharm'          ,
                'DiMuonAndCharm'       ,
                ## triple  charm: 
                'TripleDiMuon'         ,
                'TripleCharm'          ,
                'DiMuonAndDiCharm'     ,
                'DoubleDiMuonAndCharm'                 
                )
# =============================================================================
from Hlt2Lines.Utilities.Hlt2Combiner    import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter      import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2MergedStage import Hlt2MergedStage
# =============================================================================
## @class MergedCharm
#  make ``merged'' container of charm 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-03-10
class MergeCharm(Hlt2MergedStage) :
    """
    Make ``merged'' container of charm
    """
    def __init__ ( self , name , inputs ) :
        super(MergeCharm, self).__init__( name, inputs )

# =============================================================================
## @class DiCharm
#  Make various combinations of charm hadrons 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-03-10
class DiCharm(Hlt2Combiner) :
    """
    Make various combinations of charm hadrons 
    """
    def __init__ ( self , name , decays , inputs ) :
        """ Instantiate the combiner """ 
        super(DiCharm, self).__init__( name , decays , inputs ,
                                       CombinationCut = 'AALL' , 
                                       MotherCut      = 'ALL'  )

# ==============================================================================
## decays for  double charm:
# ==============================================================================
decays_2x2mu = [ 'chi_b0(1P) -> J/psi(1S) J/psi(1S)'           ] 
decays_2muHc = [ "[ chi_b1(1P) -> J/psi(1S) D0        ]cc"     ,
                 "[ chi_b1(1P) -> J/psi(1S) D+        ]cc"     ,
                 "[ chi_b1(1P) -> J/psi(1S) D_s+      ]cc"     ,
                 "[ chi_b1(1P) -> J/psi(1S) Lambda_c+ ]cc"     ]
decays_2xHc  = [ "   psi(3770) -> D0        D~0            "   ,
                 " [ psi(3770) -> D0        D-         ]cc "   ,
                 " [ psi(3770) -> D0        D_s-       ]cc "   ,
                 " [ psi(3770) -> D0        Lambda_c~- ]cc "   ,
                  #
                 "   psi(3770) -> D+        D-             "   ,
                 " [ psi(3770) -> D+        D_s-       ]cc "   ,
                 " [ psi(3770) -> D+        Lambda_c~- ]cc "   ,
                 #
                 "   psi(3770) -> D_s+      D_s-           "   ,
                 " [ psi(3770) -> D_s+      Lambda_c~- ]cc "   ,
                 #
                 "   psi(3770) -> Lambda_c+ Lambda_c~-     "   ,
                 # double charm
                 " [ psi(3770) -> D0        D0         ]cc"    ,
                 " [ psi(3770) -> D0        D+         ]cc "   ,
                 " [ psi(3770) -> D0        D_s+       ]cc "   ,
                 " [ psi(3770) -> D0        Lambda_c+  ]cc "   ,
                 #
                 " [ psi(3770) -> D+        D+         ]cc "   ,
                 " [ psi(3770) -> D+        D_s+       ]cc "   ,
                 " [ psi(3770) -> D+        Lambda_c+  ]cc "   ,
                 # 
                 " [ psi(3770) -> D_s+      D_s+       ]cc "   ,
                 " [ psi(3770) -> D_s+      Lambda_c+  ]cc "   ,
                 #
                 " [ psi(3770) -> Lambda_c+ Lambda_c+  ]cc "   ]

# =============================================================================
## construct decays for tricharm :
# =============================================================================

decays_3x2mu = [ 'chi_b0(1P) -> J/psi(1S) J/psi(1S) J/psi(1S)' ]  ## simple case 

parts  = 'D0'  , 'D+' , 'D_s+' , 'Lambda_c+' 
aparts = 'D~0' , 'D-' , 'D_s-' , 'Lambda_c~-'

## combinations for triple charm 
def _comb3 ( cnt ) :            
    decs = set()
    l = len(cnt) 
    for i in range(l) :
        ip = cnt[i]
        for j in range(i,l) : 
            jp = cnt[j]
            for k in range(j,l) :
                kp = cnt[k]
                decs.add ( '[chi_c1(1P) -> %s %s %s]cc' % ( ip , jp , kp ) )            
    return decs

## combinations for triple charm 
def _comb2 ( cnt1 , cnt2 ) :
    decs = set()
    l1 = len(cnt1) 
    l2 = len(cnt2) 
    for i in range(l1) :
        ip = cnt1[i]
        for j in range(i,l1) : 
            jp = cnt1[j]
            for k in range(l2) :
                kp = cnt2[k]
                decs.add ( '[chi_c2(1P) -> %s %s %s]cc' % ( ip , jp , kp ) )
    return decs

decays_3xHc_1 = _comb3 ( parts          )
decays_3xHc_2 = _comb2 ( parts , aparts )
decays_3xHc   = list ( decays_3xHc_1.union ( decays_3xHc_2 ) )    
decays_3xHc.sort() 

def _comb_1 ( cnt ) :            
    decs = set()
    l = len(cnt) 
    for i in range(l) :
        ip = cnt[i]
        for j in range(i,l) :
            jp = cnt[j]
            decs.add ( '[chi_c0(1P) -> J/psi(1S) %s %s]cc' % ( ip , jp ) )            
    return decs

def _comb_2 ( cnt1 , cnt2  ) :            
    decs = set()
    l1 = len(cnt1) 
    l2 = len(cnt2) 
    for i in range(l1) :
        ip = cnt1[i]
        for j in range(i,l2) :
            jp = cnt2[j]
            if i == j : decs.add ( ' chi_c0(1P) -> J/psi(1S) %s %s'    % ( ip , jp  ) )
            else      : decs.add ( '[chi_c0(1P) -> J/psi(1S) %s %s]cc' % ( ip , jp  ) )
    return decs


_decs3 = _comb_1 ( parts          ) 
_decs2 = _comb_2 ( parts , aparts ) 
decays_2mu2Hc = list(_decs2.union(_decs3))
decays_2mu2Hc.sort() 

decays_2x2muHc = [ '[Upsilon(2S) -> J/psi(1S) J/psi(1S) %s]cc' % i for i in parts  ]

# =============================================================================
class DoubleDiMuon  (DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_2x2mu , inputs )
        
# =============================================================================
class DiMuonAndCharm(DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_2muHc , inputs )

# =============================================================================
class DoubleCharm(DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_2xHc  , inputs )


# =============================================================================
#  Triple charm 
# =============================================================================

# =============================================================================
class TripleDiMuon  (DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_3x2mu , inputs )
        
# =============================================================================
class TripleCharm(DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_3xHc  , inputs )

# =============================================================================
class DiMuonAndDiCharm(DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_2mu2Hc , inputs )

# =============================================================================
class DoubleDiMuonAndCharm(DiCharm) :
    def __init__ ( self , name , inputs ) :
        DiCharm.__init__ ( self , name , decays_2x2muHc , inputs )
        
# ==============================================================================
if '__main__' == __name__ :

    print 100*'*'
    print __doc__ 
    print 100*'*'
    print 'Symbols : %s ' % list( __all__ ) 
    print 'Author  : %s ' % __author__ 
    print 'Date    : %s ' % __date__ 
    print 'Verison : %s ' % __version__
    print 100*'*'
    
# =============================================================================
# The END 
# =============================================================================
