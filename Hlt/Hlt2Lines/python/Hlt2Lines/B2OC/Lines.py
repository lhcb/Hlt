from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
from Hlt2Lines.Utilities.Hlt2RelatedInfo import Hlt2RelInfoConeVariables

heavyIDs = "( (5 == ((ABSID / 1000) % 10)) " \
           + "| (4 == ((ABSID / 1000) % 10))" \
           + "| (5 == ((ABSID / 100) % 10))" \
           + "| (4 == ((ABSID / 100) % 10)) )"


class B2OCLines(Hlt2LinesConfigurableUser):

    
    __slots__ = {'Prescale' : {},
                 'TrackGEC' : {'NTRACK_MAX'           : 500},
                 'Common'   : { 'TRCHI2DOF_MAX'    : 4.0,
                                 'PT_MIN'               : 100 * MeV,
                                 'P_MIN'                : 1000 * MeV,
                                 'MIPCHI2_MIN'           : 4.0,
                                 'GHOSTPROB_MAX'        : 0.4},

                 'BachKaon'   : { 'PT_MIN'               : 500 * MeV,
                                 'P_MIN'                : 5000 * MeV},
                 'BachPion'   : { 'PT_MIN'               : 500 * MeV,
                                 'P_MIN'                : 5000 * MeV},

                 'D2HH'     : {  'ComCuts'    : ("(ASUM(PT)>1800*MeV)"
                                                 " & (in_range(1764.84*MeV,AMASS(1,2),1964.84*MeV))"
                                                 " & (AHASCHILD((HASTRACK & (PT > 500*MeV) & (P > 5000*MeV))))"
                                                 " & (ADOCA(1,2)<0.5*mm)"),
                                 'MomCuts'   :  "(VFASPF(VCHI2/VDOF)<10) & (BPVVDCHI2>36) & (BPVDIRA>0)",
                                 'DauCuts'    : "ALL",
                                 'PIDCuts'    : (" ((NINGENERATION(('K+'==ABSID)  & (PIDK > -10), 1) == 1)"
                                                 " & (NINGENERATION(('pi+'==ABSID) & (PIDK < 20), 1) == 1))"
                                                 " | (NINGENERATION(('K+'==ABSID)  & (PIDK > -2), 1) == 2)"
                                                 " | (NINGENERATION(('pi+'==ABSID)  & (PIDK < 4), 1) == 2)")},

                 'B2DH_D2HH'     : { 'MomCuts'    : ("(VFASPF(VCHI2/VDOF)<10)"
                                                     " & (INTREE(HASTRACK & (P>10000*MeV) & (PT>1700*MeV) & (MIPCHI2DV(PRIMARY)>16) & (MIPDV(PRIMARY)>0.1*mm)))"
                                                     " & (NINTREE((ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV))) > 1)"
                                                     " & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.999)"),
                                     'ComCuts'   :  "(ASUM(SUMTREE(PT,ISBASIC,0.0))>5000*MeV) & (AM<7000*MeV) & (AM>4750*MeV)",
                                 'DauCuts'    : "ALL" },


                 #related info
                 'PConeVar10' : { "ConeAngle" : 1.0,
                                   "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                   "Location"  : 'PConeVar10' },
                 'PConeVar15' : { "ConeAngle" : 1.5,
                                   "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                   "Location"  : 'PConeVar15' },
                 'PConeVar17' : { "ConeAngle" : 1.7,
                                   "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                   "Location"  : 'PConeVar17' },


                ## Configuration for Generic B to C decay BDT
                'GenericBBDT'   : {
                                  'ParamFile'       : "Beauty2Charm_BDTParams_v1r0.txt"
                                , 'Threshold'             : 0.05

                                , 'VarMap' : {
                                        "FDCHI2"          : "BPVVDCHI2",
                                        "PT" : "PT",
                                        "HVCHI2DOFTOT" :  ("SUMTREE(VFASPF(VCHI2),%s,0.0) / "
                                                           "SUMTREE(VFASPF(VDOF),%s,0.0)" % (heavyIDs, heavyIDs)) 
                                }
                }

                 
             }
    
    def relatedInfo(self):


        PConeVar10 = Hlt2RelInfoConeVariables('PConeVar10')
        PConeVar15 = Hlt2RelInfoConeVariables('PConeVar15')
        PConeVar17 = Hlt2RelInfoConeVariables('PConeVar17')
        ConeVars = [PConeVar10,PConeVar15,PConeVar17]


        stages = [ 'B2DPi_D2HH' ,  
                   'B2DK_D2HH'  ]  
        
        out_dict = {}
        for stage in stages:
            out_dict[stage] = ConeVars
        
        return out_dict




    def __apply_configuration__(self):
        from HltLine.HltLine import Hlt2Line


        from Stages import BachNoPIDPions, BachNoPIDKaons
        from Stages import B2DHBuilder
        from Stages import BDTFilter
        from Stages import SelD2HH

        SelB2DPi_D2HH_Comb = B2DHBuilder("B2DPi_D2HH","B2DH_D2HH", "[ B- -> D0 pi- ]cc",[SelD2HH,BachNoPIDPions])
        SelB2DK_D2HH_Comb = B2DHBuilder("B2DK_D2HH","B2DH_D2HH", "[ B- -> D0 K- ]cc",[SelD2HH,BachNoPIDKaons])
        SelB2DK_D2HH = BDTFilter('B2DK_D2HH_BDTFilt', 'GenericBBDT', [ SelB2DK_D2HH_Comb ], self.getProps()['GenericBBDT'] )
        SelB2DPi_D2HH = BDTFilter( 'B2DPi_D2HH_BDTFilt','GenericBBDT' , [ SelB2DPi_D2HH_Comb ], self.getProps()['GenericBBDT'] )

        stages = { 'B2DPi_D2HH' :  [ SelB2DPi_D2HH],
                   'B2DK_D2HH'  :  [ SelB2DK_D2HH]}



        for (nickname, algos, relInfo) in self.algorithms(stages):
            Hlt2Line('B2OC_'+nickname+'Turbo', prescale = self.prescale, Turbo = True,
                     algos = algos, RelatedInfo = relInfo, postscale = self.postscale)
