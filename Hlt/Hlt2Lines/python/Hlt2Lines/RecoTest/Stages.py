# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Stage import Hlt2Stage
from HltTracking.HltPVs import PV3D

from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Inputs import Pions,DownPions,Photons,TrackFittedDiMuon

class CreatePions(Hlt2ParticleFilter):
    def __init__(self,name):
        code = ("(PT >  %(Pt)s)")
        inputs = [ Pions, DownPions, Photons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, dependencies = [PV3D('Hlt2')])

class JpsiReFitPVsFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS('J/psi(1S)') < %(MassWindow)s) " +
                "& (PT > %(Pt)s) " +
                "& (MAXTREE('mu-' == ABSID,TRCHI2DOF) < %(TrChi2Tight)s )" +
                "& (MINTREE('mu-' == ABSID,PT) > %(MuPt)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s )")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_out', nbins = 25)
                                + " & " + Hlt2MonitorMinMax("PT","PT(#mu#mu)", 0, 10000, 'JPsiPT_out', nbins = 100)
                                + " & " + Hlt2MonitorMinMax("MINTREE('mu-' == ABSID, PT)", "MINTREE(mu-==ABSID, PT)", 0, 10000, 'MuPT_out', nbins = 100)
                                + " & " + Hlt2MonitorMinMax("VFASPF(VCHI2PDOF)", "VFASPF(VCHI2PDOF)", 0, 25, 'JPsiVeterxChi2_out', nbins = 100),
                                'ReFitPVs':True,
                                'CloneFilteredParticles':True}
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True, **args)


# Define additional algorithm that you want to add to the line
from Hlt2Lines.Utilities.Hlt2ExtraCombiner import Hlt2ExtraCombiner
class ExtraCombiner(Hlt2ExtraCombiner):
    """
    First decay descriptor of Hlt2ExtraCombiner is passed to normal CombineParticles,
    so it should follow these rules:
        - only fully described 1-step decay
        - only true particle names can appear in the descriptor
        - only "simple" arrow "->" can be used
        -  [...]cc (in lower case!)

    Second decay descriptor is passed to FilterDecays and is much less constrained:
        - use ^ to specify particles, that should be saved
        - use [...]CC in upper case
    """
    def __init__(self, name):
        ## Cut values are defined in __slots__ in Lines.py
        picut = ( "(PT > %(PT_daughter)s )")
        dc =    { 'pi+' : picut }
        cc =    ( "( (APT1 + APT2) > %(SumPT)s )")
        mc =    ( "( PT > %(BPT)s )")
        inputs = [ JpsiReFitPVsFilter('JpsiReFitPVsFilter') , Pions ]
        Hlt2ExtraCombiner.__init__(self, name,
                              ['[B+ -> J/psi(1S) pi+]cc'],
                              inputs,
                              DaughtersCuts = dc, CombinationCut = cc,
                              MotherCut = mc, Preambulo = [],
                              ParticlesToSave = "[B+ -> J/psi(1S) ^pi+]CC" , # New feature to save only selected branch of decay
                              )
