from Hlt2Lines.Utilities.Hlt2MergedStage import Hlt2MergedStage
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter, Hlt2ParticleFilter
from Inputs import Pions, DownPions, Photons
from HltTracking.HltPVs import PV3D

class CreateReco(Hlt2MergedStage):
    def __init__(self,name):
        inputs = [ Pions, DownPions, Photons ]
        Hlt2MergedStage.__init__(self, name, inputs, dependencies = [PV3D('Hlt2')])

class FilterOnPVs(Hlt2VoidFilter):
    def __init__(self,name):
        code = ("CONTAINS('%s')" % PV3D('Hlt2').output) + " == %(nPVs)s"
        Hlt2VoidFilter.__init__(self, name, code, [PV3D('Hlt2')],nickname = 'NPVs')

class SingleTrackFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(TRCHI2DOF < 10.)")
        inputs = [ Pions ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True)
