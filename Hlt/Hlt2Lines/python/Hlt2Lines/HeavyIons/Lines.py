from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser


class HeavyIonsLines(Hlt2LinesConfigurableUser):
    __slots__ = {
        'Prescale': {},
        'Postscale': {},
        'PersistReco': {"HighVeloMultTurbo": True},
        'NPVs'       : {"nPVs" :  1},
        'MBMicroBiasVelo': {
            'HLT1': "HLT_PASS('Hlt1BBMicroBiasVeloDecision')",
            'VoidFilter': '',
        },
        'MBMicroBiasLowMultVelo': {
            'HLT1': "HLT_PASS('Hlt1BBMicroBiasLowMultVeloDecision')",
            'VoidFilter': '',
        },
        'MBHighMult': {
            'HLT1': "HLT_PASS('Hlt1BBHighMultDecision')",
            'VoidFilter': '',
        },
        'BBHighMult': {
            'HLT1': "HLT_PASS('Hlt1BBVeryHighMultDecision')",
            'VoidFilter': '',
        },
        'BEHighMult': {
            'HLT1': "HLT_PASS('Hlt1BEVeryHighMultDecision')",
            'VoidFilter': '',
        },
        'BEMicroBiasLowMultVelo': {
            'HLT1': "HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision')",
            'VoidFilter': '',
        },
        'HighVeloMultTurbo': {
            'HLT1': "HLT_PASS_RE('^Hlt1HighVeloMult.*Decision$')",
            'VoidFilter': '',
        },
        'SMOGDiMuon': {
            'HLT1': "HLT_PASS_RE('^Hlt1SMOGDiMuon.*Decision$')",
            'VoidFilter': '',
        },
        'SMOGPassThrough': {
            'HLT1': "HLT_PASS_RE('Hlt1(BE|EB).*Decision')",
            'VoidFilter': '',
        },
        'BBSMOGPassThrough': {
            'HLT1': "HLT_PASS_RE('Hlt1BB(?!SMOG).*Decision')",
            'VoidFilter': '',
        },
        'BBPassThrough': {
            'HLT1': "HLT_PASS_RE('Hlt1BB.*Decision')",
            'VoidFilter': '',
        },
        'SingleTrack' : {
            "HLT1" :  "HLT_PASS_RE('Hlt1.*MicroBias.*Decision')",
			'L0Req':   "(L0_DATA('Spd(Mult)') < 50)"
            },
    }

    def __apply_configuration__(self):
        from Stages import CreateReco,FilterOnPVs, SingleTrackFilter
        stages = {
            'MBMicroBiasVelo': [],
            'MBMicroBiasLowMultVelo': [],
            'BEMicroBiasLowMultVelo': [],
            'MBHighMult': [],
            'BBHighMult': [],
            'BEHighMult': [],
            'HighVeloMultTurbo': [CreateReco("Hlt2HighVeloMultTurbo"),FilterOnPVs("Decision")],
            'SMOGDiMuon': [],
            'SMOGPassThrough': [],
            'BBSMOGPassThrough': [],
            'BBPassThrough': [],
            'SingleTrack': [SingleTrackFilter('SingleTrackFilter')],
        }

        from HltLine.HltLine import Hlt2Line
        for name, algos in self.algorithms(stages):
            localProps = self.getProps().get(name, {})
            linename = name
            doturbo = linename.endswith('Turbo')
            Hlt2Line(linename,
                     prescale=self.prescale,
                     postscale=self.postscale,
                     algos=algos,
                     ODIN = localProps.get('ODIN',None),
                     L0DU = localProps.get('L0Req', None),
                     HLT1=localProps.get('HLT1', None),
                     VoidFilter=localProps.get('VoidFilter', None),
                     priority=localProps.get('Priority', None),
                     Turbo=doturbo,
                     PersistReco=self.getProps()['PersistReco'].get(linename, False),
                     )
