__author__ = 'Mika Vesterinen'
__date__ = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Inputs import GoodPions, GoodKaons, VeloKaons, VeloPions , SlowPions, AllPions

#######################
# Global event filters
#######################
class TrackGEC(Hlt2VoidFilter):
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules = Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] :
            if i not in modules : modules.append(i)
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, "TrackEff" + name, code, [tracks], shared=True, nickname='TrackGEC')

################################
# Single track filters TAG
################################
class TrackEffTagFilter(Hlt2ParticleFilter):
    def __init__(self, name, pid, inputs):
        if pid == "K":
            pid_cut = "(PROBNNk > %(TagK_MinPROBNNk)s  )"
        elif pid == "Pi":
            pid_cut = "(PROBNNpi > %(TagPi_MinPROBNNpi)s  )"

        cut = "(PT > %(Tag_MinPT)s) & (P>%(Tag_MinP)s) & " + pid_cut + " & (MIPCHI2DV(PRIMARY) > %(Tag_MinIPCHI2)s) & (TRCHI2DOF <%(Tag_MaxTrChi2)s)"
        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared=True)

TagKaonsForTwoTrack = TrackEffTagFilter("D2KPi_TagK", "K", [GoodKaons]);
TagPionsForTwoTrack = TrackEffTagFilter("D2KPi_TagPi", "Pi", [GoodPions]);

TagPionsForDs = TrackEffTagFilter("Ds_TagPi", "Pi", [GoodPions]);
TagKaonsForPhi = TrackEffTagFilter("Phi_TagK", "K", [GoodKaons]);

TagKaonsForFourTrack = TrackEffTagFilter("D2K3Pi_TagK", "K", [GoodKaons]);
TagPionsForFourTrack = TrackEffTagFilter("D2K3Pi_TagPi", "Pi", [GoodPions]);


################################
# Track combinations TAG
################################
class PhiToKPiCombination(Hlt2Combiner):
    def __init__(self, name, inputs, shared=False):
        combCut = ("in_range( %(AMMIN)s, AM, %(AMMAX)s)"
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(PHITAGS_AMAXIPCHI2)s) "
                     + "& (AMAXCHILD(PT) > %(MAXCHILDMinPT)s) ")
        motherCut = ("(BPVVDZ > %(Phi_MinVDZ)s)"
                    + "& (VFASPF(VCHI2) < %(VCHI2)s)"
                    + "& (BPVVDCHI2 > %(VDCHI2)s)"
                    + "& ( BPVCORRM < %(MAXCORRM)s )")
        
        from HltTracking.HltPVs import PV3D
        
        Hlt2Combiner.__init__(self, name, ["[K*(892)0 -> K- pi+]cc"], #using the K* to tag the flavour of the pion
                              inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut, MotherCut=motherCut,
                              shared=shared
                              )

class K2piLongTracksCombiner(Hlt2Combiner):
    def __init__(self, name, inputs):
        combCut = (" (ANUM(PT > %(Kst_MinAPTTwoDaughters)s)>2) "
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(Kst_MAXCHILDMinIPCHI2)s) "
                     + "& (AMAXCHILD(PT) > %(Kst_MAXCHILDMinPT)s) "
                     + "& (APT > %(Kst_MinAPT)s)"
                     + "& (AMAXDOCA('') < %(D0_MaxDOCA)s)"
                     + "& (in_range( %(Kst_MinAM)s,AM, %(Kst_MaxAM)s ))")

        motherCut = ("(BPVVDZ > %(D0_MinVDZ)s) & (BPVVDCHI2 > %(D0_MinVDCHI2)s)"  
                     + "& (VFASPF(VCHI2/VDOF) < %(Kst_MaxVCHI2NDF)s)"
                     + "& (MIPCHI2DV(PRIMARY) > %(Kst_MinIPCHI2)s)")
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, ['[K*(892)+ -> K- pi+ pi+]cc',
                                          '[K*(892)+ -> K- pi+ pi-]cc',
                                          '[K*(892)+ -> K- pi- pi-]cc'],
                              inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut,
                              MotherCut=motherCut)

class KStarToKKCombination(Hlt2Combiner):
    def __init__(self, name, inputs):
        combCut = ("(AM<1800*MeV) & (AM>1100*MeV)"
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(KSTARTAGS_AMAXIPCHI2)s) ")
        motherCut = ("(BPVVDZ > %(Kstar_MinVDZ)s)"
                    + "& (VFASPF(VCHI2) < %(VCHI2)s)"
                    + "& (BPVVDCHI2 > %(VDCHI2)s)")
        
        from HltTracking.HltPVs import PV3D
        
        Hlt2Combiner.__init__(self, name, ["[phi(1020) -> K- K+]cc"], #use a phi(1020) internally just for convenience - K* 892 is too light to decay to KK
                              inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut, MotherCut=motherCut
                              )

################################
# Tag+Probe combinations
################################
class D0ToKpiCombiner(Hlt2Combiner):
    def __init__(self, name, inputs, shared):
        combCut = ("(in_range( %(D0_MinAM)s, AM, %(D0_MaxAM)s ))"
                     + "& (AMAXDOCA('') < %(D0_MaxDOCA)s)"
                     + "& (ACUTDOCACHI2(%(D0_MaxDOCACHI2)s, ''))"
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(D0_AMAXIPCHI2)s) "
                     + "& (AOVERLAP(1,2, LHCb.LHCbID.Velo) < %(D0_MaxOverlap)s)")

        motherCut = ("(BPVVDZ > %(D0_MinVDZ)s) & (BPVVDCHI2 > %(D0_MinVDCHI2)s)"
                     + "& (math.log(BPVVD)< %(D0_MaxLogVD)s) "
                     + "& (VFASPF(VCHI2/VDOF)< %(D0_MaxVCHI2NDF)s)"
                     + "& (abs(CHILD(1, ETA) - CHILD(2, ETA)) < %(D0_MaxDeltaEta)s)"
#                     + "& (math.log(D0_MASS_CONSTRAINT_IP) < %(D0_MCONST_MaxLogIPCHI2)s)"
#                     + "& (math.log(D0_IP_PERP) < %(D0_MaxLogIPPerp)s)"
                     + "& (in_range( %(D0_MinSimpleFitM)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(D0_MaxSimpleFitM)s ))")

        from HltTracking.HltPVs import PV3D
        from Preambulo import D0Preambulo
        Hlt2Combiner.__init__(self, name, ["[D0 -> K- pi+]cc",
                                          "[D0 -> K- pi-]cc"], inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut, MotherCut=motherCut,
                              shared=shared,
                              Preambulo=D0Preambulo())

class D0ToK3piCombiner(Hlt2Combiner):
    def __init__(self, name, inputs):
        combCut = ("(in_range( %(D0_MinAM)s,AM, %(D0_MaxAM)s ))"
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(D0_AMAXIPCHI2)s) "
                     + "& (AMAXDOCA('') < %(D0_MaxDOCA)s)")
        motherCut = ("(BPVVDZ > %(D0_MinVDZ)s) & (BPVVDCHI2 > %(D0_MinVDCHI2)s) & (BPVDIRA > 0.99)"
                     + "& (VFASPF(VCHI2/VDOF)< %(D0_MaxVCHI2NDF)s)" 
                     + "& (abs(CHILD(1, ETA) - CHILD(2, ETA)) < %(D0_MaxDeltaEta)s)"
                     + "& (math.log(D0_MASS_CONSTRAINT_IP) < %(D0_MCONST_MaxLogIPCHI2)s)"
                     + "& (math.log(D0_IP_PERP) < %(D0_MaxLogIPPerp)s)"
#                     + "& (math.sqrt(mass_constraint_d0_momentum_vector[0]**2+mass_constraint_d0_momentum_vector[1]**2) > 1500)" # MinPT
                     + "& (in_range( %(D0_MinSimpleFitM)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(D0_MaxSimpleFitM)s ))")

        from HltTracking.HltPVs import PV3D
        from Preambulo import D0Preambulo
        
        # The K*+ always has a K- daughter, which tags the D0 decay. 
        Hlt2Combiner.__init__(self, name, ["[D0 -> K*(892)+ pi+]cc",
                                           "[D0 -> K*(892)+ pi-]cc"] , inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut, MotherCut=motherCut,
                              Preambulo=D0Preambulo())

class DstTagger(Hlt2Combiner):
    def __init__(self, name, inputs, shared=False):
        DstCombinationCut = "(APT > %(Dst_MinAPT)s )"
        DstMotherCut = "ALL"
        DaughtersCuts = {'pi+' : "(PT > %(Slowpi_MinPt)s) & (PROBNNghost < 0.4) & (MIPCHI2DV(PRIMARY) < 10)"}
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, ["[D*(2010)+ -> D0 pi+]cc"], inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              CombinationCut=DstCombinationCut,
                              MotherCut=DstMotherCut, 
                              shared=shared,
                              DaughtersCuts=DaughtersCuts,)

class DstCloneKiller(Hlt2ParticleFilter):
    def __init__(self, name, inputs, shared=False):
        cut = "(MAXOVERLAP( (ABSID == 'pi+') | (ABSID=='K+') ) < %(D0_MaxOverlap)s)"
        
        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared=shared)

class DstPVDeltaMassFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, shared=False):
        """ The monitoring of the pointing delta-mass """
        from HltLine.Hlt2Monitoring import Hlt2MonitorMinMax
        from Preambulo import DstPreambulo
        
        cut = "((Dst_M - D0_M) < %(Dst_MaxSimpleFitDeltaMass)s)"
        args = {'PreMonitor'  : (Hlt2MonitorMinMax("(Dst_M - D0_M)", "D^{*} M_{PV Constraint} - D^{0} M_{PV Constraint} [MeV]", 100, 225, "Pointing_DeltaMass_in", nbins=50)),
                'PostMonitor'  :(Hlt2MonitorMinMax("(Dst_M - D0_M)", "D^{*} M_{PV Constraint} - D^{0} M_{PV Constraint} [MeV]", 100, 225, "Pointing_DeltaMass_out", nbins=50))
                }
        
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    Preambulo=DstPreambulo(),
                                    shared=shared,
                                    **args
                                    )

class DstDTFFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, shared=False):
        from HltLine.Hlt2Monitoring import Hlt2MonitorMinMax
        cut = "(DTF_FUN ( M , True ,  'D0' , %(Dst_MaxDTFCHI2NDF)s , %(Dst_MaxDTFM)s+1.0 ) < %(Dst_MaxDTFM)s)"
        args = {'PreMonitor'  : (Hlt2MonitorMinMax("DTF_FUN(M,True,'D0')", "D^{*} M_{DTF}(D^0,PV) [MeV]", 2000, 2060, "Dst_DTF_M_in", nbins=50)
                                 + "&" + Hlt2MonitorMinMax("DTF_CHI2NDOF(True,'D0')", "DecayTreeFitter D^{*} #chi^{2}/ndf", 0, 20, "Dst_DTF_CHI2_in", nbins=50)),
                'PostMonitor'  : (Hlt2MonitorMinMax("DTF_FUN(M,True,'D0')", "DecayTreeFitter D^{*} mass [MeV]", 2000, 2060, "Dst_DTF_M_out", nbins=50)
                                  + "&" + Hlt2MonitorMinMax("DTF_CHI2NDOF(True,'D0')", "DecayTreeFitter D^{*} #chi^{2}/ndf", 0, 20, "Dst_DTF_CHI2_out", nbins=50))
                }
        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared=shared, **args)

class DsToPhiKCombiner(Hlt2Combiner):
    def __init__(self, name, inputs, shared=False):
        combCut = ( " (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(Ds_AMAXIPCHI2)s) "
                     + "& (ADOCAMAX('') < %(Ds_MaxDOCA)s)"
                     )
        motherCut = (  "(BPVVDZ > %(Ds_MinVDZ)s) & (BPVVDZ < %(Ds_MaxVDZ)s) & (BPVVDCHI2 > %(Ds_MinVDCHI2)s) & (BPVDIRA > %(DIRA)s)"
                     + "& (VFASPF(VCHI2/VDOF)< %(Ds_MaxVCHI2NDF)s)" 
                     + "& (in_range( %(Phi_M_PV_FitMin)s, Phi_M_from_PV_constraint, %(Phi_M_PV_FitMax)s))"
                     + "& (in_range( %(Ds_MinSimpleFitM)s, DsMassFromConstraint, %(Ds_MaxSimpleFitM)s ))"
                     + "& (in_range( %(DELTA_MASS_MIN)s, Ds_M_from_PV_constraint-Phi_M_from_PV_constraint, %(DELTA_MASS_MAX)s))"
                     )

        from HltTracking.HltPVs import PV3D
        from Preambulo import DsPreambulo
        
        # This is a K* to enable access to the charge of the
        # tag kaon.
        Hlt2Combiner.__init__(self, name, ["[D_s+ -> K*(892)0 K+]cc"] ,
                              inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut, MotherCut=motherCut,
                              Preambulo=DsPreambulo() + 
                                    			 ["mHist_phi_PV     = Gaudi.Histo1DDef('Phi_M_from_PV_constraint'   , 980 * MeV , 1100  * MeV, 100)",
                                                  "mHist_Ds_M_PV    = Gaudi.Histo1DDef('Ds_M_from_PV_constraint'     , 1270 * MeV , 2570 * MeV, 100)"
                                                 ],
                              shared=shared)


class DsToKstarPiCombiner(Hlt2Combiner):
    def __init__(self, name, inputs):
        combCut = (" (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(Ds_AMAXIPCHI2)s) "
                     + "& (AMAXDOCA('') < %(Ds_MaxDOCA)s)")
        motherCut = (  "(BPVVDZ > %(Ds_MinVDZ)s) & (BPVVDCHI2 > %(Ds_MinVDCHI2)s) & (BPVDIRA > %(DIRA)s)"
                     + "& (VFASPF(VCHI2/VDOF)< %(Ds_MaxVCHI2NDF)s)" 
                     + "& (in_range( %(Ds_MinSimpleFitM)s, DsMassFromConstraint, %(Ds_MaxSimpleFitM)s ))"
                     + "& (in_range( %(Phi_M_PV_FitMin)s, Phi_M_from_PV_constraint, %(Phi_M_PV_FitMax)s))"
                     + "& (in_range( %(DELTA_MASS_MIN)s, Ds_M_from_PV_constraint-Phi_M_from_PV_constraint, %(DELTA_MASS_MAX)))"
                     )

        from HltTracking.HltPVs import PV3D
        from Preambulo import DsPreambulo
        
        # The K*+ always has a K- daughter, which tags the D0 decay. 
        Hlt2Combiner.__init__(self, name, ["[D_s+ -> phi(1020) pi+]cc"] ,
                              inputs,
                              dependencies=[TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos='TisTosSpec',
                              CombinationCut=combCut, MotherCut=motherCut,
                              Preambulo=DsPreambulo(TwoBodyMass=890.,
                                                    ProbeM=139.6))


#### pion probe
TagPions = TrackEffTagFilter('D2KPi_TagPi', pid='Pi', inputs=[GoodPions])

D0ToKpiPionProbe = D0ToKpiCombiner('D2KPi_D0ForPionProbe', 
                                   inputs=[TagKaonsForTwoTrack, VeloPions],
                                   shared=True)
DstD0ToKpiPionProbe = DstTagger('Dst', 
                                inputs=[D0ToKpiPionProbe, SlowPions],
                                shared=True)
CloneKilledDstD0KPiPionProbe = DstCloneKiller('DstNoClones', 
                                              inputs=[DstD0ToKpiPionProbe],
                                              shared=True)
PreFilteredDstD0ToKpiPionProbe = DstPVDeltaMassFilter('DstDMPVConForPionProbe', 
                                                      [CloneKilledDstD0KPiPionProbe],
                                                      shared=True)
FilteredDstD0ToKpiPionProbe = DstDTFFilter('DstDTFForPionProbe', 
                                           inputs=[PreFilteredDstD0ToKpiPionProbe],
                                           shared=True)

##
## Add the probe long tracks to a D* decay
##
from Hlt2Lines.Utilities.Hlt2ExtraCombiner import Hlt2ExtraCombiner
class ExtraProbeSelectionDstar(Hlt2ExtraCombiner):
    def __init__(self,name, inputs):
        mc =  "(MAXOVERLAP( (ABSID == 'pi+') | (ABSID == 'K+') ) > %(MINOVERLAPPROBE)s)"
        
        Hlt2ExtraCombiner.__init__(self,name,
                                   ["[D_s+ -> D*(2010)+ pi+]cc",
                                    "[D_s+ -> D*(2010)+ pi-]cc"
                                    ],
                                   inputs,
                                   CombinationCut = "AALL",
                                   MotherCut = mc,
                                   ParticleCombiners={'':'ParticleAdder'},
                                   ParticlesToSave = "[D_s+ -> D*(2010)+ ^[pi+]CC]CC"
                                   )

##### kaon probe
D0ToKpiKaonProbe = D0ToKpiPionProbe.clone('D2KPi_D0ForKaonProbe',
                                            decay=["[D0 -> pi+ K-]cc",
                                                     "[D0 -> pi+ K+]cc"],
                                            inputs=[TagPionsForTwoTrack,
                                                      VeloKaons])
DstD0ToKpiKaonProbe = DstTagger('Dst', 
                                inputs=[D0ToKpiKaonProbe, SlowPions])
CloneKilledDstD0KPiKaonProbe = DstCloneKiller('DstNoClones', 
                                           inputs=[DstD0ToKpiKaonProbe])
PreFilteredDstD0ToKpiKaonProbe = DstPVDeltaMassFilter('DstDMPVConForKaonProbe', 
                                           inputs=[CloneKilledDstD0KPiKaonProbe])
FilteredDstD0ToKpiKaonProbe = DstDTFFilter('DstDTFForKaonProbe', 
                                           inputs=[PreFilteredDstD0ToKpiKaonProbe])

#### pion probe, 4-body
ThreeLongTrackCombinations = K2piLongTracksCombiner('D2K3Pi_KStar',
                                                     inputs=[TagPionsForFourTrack,
                                                               TagKaonsForFourTrack])
D0ToK3piPionProbe = D0ToK3piCombiner('D2K3Pi_D0ForFourBody', inputs=[ ThreeLongTrackCombinations,
                                                                      VeloPions])
DstD0ToK3piPionProbe = DstTagger('D2K3Pi_Dst', inputs=[D0ToK3piPionProbe, SlowPions])
CloneKilledDstFourBodyPionProbe = DstCloneKiller('D2K3Pi_DstCloneKiller', inputs=[DstD0ToK3piPionProbe])
FilteredDstD0ToK3piPionProbe = DstPVDeltaMassFilter('D2K3Pi_DstPVFilter', inputs=[CloneKilledDstFourBodyPionProbe])
# The DTF filter fails, as the K* hypothesis is still there... it would be great
# if we could use our recombiner in the trigger.

# ## Kaon probe, PHI
Phi2KPiCombo = PhiToKPiCombination("Phi2KPi", inputs=[TagPionsForDs,
                                                      TagKaonsForPhi], shared=True)
Ds2PhiKCombo = DsToPhiKCombiner("Ds2PhiPi_KaonProbe", inputs=[Phi2KPiCombo,
                                                                 VeloKaons], shared=True)
FilteredDs2PhiPiKaonProbe = DstCloneKiller('Ds_CloneKiller', inputs=[Ds2PhiKCombo], shared=True)

class ExtraProbeSelectionDs(Hlt2ExtraCombiner):
    def __init__(self,name, inputs):
        mc =  "(MAXOVERLAP( (ABSID == 'pi+') | (ABSID == 'K+') ) > %(MINOVERLAPPROBE)s)"
        
        Hlt2ExtraCombiner.__init__(self,name,
                                   ["[B0 -> D_s+ K+]cc",
                                    "[B0 -> D_s+ K-]cc"
                                    ],
                                   inputs,
                                   CombinationCut = "AALL",
                                   MotherCut = mc,
                                   ParticleCombiners={'':'ParticleAdder'},
                                   ParticlesToSave = "[B0 -> D_s+ ^[K+]CC]CC"
                                   )


### Pion probe, K*(892)?
KStar2KKCombo = KStarToKKCombination("Kstar2KK", inputs=[TagKaonsForPhi])
Ds2KStarKKCombo = DsToKstarPiCombiner("Ds2KStarK_PionProbe", inputs=[KStar2KKCombo, GoodPions])
FilteredDs2KStarKPionProbe = DstCloneKiller('Ds_KStarK_CloneKiller', inputs=[Ds2KStarKKCombo])
