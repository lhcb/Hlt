################################################################################
# Package: HltLuminosity
################################################################################
gaudi_subdir(HltLuminosity)

gaudi_depends_on_subdirs(Calo/CaloUtils
                         Event/DAQEvent
                         Event/HltEvent
                         Event/L0Event
                         Event/LumiEvent
                         Event/TrackEvent
                         GaudiAlg
                         Hlt/HltBase
                         Tr/TrackInterfaces)

find_package(AIDA)
find_package(Boost)

find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(HltLuminosity
                 src/*.cpp
                 INCLUDE_DIRS AIDA Boost Event/LumiEvent Tr/TrackInterfaces
                 LINK_LIBRARIES Boost CaloUtils DAQEventLib HltEvent L0Event TrackEvent GaudiAlgLib HltBase)


gaudi_add_test(QMTest QMTEST)
