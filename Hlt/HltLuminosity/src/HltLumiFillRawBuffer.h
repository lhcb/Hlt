#ifndef HLTLUMIFILLRAWBUFFER_H
#define HLTLUMIFILLRAWBUFFER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/RawEvent.h"

/** @class HltLumiFillRawBuffer HltLumiFillRawBuffer.h
 *  Fills the Raw Buffer banks for the LumiSummary
 *
 *  @author Jaap Panman
 *  @date   2004-07-22
 */
class HltLumiFillRawBuffer : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    HltLumiFillRawBuffer( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    void fillDataBankShort();
    std::string m_inputBank;

    // Statistics
    double m_totDataSize = 0;
    int m_nbEvents = 0;
    std::vector<unsigned int> m_bank;
};
#endif // HLTLUMIFILLRAWBUFFER_H
