#ifndef LUMIPUTRESULT_H
#define LUMIPUTRESULT_H 1

// Include files
#include <memory>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class LumiPutResult LumiPutResult.h
 *
 *
 *  @author Jaap Panman
 *  @date   2008-08-27
 */
class LumiPutResult : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    LumiPutResult( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    std::string m_InputContainerName;
    unsigned int m_size;
    std::vector<double> m_means;
    std::vector<double> m_thresholds;
    std::vector<unsigned int> m_infoKeys;
};
#endif // LUMIPUTRESULT_H
