import collections
import logging

log = logging.getLogger(__name__)

#########################################################################################
# Utility function for setting thresholds both in Hlt1 and 2
#
def setThresholds(ThresholdSettings,conf_):
    """
    Look in ThresholdSettings for configurable conf_
    and set the appropriate settings
    
    @author G. Raven, P. Koppenburg
    @date 23/7/2009 (moved)
    """
    from HltLine.HltLinesConfigurableUser import HltLinesConfigurableUser
    assert issubclass(conf_,HltLinesConfigurableUser) , '%s is not an HltLinesConfigurableUser'%i.__name__
    conf = conf_()  # The configurable _must_ be instantiated even if not explicitly configured. Or it will be ignored
    for (k,v) in ThresholdSettings.get(conf_, dict()).iteritems() :
        if hasattr(conf,k) and v==getattr(conf,k) : continue # nothing to do, move on to next item...

        if conf.isPropertySet(k):
            log.warning('%s.%s has explicitly been set, NOT using requested predefined threshold %s, but keeping explicit value: %s '%(conf.name(),k,str(v),getattr(conf,k)))
        else:
            if isinstance(v, dict):
                # update (instead of overwrite) dictionary-valued properties
                try:
                    val = getattr(conf, k)
                except KeyError:
                    log.warning("Cannot get property {} of Configurable {}"
                                .format(k, conf.getName()))
                    raise
                val.update(v)
                setattr(conf, k, val)
            else:
                setattr(conf, k, v)

def _recursive_update(x, other, path=''):
    """Recursively update dictionary values."""
    for key, value in other.iteritems():
        if isinstance(value, collections.Mapping):
            x[key] = _recursive_update(x.get(key, {}), value,
                                       path=(path + '[{!r}]'.format(key)))
        else:
            try:
                if x[key] != value:
                    log.warning('Updating {}[{!r}]\n    old: {}\n    new: {}'
                                .format(path, key, x[key], value))
            except KeyError:
                pass
            x[key] = value
    return x

def overwriteThresholds(thresholdSettings, otherSettings):
    """Overwrite threshold settings with other settings.

    Args:
        thresholdSettings: The original settings, modified in place
        otherSettings: Settings in the usual structure with only the
            values to be updated.

    Example:
        To update Param3 in TrackMVA in Hlt1MVALinesConf use:
        otherSettings = {
            Hlt1MVALinesConf: {
                'TrackMVA': {
                    'Param3': 2,
                },
            },
        }
    """
    _recursive_update(thresholdSettings, otherSettings)

def Name2Threshold(name) :
    if not hasattr(Name2Threshold,'_dict') : Name2Threshold._dict = {}
    if name not in Name2Threshold._dict : 
        log.warning(' '+'#'*(41+len(name)) )
        log.warning(' ## using trigger threshold settings "%s" ##'%name)
        try :
            mod = getattr( __import__('HltSettings.%s' % name ), name )
            cls = getattr( mod, name )
        except (AttributeError,ImportError) as error:
            log.error('   ## Unknown ThresholdSetting "%s" -- please fix Moore().ThresholdSetting ##' % name )
            raise error
        Name2Threshold._dict[name] = cls()
        #log.warning(' ## type is %-30s ##' % Name2Threshold._dict[name].HltType() )
        log.warning(' '+'#'*(41+len(name)) )
    return Name2Threshold._dict[name]

def importLineConfigurables(pkg, endsWithLines = False) :
    # import all modules in Hlt{1,2}Lines
    # For Hlt1 do the equivalent of:
    #    import Hlt1Lines.HltXSomeLines, ...
    #    return [ Hlt1Lines.HltXSomeLines.HltXSomeLinesConf , ... ]
    # For Hlt2 do the equivalent of:
    #    import Hlt2Lines.SomeFolder.Lines, ...
    #    return [ Hlt2Lines.SomeFolder.Lines.SomeLines , ... ]
    import os.path, pkgutil, importlib
    old_style = [getattr(importlib.import_module(pkg.__name__ + '.' + name), name + 'Conf')
                 for _, name, is_pkg in pkgutil.iter_modules(pkg.__path__[:2])
                 if (not endsWithLines or (endsWithLines and name.endswith('Lines'))) and not is_pkg]
    new_style = []
    for _, name, is_pkg in pkgutil.iter_modules(pkg.__path__[:2]):
        if not is_pkg or name == "Utilities":
            continue
        pkg_name = pkg.__name__ + '.' + name + '.Lines'
        conf = name + 'Lines'
        try:
            mod = getattr(importlib.import_module(pkg_name), conf)
            new_style += [mod]
        except ImportError as e:
            from Gaudi.Configuration import log
            log.error("Could not import configurable %s from package %s", pkg_name, conf)
            raise e
            
    return old_style + new_style
