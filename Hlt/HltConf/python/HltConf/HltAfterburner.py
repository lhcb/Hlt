"""HLT Afterburner configuration."""
from itertools import chain
import logging
import os
from pprint import pformat
import re
import types
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import mm
from LHCbKernel.Configuration import *
from Configurables import (
    CaloClusterCloner,
    CaloHypoCloner,
    ChargedProtoParticleAddVeloInfo,
    CopyLinePersistenceLocations,
    CopyParticle2PVRelationsFromLinePersistenceLocations,
    CopyParticle2RelatedInfoFromLinePersistenceLocations,
    CopyPrimaryVertices,
    GaudiSequencer as Sequence,
    HltLinePersistenceSvc,
    LoKi__HDRFilter as HltFilter,
    LoKi__VoidFilter as Filter,
    ProtoParticleCloner,
    TrackBuildCloneTable,
    TrackCloneCleaner,
    TrackClonerWithClusters,
    TrackToDST,
    RecSummaryAlg,
    Rich__Future__RawBankDecoder as RichDecoder
)

from DAQSys.Decoders import DecoderDB

from HltTracking.HltPVs import PV3D
from HltTracking.HltTrackNames import Hlt2TrackLoc
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedDownstreamTracking
from HltTracking.HltVertexNames import Hlt3DPrimaryVerticesName as PV3DSelection
from HltTracking.HltVertexNames import HltSharedVerticesPrefix, HltGlobalVertexLocation
from HltTracking.HltVertexNames import _vertexLocation
from HltTracking.Hlt2AllTrackings import Hlt2AllTrackings

from HltPersistReco import HltPersistRecoConf

from ThresholdUtils import importLineConfigurables
from HltLine.HltLine import hlt2Lines
import Hlt2Lines
from Hlt2Lines.Utilities.Utilities import uniqueEverseen

__author__ = "R. Aaij roel.aaij@cern.ch"

log = logging.getLogger(__name__)


class HltAfterburnerConf(LHCbConfigurableUser):
    # python configurables to be applied before me
    __queried_configurables__ = [
        Hlt2AllTrackings,
    ] + importLineConfigurables(Hlt2Lines)
    # We need the dependency on the Hlt2 lines above as we need to inspect
    # all of them for the PersistReco flag in order to create the filter

    # python configurables that I configure
    __used_configurables__ = [
        HltPersistRecoConf,
    ]

    __slots__ = {"Sequence"                : None,
                 "PersistedReconstructionPrefix": "Turbo",
                 "EnableHltRecSummary"     : True,
                 "RecSummaryLocation"      : "Hlt2/RecSummary",
                 "AddAdditionalTrackInfos" : True,
                 "Hlt2Filter"              : "HLT_PASS_RE('Hlt2(?!Forward)(?!DebugEvent)(?!Lumi)(?!Transparent)(?!PassThrough).*Decision')",
                 "Simulation"              : False,
                }

    def _persistRecoLines(self):
        return [line for line in hlt2Lines() if line.persistReco()]

    def _persistRecoFilterCode(self, lines):
        decisions = [line.decision() for line in lines]
        # code = ' | '.join(["HLT_TURBOPASS('{}')".format(i) for i in decisions])
        # if not code: code = 'HLT_NONE'
        # There is no HLT_TURBOPASS functor, so need to use regexps:
        code = "HLT_TURBOPASS_RE('^({})$')".format('|'.join(decisions))
        return code

    def __pvTrackLocation(self):
        # NOTE we need to import FittedVelo here, otherwise the FastVeloTracking
        #      instance is created before any defaults are overridden!
        from HltTracking.HltSharedTracking import FittedVelo
        return FittedVelo.outputSelection()

    def __pvLocation(self):
        pvLocation = _vertexLocation(HltSharedVerticesPrefix, HltGlobalVertexLocation, PV3DSelection)
        return pvLocation

    def _hlt2FullReconstructionLocations(self):
        longTracking = Hlt2BiKalmanFittedForwardTracking()
        downstreamTracking = Hlt2BiKalmanFittedDownstreamTracking()
        caloHypoPrefix = longTracking._caloIDLocation(withSuffix = True)

        def digits_location(det):
            """Return CaloDigit container location for the given subdetector."""
            assert det in ["Ecal", "Hcal", "Prs", "Spd"]
            decoder_name = "CaloDigitsFromRaw/{0}FromRaw".format(det)
            decoder_locs = DecoderDB[decoder_name].listOutputs()
            digit_locs = [l for l in decoder_locs if "digit" in l.lower()]
            assert len(digit_locs) == 1, "There should be one Digits location"
            return digit_locs[0]

        def caloadcs_location(det):
            return digits_location(det).replace('/Digits', '/Adcs')

        def clusters_location(det):
            assert det in ["Velo", "TT", "IT"]
            if det == "Velo":
                decoder_name = "DecodeVeloRawBuffer"
            else:
                decoder_name = "RawBankToSTClusterAlg"
            decoder_name += "/create{0}Clusters".format(det)
            decoder_locs = DecoderDB[decoder_name].listOutputs()
            assert len(decoder_locs) == 1, "There should be one {0}Cluster location".format(det)
            return decoder_locs[0]

        locations = {
            "Hlt2LongProtos":                longTracking.hlt2ChargedAllPIDsProtos().outputSelection(),
            "Hlt2DownstreamProtos":          downstreamTracking.hlt2ChargedAllPIDsProtos().outputSelection(),
            "Hlt2LongRichPIDs":              longTracking.hlt2RICHID().outputSelection(),
            "Hlt2DownstreamRichPIDs":        downstreamTracking.hlt2RICHID().outputSelection(),
            "Hlt2MuonPIDs":                  longTracking.hlt2MuonID().outputSelection(),
            "Hlt2MuonPIDSegments":           longTracking._trackifiedMuonIDLocation(),
            "Hlt2LongTracks":                longTracking.hlt2PrepareTracks().outputSelection(),
            "Hlt2DownstreamTracks":          downstreamTracking.hlt2PrepareTracks().outputSelection(),
            "Hlt2VeloPVTracks":              self.__pvTrackLocation(),
            "Hlt2VeloClusters":              clusters_location("Velo"),
            "Hlt2TTClusters":                clusters_location("TT"),
            "Hlt2ITClusters":                clusters_location("IT"),
            "Hlt2RecVertices":               self.__pvLocation(),
            "Hlt2NeutralProtos":             longTracking.hlt2NeutralProtos().outputSelection(),
            "Hlt2CaloClusters":              "Hlt/Calo/EcalClusters",
            "Hlt2EcalSplitClusters":         caloHypoPrefix + "/EcalSplitClusters",
            "Hlt2CaloElectronHypos":         caloHypoPrefix + "/Electrons",
            "Hlt2CaloPhotonHypos":           caloHypoPrefix + "/Photons",
            "Hlt2CaloMergedPi0Hypos":        caloHypoPrefix + "/MergedPi0s",
            "Hlt2CaloSplitPhotonHypos":      caloHypoPrefix + "/SplitPhotons",
            "Hlt2CaloEcalDigits":            digits_location("Ecal"),
            "Hlt2CaloHcalDigits":            digits_location("Hcal"),
            "Hlt2CaloPrsDigits":             digits_location("Prs"),
            "Hlt2CaloSpdDigits":             digits_location("Spd"),
            "Hlt2CaloEcalAdcs":              caloadcs_location("Ecal"),
            "Hlt2CaloHcalAdcs":              caloadcs_location("Hcal"),
            "Hlt2CaloPrsAdcs":               caloadcs_location("Prs"),
            "Hlt2CaloSpdAdcs":               caloadcs_location("Spd")
        }

        for loc in locations:
            if not locations[loc].startswith('/Event/'):
                locations[loc] = '/Event/' + locations[loc]

        return locations


    def _hlt2LineOutputs(self, outputPrefix):
        """Return dict of Turbo line names to a list of location 2-tuples.

        For each line, the 2-tuple has the form
            (source_location, cloned_location)
        where source_location is an output location of the line, and
        cloned_location is the location the output should be cloned to.
        """
        validPrefixFormat = (
            outputPrefix.startswith('/') and
            not outputPrefix.endswith('/')
        )
        assert validPrefixFormat, 'Prefix must begin with a slash and not end with a slash, got {0!r}'.format(outputPrefix)

        def prefix(loc, prefix='/Event'):
            """Add a prefix to a location."""
            return (prefix + '/' +
                    re.sub('^({0}|/Event)/'.format(prefix), '', loc))

        def sel_locations(sel):
            assert sel.endswith('/Particles')

            # Algorithms that create '/Particle' locations can also create some
            # additional locations under the same parent path; we want those too
            sneakySuffixes = ['decayVertices', '_RefitPVs', 'Particle2VertexRelations']

            locations = []
            for suffix in ['Particles'] + sneakySuffixes:
                loc = re.sub('/Particles$', '/' + suffix, sel)
                locations.append((prefix(loc), prefix(loc, outputPrefix)))
            return locations

        pv_location = self.__pvLocation()
        pv_locations = [
            (prefix(pv_location), prefix(pv_location, outputPrefix))]
        full_reco_locations = [
            (prefix(loc), prefix(loc, outputPrefix))
            for loc in self._hlt2FullReconstructionLocations().values()]

        line_locations = {}
        for line in hlt2Lines():
            if not line._Turbo:
                continue
            locations = []
            if line.persistReco():
                # Full HLT2 reconstruction
                locations += full_reco_locations
            else:
                # All Turbo lines get all PVs
                locations += pv_locations
            # Line candidates
            locations += sel_locations(line.outputSelection())
            # Extra algorithms
            for extra_selection in line.extraAlgosSelections():
                locations += sel_locations(extra_selection)

            line_locations[line.decision()] = locations

        return line_locations

    def _trackStateCutterSeq(self):
        """Return sequence that reduces the number of states on all tracks."""
        tracksLong = Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks()
        tracksDown = Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks()

        longStateCutter = TrackToDST("Hlt2TrackToDSTLong")
        longStateCutter.TracksInContainer = tracksLong.outputSelection()
        downStateCutter = TrackToDST("Hlt2TrackToDSTDown")
        downStateCutter.TracksInContainer = tracksDown.outputSelection()
        fittedVeloTracksCutter = TrackToDST("Hlt2TrackToDSTVeloPV",
                                            veloStates=["ClosestToBeam"])
        fittedVeloTracksCutter.TracksInContainer = self.__pvTrackLocation()

        seq = Sequence('HltPersistRecoTrackStateCutterSeq')
        seq.Members = [
            longStateCutter,
            downStateCutter,
            fittedVeloTracksCutter
        ]
        return seq

    def _hlt2FullReconstructionSeq(self):
        """Return sequence that runs the full HLT2 reconstruction."""
        pvReco = PV3D("Hlt2")
        longProtos = Hlt2BiKalmanFittedForwardTracking().hlt2ChargedAllPIDsProtos()
        downstreamProtos = Hlt2BiKalmanFittedDownstreamTracking().hlt2ChargedAllPIDsProtos()
        neutralProtos = Hlt2BiKalmanFittedForwardTracking().hlt2NeutralProtos()

        return Sequence(
            "HltPersistRecoCreateProtos",
            Members=list(uniqueEverseen(chain.from_iterable([
                longProtos,
                downstreamProtos,
                neutralProtos,
                pvReco
            ]))),
            ModeOR=True,
            ShortCircuit=False
        )

    def _fullPersistRecoLineFilter(self):
        """Return HltFilter that only passes if >= 1 PersistReco line fired."""
        lines = self._persistRecoLines()
        log.info('# List of requested PersistReco lines: {}'.format(
            [line.name() for line in lines]
        ))

        decoder = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"]
        lineFilter = HltFilter(
            "Hlt2PersistRecoLineFilter",
            Code=self._persistRecoFilterCode(lines),
            Location=decoder.listOutputs()[0]
        )
        return lineFilter

    def _turboLineFilter(self):
        """Return HltFilter that only passes if >= 1 Turbo line fired."""
        decoder = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"]
        lineFilter = HltFilter(
            "Hlt2TurboLineFilter",
            Code="HLT_TURBOPASS_RE('^Hlt2.*Decision$')",
            Location=decoder.listOutputs()[0]
        )
        return lineFilter

    def _decodeFullClustersSeq(self):
        """Decode the full VELO, IT, and TT clusters so we can persist them."""
        decodeFullClustersSeq = Sequence("DecodeFullClustersSequence")
        decodeFullClustersSeq.Members = [
            DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].setup(),
            DecoderDB["RawBankToSTClusterAlg/createTTClusters"].setup(),
            DecoderDB["RawBankToSTClusterAlg/createITClusters"].setup()
        ]
        decodeFullClustersSeq.IgnoreFilterPassed = True
        return decodeFullClustersSeq

    def _persistRecoSeq(self):
        """Return sequence to configure the persistence of HLT2 objects.

        The general idea is that this sequence should clone everything that
        should be persisted under the PersistedReconstructionPrefix, and then
        everything under that prefix is packed and serialised by the
        HltPersistReco configuration.

        The sequence runs in these steps:

            1. Reduce the number of states on all tracks;
            2. If at least one PersistReco (PR) line has fired, run the full
            HLT2 reconstruction (to make sure everything exists) and then
            clone all PVs and their associated tracks; and
            3. Copy the outputs declared by all Turbo lines that fired.

        For step 3, the locations declared by each Turbo line contain:

            1. The candidates themselves and all associated decay vertices and
               particle-to-PV relations tables;
            2. The outputs of any extra algorithms;
            3. The full HLT2 reconstruction (if the PersistReco flag is True).

        The cloning of all of these locations is handled by the
        CopyLinePersistenceLocations,
        CopyParticle2PVRelationsFromLinePersistenceLocations, and
        CopyParticle2RelatedInfoFromLinePersistenceLocations algorithms, with
        the exception of PR PVs which are copied by a separate cloner which
        ensures all associated VELO tracks are cloned.
        """
        outputPrefix = self.getProp("PersistedReconstructionPrefix")
        fullPrefix = "/Event/{0}".format(outputPrefix)

        lineLocations = self._hlt2LineOutputs(outputPrefix=fullPrefix)
        lineOutputs = {line: [loc for (loc, _) in lineLocations[line]]
                       for line in lineLocations}
        lineClonedOutputs = {line: [loc for (_, loc) in lineLocations[line]]
                             for line in lineLocations}

        # Sequence to run the full HLT2 reconstruction if there is a positive
        # decision from any line with full PersistReco enabled, and then copy
        # the PVs with their associated tracks
        # The rest of the reconstruction will be copied by the Turbo line
        # output cloners
        copyFullRecoSeq = Sequence(
            "HltFullPersistRecoSequence",
            Members=[
                self._fullPersistRecoLineFilter(),
                Sequence("HltFullPersistRecoRunSequence", Members=[
                    self._hlt2FullReconstructionSeq()
                ], IgnoreFilterPassed=True)
            ]
        )

        # Configure the packers to pack the reconstruction that will be cloned
        # under outputPrefix
        recoLocations = self._hlt2FullReconstructionLocations()
        for loc in recoLocations:
            # We shouldn't know about prefixed locations at this point
            assert fullPrefix not in recoLocations[loc]
            recoLocations[loc] = recoLocations[loc].replace('/Event',
                                                            fullPrefix)
        HltPersistRecoConf().PackingInputs = recoLocations

        # Configure the ANNSvc so that it is aware of all locations that are
        # referenced by the objects to be serialised
        HltPersistRecoConf().ClonedLineLocations = lineClonedOutputs

        # Configure the persistence service with the list of PersistReco lines,
        # which will be saved in the TCK to be used by Tesla whilst streaming
        HltLinePersistenceSvc().TurboPPLines = [i.decision() for i in self._persistRecoLines()]
        ApplicationMgr().ExtSvc.append(HltLinePersistenceSvc())

        # PersistRecoConf will add its own algorithms into this sequence
        persistenceSeq = Sequence("HltPersistReco")
        HltPersistRecoConf().Sequence = persistenceSeq

        return Sequence(
            "HltPersistRecoSequence",
            Members=[
                self._turboLineFilter(),
                self._trackStateCutterSeq(),
                self._decodeFullClustersSeq(),
                Sequence("HltPersistRecoClonerSequence", Members=[
                    copyFullRecoSeq,
                    self._copyTurboOutputsSeq(lineOutputs, outputPrefix),
                ], IgnoreFilterPassed=True),
                persistenceSeq
            ]
        )

    def _copyTurboOutputsSeq(self, lineOutputs, outputPrefix):
        """Return sequence that clones all outputs defined by Turbo lines."""
        log.info('Will copy these Turbo line outputs: %s',
                 pformat(lineOutputs))

        # Use the persistence svc as a way to pass the list of locations to be
        # cloned to the cloners
        persistence_svc = HltLinePersistenceSvc(
            'HltLinePersistenceSvcForCloners',
            Locations=lineOutputs,
            TurboPPLines=[i.decision() for i in self._persistRecoLines()],
        )
        ApplicationMgr().ExtSvc.append(persistence_svc)

        container_cloner = CopyLinePersistenceLocations(
            OutputPrefix=outputPrefix,
            LinesToCopy=lineOutputs.keys(),
            ILinePersistenceSvc=persistence_svc.getFullName()
        )

        # Clone clusters associated to tracks
        container_cloner.addTool(ProtoParticleCloner, name="ProtoParticleCloner")
        container_cloner.ProtoParticleCloner.ICloneTrack = "TrackClonerWithClusters"
        container_cloner.addTool(TrackClonerWithClusters, name="TrackClonerWithClusters")
        container_cloner.TrackClonerWithClusters.CloneAncestors = False

        # We do two things here:
        #   1. Configure the CaloCluster and CaloHypo cloners to have
        #   consistent behaviour;
        #   2. Force digits to always be cloned when running over simulated
        #   data.
        simulation = self.getProp("Simulation")
        ccc = container_cloner.addTool(CaloClusterCloner, name="CaloClusterCloner")
        ccc.CloneEntriesNeuP = True
        ccc.CloneEntriesChP = False
        ccc.CloneEntriesAlways = simulation
        chc = container_cloner.addTool(CaloHypoCloner, name="CaloHypoCloner")
        chc.CloneClustersNeuP = True
        chc.CloneClustersChP = False
        chc.CloneDigitsNeuP = True
        chc.CloneDigitsChP = False
        chc.CloneClustersAlways = simulation
        chc.CloneDigitsAlways = simulation

        # We *always* want associated digits to be cloned in PersistReco
        ccc_pp = ccc.clone("CaloClusterClonerForTurboPP")
        ccc_pp.CloneEntriesAlways = True
        container_cloner.TurboPPICloneCaloCluster = container_cloner.addTool(ccc_pp).getFullName()
        chc_pp = chc.clone("CaloHypoClonerForTurboPP")
        chc_pp.CloneClustersAlways = True
        chc_pp.CloneDigitsAlways = True
        container_cloner.TurboPPICloneCaloHypo = container_cloner.addTool(chc_pp).getFullName()

        p2pv_cloner = CopyParticle2PVRelationsFromLinePersistenceLocations(
            # Clone PVs, but not the tracks they reference
            ClonerType='VertexBaseFromRecVertexClonerNoTracks',
            OutputPrefix=outputPrefix,
            LinesToCopy=lineOutputs.keys(),
            ILinePersistenceSvc=persistence_svc.getFullName()
        )
        p2relinfo_cloner = CopyParticle2RelatedInfoFromLinePersistenceLocations(
            # Search for all LHCb::RelatedInfoMap objects at the same TES level
            # as the container of LHCb::Particle objects using the CLID, rather
            # than a name
            RelationsBaseName="",
            UseRelationsCLID=True,
            OutputPrefix=outputPrefix,
            LinesToCopy=lineOutputs.keys(),
            ILinePersistenceSvc=persistence_svc.getFullName()
        )
        return Sequence("HltCopyTurboOutputsSequence", Members=[
            container_cloner,
            p2pv_cloner,
            p2relinfo_cloner
        ])

###################################################################################
#
# Main configuration
#
    def __apply_configuration__(self):
        """
        HLT Afterburner configuration
        """
        Afterburner = self.getProp("Sequence") if self.isPropertySet("Sequence") else None
        if not Afterburner:
            return
        AfterburnerFilterSeq = Sequence("HltAfterburnerFilterSequence" )
        Afterburner.Members += [AfterburnerFilterSeq]
        if self.getProp("Hlt2Filter"):
            decoder = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"]
            hlt2Filter = HltFilter('HltAfterburnerHlt2Filter', Code = self.getProp("Hlt2Filter"),
                                   Location = decoder.listOutputs()[0])
            AfterburnerFilterSeq.Members += [hlt2Filter]
        AfterburnerSeq = Sequence("HltAfterburnerSequence", IgnoreFilterPassed = True)
        AfterburnerFilterSeq.Members += [ AfterburnerSeq ]
        if self.getProp("EnableHltRecSummary"):
            seq = Sequence("RecSummarySequence")

            tracksLong = Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks()
            tracksDown = Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks()
            muonID = Hlt2BiKalmanFittedForwardTracking().hlt2MuonID()

            def bm_output(bm, name=None):
                """Get the output location from a DecoderXXX bindMembers."""
                if name:
                    return (bm, bm.decoder.Outputs[name])
                outputs = bm.decoder.listOutputs()
                assert len(outputs) == 1
                return (bm, outputs[0])

            from HltLine.HltDecodeRaw import DecodeVELO, DecodeIT, DecodeTT, DecodeSPD, DecodeMUON
            decoders = {"Velo"   : bm_output(DecodeVELO),
                        "TT"     : bm_output(DecodeTT),
                        "IT"     : bm_output(DecodeIT),
                        "SPD"    : bm_output(DecodeSPD, 'DigitsContainer'),
                        'Muon'   : bm_output(DecodeMUON),
                        'MuonTr' : (muonID, muonID.members()[-1].getProp("MuonTrackLocation")),
                        }

            PVs = PV3D("Hlt2")
            recSeq = Sequence("RecSummaryRecoSequence", IgnoreFilterPassed = True)
            recSeq.Members = list(uniqueEverseen(chain.from_iterable([dec[0] for dec in decoders.itervalues()]
                                                                     +
                                                                     [tracksLong, tracksDown, muonID, PVs])))
            recSeq.Members += [ RichDecoder( "RichFutureDecode" ) ]
            summary = RecSummaryAlg('Hlt2RecSummary', SummaryLocation = self.getProp("RecSummaryLocation"),
                                    HltSplitTracks = True,
                                    SplitLongTracksLocation = tracksLong.outputSelection(),
                                    SplitDownTracksLocation = tracksDown.outputSelection(),
                                    PVsLocation = PVs.output,
                                    VeloClustersLocation = decoders['Velo'][1],
                                    ITClustersLocation = decoders['IT'][1],
                                    TTClustersLocation = decoders['TT'][1],
                                    SpdDigitsLocation  = decoders['SPD'][1],
                                    MuonCoordsLocation = decoders['Muon'][1],
                                    MuonTracksLocation = decoders['MuonTr'][1])
            seq.Members = [recSeq, summary]
            AfterburnerSeq.Members += [seq]

        if self.getProp("AddAdditionalTrackInfos"):
            trackLocations = [ Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks().outputSelection(),
                               Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks().outputSelection() ]
            infoSeq = Sequence("TrackInfoSequence", IgnoreFilterPassed = True)
            # I don't want to pull in reconstruction if not run before, then there should be also no candidates needing this information
            # This is anyhow done by the RecSummary above
            members = [Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks().members() + Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks().members()]
            infoSeq.Members += list(uniqueEverseen(chain.from_iterable(members)))
            prefix = "Hlt2"
            trackClones = Sequence(prefix + "TrackClonesSeq")
            #checkTracks =  Filter(prefix+"CheckTrackLoc",Code = "EXISTS('%(trackLocLong)s') & EXISTS('%(trackLocDown)s')" % {"trackLocLong" : trackLocations[0], "trackLocDown" : trackLocations[1]})
            #trackClones.Members += [checkTracks]
            cloneTable = TrackBuildCloneTable(prefix + "FindTrackClones")
            cloneTable.maxDz   = 500*mm
            cloneTable.zStates = [ 0*mm, 990*mm, 9450*mm ]
            cloneTable.klCut   = 5e3
            cloneTable.inputLocations = trackLocations
            cloneTable.outputLocation = trackLocations[0]+"Downstream" + "Clones"
            cloneCleaner = TrackCloneCleaner(prefix + "FlagTrackClones")
            cloneCleaner.CloneCut = 5e3
            cloneCleaner.inputLocations = trackLocations
            cloneCleaner.linkerLocation = cloneTable.outputLocation
            trackClones.Members += [ cloneTable, cloneCleaner ]

            infoSeq.Members += [trackClones]

            AfterburnerSeq.Members += [infoSeq]

            # Add VeloCharge to protoparticles for dedx
            veloChargeSeq = Sequence("VeloChargeSequence")
            protoLocation = Hlt2BiKalmanFittedForwardTracking().hlt2ChargedAllPIDsProtos().outputSelection()
            checkProto =  Filter("CheckProtoParticles",Code = "EXISTS('%(protoLoc)s')"% {"protoLoc" : protoLocation})
            addVeloCharge = ChargedProtoParticleAddVeloInfo("Hlt2AddVeloCharge")
            addVeloCharge.ProtoParticleLocation = protoLocation
            decodeVeloFullClusters = DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].setup()
            veloChargeSeq.Members +=  [checkProto, decodeVeloFullClusters, addVeloCharge]
            AfterburnerSeq.Members += [veloChargeSeq]

        # Configure and add the persist reco
        AfterburnerSeq.Members += [self._persistRecoSeq()]
